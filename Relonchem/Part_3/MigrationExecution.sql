
prompt -----------------------------------;
prompt - Migration Script Execution     --;
prompt -----------------------------------;
prompt
accept target_argus_instance     char prompt 'Enter TNSNAMES entry to connect to Source Argus database 		: '
accept source_argus_instance      char prompt 'Enter TNSNAMES entry to connect to Target Argus database  		: '
accept sys_pwd            char prompt 'Enter SYSTEM user password for Argus database     		: ' hide
accept argus_app_pass     char prompt 'Enter Argus_App password                          		: ' hide
accept source_schema_owner	  char prompt 'Enter Source Schema Owner Name                      		: ' 
accept log_file           char prompt 'Enter the log file name						:'
accept enterprise_abbrv   char prompt 'Enter the enterprise abbreviation				:'
accept migration_user     char prompt 'Enter the migration username					:'
accept migration_site     char prompt 'Enter the migration site description				:'

set serveroutput on;
prompt
prompt Connecting to &target_argus_instance. as Argus_App
prompt 
connect Argus_App/&argus_app_pass.@&target_argus_instance.

prompt Press Enter if connected successfully...;
pause  Else Close Window and Re-Execute with appropriate Username/Password....
prompt 


prompt 
prompt ################################################################################;
prompt # Validating the input parameters 
prompt ################################################################################;

set verify off;
Declare cnt NUMBER;
enterprise_id NUMBER;
BEGIN
pkg_rls.set_context (
         'admin',
         0,
         'ARGUS_SAFETY',
         ''
      );
	  
      SELECT count(*)
        INTO cnt
        FROM cfg_enterprise
       WHERE TRIM (UPPER (enterprise_abbrv)) =
                                            TRIM (UPPER ('&enterprise_abbrv'));	
	 
       IF(cnt =0)
	then
		dbms_output.put_line('Enterprise Abbreviation : INVALID');
	
else
		dbms_output.put_line('Enterprise Abbreviation : VALID');
		select enterprise_id into enterprise_id
		from cfg_enterprise where TRIM (UPPER (enterprise_abbrv)) =
                                            TRIM (UPPER ('&enterprise_abbrv'));

		pkg_rls.set_context (
         'admin',
         enterprise_id,
         'ARGUS_SAFETY',
         ''
      );

	SELECT count(*)
        INTO cnt
	FROM   argus_app.cfg_users
       WHERE TRIM (UPPER (user_fullname)) = TRIM (UPPER ('&migration_user'));
	IF(cnt =0)
	then
		dbms_output.put_line('Migration User : INVALID');
	else
		dbms_output.put_line('Migration User : VALID');
	end if;	

	SELECT count(*)
        INTO cnt
	FROM   argus_app.lm_sites
       WHERE TRIM (UPPER (site_desc)) = TRIM (UPPER ('&migration_site'));

	IF(cnt =0)
	then
		dbms_output.put_line('Migration Site : INVALID');
	else
		dbms_output.put_line('Migration Site : VALID');
	end if;


end if;	

end;
/


prompt Press Enter if all the parameters are valid;
pause  If any of the parameters is invalid, close the Window and Re-Execute with correct parameters....

prompt
prompt Connecting to &target_argus_instance. as system
prompt 
connect system/&sys_pwd.@&target_argus_instance.
prompt Press Enter if connected successfully...;
pause  Else Close Window and Re-Execute with appropriate Username/Password.... 
SPOOL .\&log_file..LOG


prompt
prompt Connecting to &target_argus_instance. as S_ARGUS_APP
prompt 
connect S_ARGUS_APP/manager#123@&target_argus_instance.
prompt Press Enter if connected successfully...;
pause  Else Close Window and Re-Execute with appropriate Username/Password....
prompt


prompt ################################################################################;
prompt # Creating Migration Packages
prompt ################################################################################;
@.\01_cre_pkg_Logger.sql;
@.\02_pkg_utl_spec.sql;
@.\03_pkg_main_spec.sql;
@.\04_pkg_utl_body.sql;
@.\05_pkg_main_body.sql;

ALTER PACKAGE S_ARGUS_APP.logger COMPILE;
ALTER PACKAGE S_ARGUS_APP.logger COMPILE BODY;

ALTER PACKAGE S_ARGUS_APP.pkg_util COMPILE;
ALTER PACKAGE S_ARGUS_APP.pkg_util COMPILE BODY;

ALTER PACKAGE S_ARGUS_APP.pkg_main COMPILE;
ALTER PACKAGE S_ARGUS_APP.pkg_main COMPILE BODY;

/*
prompt ################################################################################;
prompt # Calling main procedure for migration
prompt ################################################################################;

BEGIN pkg_main.p_main_migrate ('&enterprise_abbrv', 'NULL', '&migration_user', '&migration_site', 'NULL'); END;
/

prompt ################################################################################;
prompt # Disabling Case master Trigger
prompt ################################################################################;
ALTER TRIGGER ARGUS_APP.TRG_CM_PDP_POP_CASE_DATA DISABLE;
ALTER TRIGGER ARGUS_APP.TRG_CASE_REG_REP_AFT_INS DISABLE;
ALTER TRIGGER ARGUS_APP.TRG_CSRR_DSHBRD_ROW_AFT_UPD DISABLE;
ALTER TRIGGER ARGUS_APP.TRG_CSRR_DSHBRD_TBL_AFT_IUD DISABLE;

prompt ################################################################################;
prompt # Pushing data into Argus
prompt ################################################################################;

BEGIN pkg_main.p_push_argus_app; END;



/


prompt ################################################################################;
prompt # Enabling Case master Trigger
prompt ################################################################################;
ALTER TRIGGER ARGUS_APP.TRG_CM_PDP_POP_CASE_DATA ENABLE;
ALTER TRIGGER ARGUS_APP.TRG_CASE_REG_REP_AFT_INS ENABLE;
ALTER TRIGGER ARGUS_APP.TRG_CSRR_DSHBRD_ROW_AFT_UPD ENABLE;
ALTER TRIGGER ARGUS_APP.TRG_CSRR_DSHBRD_TBL_AFT_IUD ENABLE;
*/

SPOOL OFF

prompt ################################################################################;
prompt # Verify .\&log_file..LOG For errors, if any....;
prompt ################################################################################;

Pause  Press Enter to continue......

EXIT;
