CREATE OR REPLACE PACKAGE pkg_util
AS
-- GLOBAL VARIABLES
--------------------------------CONSTANTS---------------------------------------------
   g_migration_user            NUMBER;
   g_migration_user_fullname   VARCHAR2 (100);
   g_migration_date            DATE;
   g_migration_site_id         NUMBER;
   g_argus_meddra_dict_id      NUMBER;
   g_argus_who_dict_id         NUMBER;     
   g_enterprise_id             NUMBER;
   g_enterprise_abbrev         VARCHAR2 (20);
   g_prod_cd                   VARCHAR2 (500);



  PROCEDURE p_initialize (
      pi_enterprise_abbrv   VARCHAR2,
      pi_prod_cd            VARCHAR2,
      pi_migration_user     VARCHAR2,
      pi_migration_site     VARCHAR2
   );
   
    FUNCTION f_fetch_sequence (
      pi_table_name      VARCHAR2,
      pi_display_order   NUMBER := NULL
	)RETURN NUMBER;
	

   PROCEDURE p_insert_dm_mapping;

   PROCEDURE p_update_dm_mapping;


   FUNCTION f_get_argus_code (
      pi_target_table_name   IN   VARCHAR2,
      pi_source_lm_code     IN   VARCHAR2
   ) RETURN NUMBER;

   FUNCTION f_get_argus_code_value (
      pi_target_table_name   IN   VARCHAR2,
      pi_source_lm_code_value     IN   VARCHAR2
   ) RETURN VARCHAR2;
   
END pkg_util;
/
