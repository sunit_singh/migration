CREATE OR REPLACE PACKAGE BODY pkg_main
AS

   PROCEDURE p_migrate_case_master
   
/***********************************************************************
 Name:        p_migrate_case_master
 Description: This procedure migrates data to CASE_MASTER table.
 ***********************************************************************/
 
    AS

	l_sid                   VARCHAR2 (200);


   BEGIN

      SELECT dbname
        INTO l_sid
        FROM control_table
       WHERE ROWNUM <= 1;

      INSERT INTO scase_master
                  (case_id, case_num, rev, workflow_seq_num, last_workflow_seq_num, create_time, init_rept_date, user_id, 
				   last_update_time, last_update_user_id, requires_followup, followup_date, owner_id, state_id, country_id, lang_id, priority,
                   site_id,	seriousness, rpt_type_id, last_state_id, assessment_needed, priority_override, sid, safety_date, normal_time, max_time,
					report_scheduling, priority_assessment, close_user_id, close_date, close_notes, date_locked, deleted, due_soon, global_num, 
					priority_date_assessed, lam_assess_done, e2b_ww_number, worklist_owner_id, susar, last_update_event, initial_justification, force_soon,
					lock_status_id, medically_confirm, enterprise_id, src_case_id)
         (SELECT s_case_maste_case_id.NEXTVAL case_id,
                 SUBSTR (pcm.case_num, 1, 20) case_num,
				 pcm.rev rev,
				 100007 workflow_seq_num,
				 null last_workflow_seq_num,
				 pcm.create_time,
				 pcm.init_rept_date,
				 pkg_util.g_migration_user,
				 pcm.last_update_time,
				 pkg_util.g_migration_user,
				 pcm.requires_followup,
				 pcm.followup_date,
				 pkg_util.g_migration_user,
				 decode(pcm.state_id, 1, 1, 100005),
				 pkg_util.f_get_argus_code('LM_COUNTRIES', pcm.country_id),
				 pcm.lang_id,
				 pcm.priority,
				 100000,
				 pcm.seriousness,
				 pkg_util.f_get_argus_code('LM_REPORT_TYPE',pcm.rpt_type_id),
				 100000 last_state_id,
				 pcm.assessment_needed,
				 pcm.priority_override,
				 l_sid ,
				 pcm.safety_date,
				 pcm.normal_time,
				 pcm.max_time,
				 pcm.report_scheduling,
				 pcm.priority_assessment,
				 decode(pcm.close_user_id, null, null, pkg_util.g_migration_user),
				 pkg_util.g_migration_date, 
				 pcm.close_notes, 
				 pkg_util.g_migration_date, 
				 pcm.deleted, 
				 pcm.due_soon, 
				 pcm.global_num, 
				 pcm.priority_date_assessed, 
				 pcm.lam_assess_done, 
				 pcm.e2b_ww_number,
				 pkg_util.g_migration_user,
				 pcm.susar, 
				 pcm.last_update_event, 
				 pcm.initial_justification, 
				 pcm.force_soon,
				 3, 
				 pcm.medically_confirm, 
				 pkg_util.g_enterprise_id,
				 pcm.case_id
				 FROM pnc_data_extract.pc_case_master pcm where pcm.case_num not in ('Req for LAM Dont del','2016RLC001895','2016RLC001900','2016RLC002210'));
				 

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_MASTER'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;

	  
	  
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_master',
                         'PKG_MAIN',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE MASTER INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_master , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_master;
   
   
      PROCEDURE p_migrate_case_event
   /***********************************************************************
    Name:        p_migrate_case_event
    Description: This procedure migrates data to CASE_EVENT table.
    ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_event
                  (case_id, seq_num, desc_reptd, desc_coded,diagnosis, onset, onset_res, onset_delay,
				  stop_date, stop_date_res, duration, onset_latency, evt_intensity_id, evt_freq_id,
				  treated, study_related, rechall_related, prod_seq_num, evt_outcome_id, receipt_date,
				  rpt_serious, sc_death, sc_hosp, sc_cong_anom, sc_threat, sc_disable, sc_int_req, sc_other,
				  sc_other_text, art_code, pref_term, inc_term, body_sys, med_serious, past_hist,
				  dict_key, code_status, seriousness, sort_id, onset_date_partial, stop_date_partial,
				  efficacy, disease, withdrawal, hlgt, hlt, hlt_code, hlgt_code, soc_code, study_dropped, inc_code, onset_minutes,
				  delay_minutes, details, syn_code, deleted, study_related_reptd, duration_unit_e2b, onset_latency_unit_e2b,
				  onset_delay_unit_e2b, duration_seconds, onset_latency_seconds, onset_delay_seconds, emergency_room_visit, 
				  physician_office_visit, country_occured_id, medical_confirm, ud_number_11, enterprise_id)
		(Select cm.case_id, s_case_event.NEXTVAL AS seq_num, pce.desc_reptd, pce.desc_coded, pce.diagnosis, pce.onset, pce.onset_res,
				pce.onset_delay, pce.stop_date, pce.stop_date_res, pce.duration, pce.onset_latency,
				pkg_util.f_get_argus_code('LM_EVT_INTENSITY',pce.evt_intensity_id), pce.evt_freq_id, pce.treated, pce.study_related,
				pce.rechall_related, null,pkg_util.f_get_argus_code('LM_EVT_OUTCOME',pce.evt_outcome_id), pce.receipt_date, pce.rpt_serious,
				pce.sc_death, pce.sc_hosp, pce.sc_cong_anom, pce.sc_threat, pce.sc_disable, pce.sc_int_req, pce.sc_other, pce.sc_other_text,
				pce.art_code, pce.pref_term, pce.inc_term, pce.body_sys,
				pce.med_serious, pce.past_hist, pce.dict_key, pce.code_status, pce.seriousness, pce.sort_id,
				pce.onset_date_partial, pce.stop_date_partial, pce.efficacy, pce.disease, pce.withdrawal, pce.hlgt,
                pce.hlt, pce.hlt_code, pce.hlgt_code, pce.soc_code, pce.study_dropped, pce.inc_code, pce.onset_minutes, pce.delay_minutes, pce.details,
				pce.syn_code, pce.deleted, pce.study_related_reptd, pce.duration_unit_e2b, pce.onset_latency_unit_e2b,
				pce.onset_delay_unit_e2b, pce.duration_seconds, pce.onset_latency_seconds, pce.onset_delay_seconds, pce.emergency_room_visit, 
				pce.physician_office_visit, pce.country_occured_id, pce.medical_confirm, pce.seq_num, pkg_util.g_enterprise_id            
				FROM scase_master cm, pnc_data_extract.pc_case_event pce
                WHERE pce.case_id = cm.src_case_id); 						   
										   
							
      UPDATE scase_event
         SET dict_id = pkg_util.g_argus_meddra_dict_id,
             code_status = 1
       WHERE inc_code IS NOT NULL AND enterprise_id = pkg_util.g_enterprise_id;

	   UPDATE scase_event
         SET dict_id = pkg_util.g_argus_meddra_dict_id,
             code_status = 0
       WHERE inc_code IS NULL AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_event',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE event INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_event , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_event;
   
   
   PROCEDURE p_migrate_case_reference
    /***********************************************************************
     Name:        p_migrate_case_reference
     Description: This procedure migrates case data to CASE_REFERENCE table.
   ***********************************************************************/
   AS
   BEGIN
   
   
	  INSERT INTO scase_reference
				  (case_id, seq_num, ref_no, ref_type_id, notes, ref_case_id, sort_id, deleted, enterprise_id)
	     (SELECT cm.case_id, pkg_util.f_fetch_sequence ('CASE_REFERENCE'), pcr.ref_no,
						pkg_util.f_get_argus_code ('LM_REF_TYPES',
                                                    pcr.ref_type_id
                                                    ),
					pcr.notes,(SELECT scm1.case_id from scase_master scm1 where src_case_id=pcr.ref_case_id),
					pcr.sort_id, pcr.deleted, pkg_util.g_enterprise_id from pnc_data_extract.pc_case_reference pcr, scase_master cm
					where cm.src_case_id=pcr.case_id);
					
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_REFERENCE'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_reference',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_REFERENCE INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_reference , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_reference;
   

    PROCEDURE p_migrate_case_lab_data
	/***********************************************************************
	 Name:        p_migrate_case_lab_data
	 Description: This procedure migrates data to case_lab_data table.
	 ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_lab_data
				  (case_id, seq_num, lab_test_id, test_date, sort_id, assessment, test_date_res, results,
				   norm_high, norm_low, test_date_partial, result, unit, lab_test_name, deleted, notes, pt_code,
				   llt_code, hlt_code, hlgt_code, soc_code, syn_code, llt, hlt, hlgt, soc, code_status,
				   test_reptd, unit_id, comments, info_available, enterprise_id)
	    (SELECT cm.case_id, s_case_lab_data.NEXTVAL, 
                            pkg_util.f_get_argus_code ('LM_LAB_TEST_TYPES',
                                                        pcld.lab_test_id
                                                      ),
							pcld.test_date, pcld.sort_id, 
                            pkg_util.f_get_argus_code ('LM_LAB_ASSESSMENT',
                                                       pcld.assessment
                                                      ),
							pcld.test_date_res, pcld.results, pcld.norm_high,
							pcld.norm_low, pcld.test_date_partial, pcld.result,
							pkg_util.f_get_argus_code_value('LM_DOSE_UNITS',
															pcld.unit
															),
							pcld.lab_test_name, pcld.deleted, pcld.notes, pcld.pt_code,
							pcld.llt_code, pcld.hlt_code, pcld.hlgt_code, pcld.soc_code, pcld.syn_code,
							pcld.llt, pcld.hlt, pcld.hlgt, pcld.soc, pcld.code_status, pcld.test_reptd,
							pkg_util.f_get_argus_code ('LM_DOSE_UNITS',
                                                       pcld.unit_id
                                                      ),
							pcld.comments, pcld.info_available, pkg_util.g_enterprise_id 
							FROM scase_master cm, pnc_data_extract.pc_case_lab_data pcld
							WHERE cm.src_case_id=pcld.case_id);
                           
								
							   
		UPDATE scase_lab_data
         SET dict_id = pkg_util.g_argus_meddra_dict_id,
             code_status = 1
		WHERE llt_code IS NOT NULL AND enterprise_id = pkg_util.g_enterprise_id;

		UPDATE scase_lab_data
         SET dict_id = pkg_util.g_argus_meddra_dict_id,
             code_status = 1
		WHERE llt_code IS NULL AND enterprise_id = pkg_util.g_enterprise_id;		
	  
	  
                  
		UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
		WHERE table_name = 'CASE_LAB_DATA'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_lab_data',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_lab_data INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_lab_data , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_lab_data;
   
   
   
   PROCEDURE p_migrate_case_death
/***********************************************************************
 Name:        p_migrate_case_death
 Description: This procedure migrates data to CASE_DEATH table.
 ***********************************************************************/
   AS
    BEGIN
         INSERT INTO scase_death
					(case_id, death_date, autopsy, death_date_res, death_date_partial, determine_autopsy,
					 deleted, enterprise_id)
		 (SELECT cm.case_id, pcd.death_date, pcd.autopsy, pcd.death_date_res, pcd.death_date_partial, pcd.determine_autopsy,
					 pcd.deleted, pkg_util.g_enterprise_id 
		  FROM scase_master cm, pnc_data_extract.pc_case_death pcd where cm.src_case_id=pcd.case_id);	 

		  UPDATE dm_status
			 SET status = 1,
				 conv_end_time = SYSDATE
		   WHERE table_name = 'CASE_DEATH'
			 AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
	  
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_death',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE Death INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_death , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_death;
   
PROCEDURE p_migrate_case_death_details
/***********************************************************************
 Name:        p_migrate_case_death_details
 Description: This procedure migrates data to CASE_DEATH_DETAILS table.
 ***********************************************************************/
   AS
	BEGIN
	INSERT INTO scase_death_details (case_id, seq_num, sort_id, term_type, cause_reptd, cause_coded,
									 cause, cause_code, cause_llt, cause_llt_code, cause_hlt, cause_hlt_code,
									 cause_hlgt, cause_hlgt_code, cause_soc, cause_soc_code, cause_syn_code,
									 deleted, enterprise_id)
	(SELECT cm.case_id, S_CASE_DEATH_DETAILS.nextval, pcdd.sort_id, pcdd.term_type, pcdd.cause_reptd,
			pcdd.cause_coded, pcdd.cause, pcdd.cause_code, pcdd.cause_llt, pcdd.cause_llt_code, pcdd.cause_hlt, pcdd.cause_hlt_code,
			pcdd.cause_hlgt, pcdd.cause_hlgt_code, pcdd.cause_soc, pcdd.cause_soc_code, pcdd.cause_syn_code, pcdd.deleted, 
			pkg_util.g_enterprise_id FROM pnc_data_extract.pc_case_death_details pcdd, scase_master cm WHERE cm.src_case_id = pcdd.case_id);
	
	
	
      UPDATE scase_death_details
         SET cause_dict = pkg_util.g_argus_meddra_dict_id,
             cause_code_status = 1
       WHERE cause_llt_code IS NOT NULL
         AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE scase_death_details
         SET cause_code_status = 0
       WHERE cause_code_status IS NULL
         AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_DEATH_DETAILS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_death_details',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE Death details INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_death_details , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_death_details;
   
   PROCEDURE p_migrate_case_pat_hist
   /***********************************************************************
    Name:        p_migrate_case_pat_hist
    Description: This procedure migrates data to case_pat_hist table.
    ***********************************************************************/
   AS
	BEGIN
	INSERT INTO scase_pat_hist(case_id, seq_num, start_date, start_date_res, stop_date, stop_date_res, condition_type_id, sort_id,
								continue, parent, item_code, start_date_partial, stop_date_partial, condition, note, item_llt,
								item_hlt, item_hlgt, item_soc, item_llt_code, item_hlt_code, item_hlgt_code, item_soc_code, item_reptd,
								item_coded, item_syn_code, deleted, indication, ind_reptd, ind_coded, ind_pt_code,
								ind_llt_code, ind_llt, ind_hlt_code, ind_hlt, ind_hlgt_code, ind_hlgt, ind_soc_code, ind_soc, ind_syn_code,
								reaction, react_reptd, react_coded, react_pt_code, react_llt_code, react_llt,
								react_hlt_code, react_hlt, react_hlgt_code, react_hlgt, react_soc_code, react_soc, react_syn_code,
								family_history, enterprise_id)
					(SELECT cm.case_id, s_case_pat_hist.nextval, pcph.start_date, pcph.start_date_res, pcph.stop_date, pcph.stop_date_res,
                                                        pcph.condition_type_id,
							pcph.sort_id, pcph.continue, pcph.parent, pcph.item_code, pcph.start_date_partial, pcph.stop_date_partial, pcph.condition,
							pcph.note, pcph.item_llt, pcph.item_hlt, pcph.item_hlgt, pcph.item_soc, pcph.item_llt_code, pcph.item_hlt_code, 
							pcph.item_hlgt_code, pcph.item_soc_code, pcph.item_reptd, pcph.item_coded, pcph.item_syn_code, pcph.deleted,
							pcph.indication, pcph.ind_reptd, pcph.ind_coded, pcph.ind_pt_code, pcph.ind_llt_code, pcph.ind_llt, pcph.ind_hlt_code, 
							pcph.ind_hlt, pcph.ind_hlgt_code, pcph.ind_hlgt, pcph.ind_soc_code, pcph.ind_soc, pcph.ind_syn_code, 
							pcph.reaction, pcph.react_reptd, pcph.react_coded, pcph.react_pt_code, pcph.react_llt_code, 
							pcph.react_llt, pcph.react_hlt_code, pcph.react_hlt, pcph.react_hlgt_code, pcph.react_hlgt, pcph.react_soc_code, 
							pcph.react_soc, pcph.react_syn_code, pcph.family_history, pkg_util.g_enterprise_id
							FROM scase_master cm, pnc_data_extract.pc_case_pat_hist pcph WHERE cm.src_case_id = pcph.case_id);

      UPDATE scase_pat_hist
         SET item_dict = pkg_util.g_argus_meddra_dict_id,
             item_code_status = 1
       WHERE item_llt_code IS NOT NULL
         AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE scase_pat_hist
         SET item_code_status = 0
       WHERE item_code_status IS NULL
         AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE scase_pat_hist
         SET ind_dict_id = pkg_util.g_argus_meddra_dict_id,
             ind_code_status = 1
       WHERE ind_llt_code IS NOT NULL
         AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE scase_pat_hist
         SET ind_dict_id = 0
       WHERE ind_code_status IS NULL
         AND enterprise_id = pkg_util.g_enterprise_id;		 
		 
		 
      UPDATE scase_pat_hist
         SET react_dict_id = pkg_util.g_argus_meddra_dict_id,
             react_code_status = 1
       WHERE react_llt_code IS NOT NULL
         AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE scase_pat_hist
         SET react_dict_id = 0
       WHERE react_code_status IS NULL
         AND enterprise_id = pkg_util.g_enterprise_id;			 
		 
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PAT_HIST'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_pat_hist',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_pat_hist INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_pat_hist , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_pat_hist;  

   PROCEDURE p_migrate_case_reporters
   /***********************************************************************
    Name:        p_migrate_case_reporters
    Description: This procedure migrates data to CASE_REPORTERS table.
    ***********************************************************************/
	AS
	BEGIN
	
	  INSERT INTO scase_reporters(case_id, seq_num, media_id, occupation_id, hcp_flag, primary_contact, corr_contact, reporter_type, intermediary_id,
								  rpt_sent, sort_id, confidential, country_id, suffix, middle_name, prefix, first_name, last_name, reporter_id, institution,
								  department, city, state, postcode, country, phone, altphone, fax, reporter_ref, email, address, notes, institution_id, enterprise_id)
				  ( SELECT 	cm.case_id, s_case_reporters.nextval,
                            pkg_util.f_get_argus_code ('LM_REPORT_MEDIA',
                                                        pcr.media_id
													   ),
                            pkg_util.f_get_argus_code ('LM_OCCUPATIONS',
                                                        pcr.occupation_id
													   ),
							pcr.hcp_flag, pcr.primary_contact, pcr.corr_contact,
                            pkg_util.f_get_argus_code ('LM_REPORTER_TYPE',
                                                        pcr.reporter_type
													   ),
                            pkg_util.f_get_argus_code ('LM_INTERMEDIARY',
                                                        pcr.intermediary_id
													   ),
							pcr.rpt_sent, pcr.sort_id, pcr.confidential,
                            pkg_util.f_get_argus_code ('LM_COUNTRIES',
                                                        pcr.country_id
													   ),
							pcr.suffix, pcr.middle_name, pcr.prefix, pcr.first_name, pcr.last_name, pcr.reporter_id, pcr.institution, pcr.department,
							pcr.city, pcr.state, pcr.postcode, pcr.country, pcr.phone, pcr.altphone, pcr.fax, pcr.reporter_ref, pcr.email, pcr.address,
							pcr.notes, pcr.institution_id, pkg_util.g_enterprise_id 
							FROM scase_master cm, pnc_data_extract.pc_case_reporters pcr WHERE cm.src_case_id = pcr.case_id);

													   
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_REPORTERS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_reporters',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_REPORTERS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_reporters , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_reporters;
   
   
   PROCEDURE p_migrate_case_routing
   AS
   /***********************************************************************
     Name:        p_migrate_case_routing
     Description: This procedure migrates case data to CASE_ROUTING table.
     ***********************************************************************/
   BEGIN
   
      INSERT INTO scase_routing
                  (case_id, seq_num, route_date, user_id, sort_id,
                   comment_txt, to_state_id, fr_state_id, last_update_time,
                   report_scheduling, case_status, deleted, enterprise_id)
         (SELECT cm.case_id, pkg_util.f_fetch_sequence ('CASE_ROUTING'),
                gss_util.to_gmt (pkg_util.g_migration_date),
                pkg_util.g_migration_user, 1,
                   'Case Migrated from Source ARGUS Multitenant by '
                || pkg_util.g_migration_user,
                100005, NULL, pkg_util.g_migration_date, 0, 3, cm.deleted,
                pkg_util.g_enterprise_id
           FROM scase_master cm);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_ROUTING'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_routing',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE Routing INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_routing , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_routing;


   PROCEDURE p_migrate_case_study
   /***********************************************************************
   Name:        p_migrate_case_study
   Description: This procedure migrates case data to CASE_STUDY table.
   ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_study
				  (case_id, study_num, study_type, code_broken, product_count, blind_name, study_desc, enterprise_id)
	  (SELECT cm.case_id, pcs.study_num, pcs.study_type, pcs.code_broken, pcs.product_count, pcs.blind_name, pcs.study_desc, pkg_util.g_enterprise_id
	          FROM scase_master cm, pnc_data_extract.pc_case_study pcs WHERE pcs.case_id=cm.src_case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_STUDY'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_study',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_STUDY INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_study , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_study;
   

   PROCEDURE p_migrate_case_assess
   /***********************************************************************
    Name:        p_migrate_case_assess
    Description: This procedure migrates data to case_assess table.
    ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_assess(case_id, listedness, outcome, seriousness, serious_notes, agent_suspect,
								agent_notes, listedness_notes, bfarm_causality, bfarm_manual_text, company_diagnosis,
								company_diagnosis_notes, diagnosis_pref_code, diagnosis_inc_code, diagnosis_inc_term,
								diagnosis_hlgt_code, diagnosis_hlgt, diagnosis_hlt_code, diagnosis_hlt, diagnosis_soc_code,
								diagnosis_body_sys, evaluation, co_suspect_count, event_synopsis, event_primary, diagnosis_reptd,
								diagnosis_coded, diagnosis_syn_code, diagnosis_code_status, deleted, enterprise_id)
					(SELECT cm.case_id, pca.listedness,
                            pkg_util.f_get_argus_code ('LM_EVT_OUTCOME',
                                                        pca.outcome
													   ),													   
							pca.seriousness, pca.serious_notes, pca.agent_suspect, pca.agent_notes, pca.listedness_notes,
							pca.bfarm_causality, pca.bfarm_manual_text, pca.company_diagnosis, pca.company_diagnosis_notes, 
							pca.diagnosis_pref_code, pca.diagnosis_inc_code, pca.diagnosis_inc_term, pca.diagnosis_hlgt_code, 
							pca.diagnosis_hlgt, pca.diagnosis_hlt_code, pca.diagnosis_hlt, pca.diagnosis_soc_code,
							pca.diagnosis_body_sys, pca.evaluation, pca.co_suspect_count, pca.event_synopsis, pca.event_primary, 
							pca.diagnosis_reptd, pca.diagnosis_coded, pca.diagnosis_syn_code, pca.diagnosis_code_status, pca.deleted,
							pkg_util.g_enterprise_id FROM scase_master cm, pnc_data_extract.pc_case_assess pca WHERE cm.src_case_id = pca.case_id);
	  
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_ASSESS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_assess',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_assess INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_assess , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_assess;
  
   
   
   PROCEDURE p_migrate_case_followup
/***********************************************************************
Name:        p_migrate_case_followup
Description: This procedure migrates case data to CASE_FOLLOWUP table.
***********************************************************************/
   AS
      
   BEGIN
      
	  INSERT INTO scase_followup(case_id, seq_num, receipt_date, safety_date, significant, auditlog_type, time_stamp, significant_device,
								 data_cleanup, justification, justification_id, deleted, amendment, enterprise_id)
				(SELECT cm.case_id, pkg_util.f_fetch_sequence ('CASE_FOLLOWUP'), pcf.receipt_date, pcf.safety_date, pcf.significant, pcf.auditlog_type, pcf.time_stamp,
						pcf.significant_device, pcf.data_cleanup, pcf.justification,
                        pkg_util.f_get_argus_code ('LM_JUSTIFICATIONS',
                                                    pcf.justification_id
													   ),						
						pcf.deleted, pcf.amendment, pkg_util.g_enterprise_id FROM pnc_data_extract.pc_case_followup pcf, scase_master cm
						WHERE cm.src_case_id = pcf.case_id );
	  
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_FOLLOWUP'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_followup',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_FOLLOWUP'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_followup , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_followup;
   
   
   PROCEDURE p_migrate_case_literature
/***********************************************************************
Name:        p_migrate_case_literature
Description: This procedure migrates case data to CASE_LITERATURE table.
***********************************************************************/
   AS
      
   BEGIN
      
	  INSERT INTO scase_literature(case_id, seq_num, sort_id, literature_id, journal, author, title, vol, year, pgs, deleted, enterprise_id)
	              (SELECT cm.case_id, s_case_literature.nextval, pcl.sort_id,
							pkg_util.f_get_argus_code ('LM_LIT_CITATIONS',
														pcl.literature_id),
							pcl.journal, pcl.author, pcl.title, pcl.vol, pcl.year, pcl.pgs, pcl.deleted, pkg_util.g_enterprise_id
							FROM scase_master cm, pnc_data_extract.pc_case_literature pcl WHERE cm.src_case_id = pcl.case_id);
													   				  
	  
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_LITERATURE'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_literature',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_LITERATURE'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_literature , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_literature;   
   
   
   PROCEDURE p_migrate_case_pregnancy
   /***********************************************************************
     Name:        p_migrate_case_pregnancy
     Description: This procedure migrates data to case_pregnancy table.
     ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_pregnancy(case_id, date_of_lmp, gest_period, due_date, exp_trimester, gestation_exp_period, parent, deleted, breastfeeding, 
								   prospective, number_of_fetus, date_of_lmp_res, date_of_lmp_partial, gravida, para, gest_period_unit, enterprise_id)
				(SELECT cm.case_id, pcp.date_of_lmp, pcp.gest_period, pcp.due_date, pcp.exp_trimester, pcp.gestation_exp_period, pcp.parent,
				        pcp.deleted, pcp.breastfeeding, pcp.prospective, pcp.number_of_fetus, pcp.date_of_lmp_res, pcp.date_of_lmp_partial, 
						pcp.gravida, pcp.para, pcp.gest_period_unit, pkg_util.g_enterprise_id FROM scase_master cm, pnc_data_extract.pc_case_pregnancy pcp
                        WHERE cm.src_case_id = pcp.case_id);						

         
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PREGNANCY'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_pregnancy',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_pregnancy INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_pregnancy , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_pregnancy;
   
   
   PROCEDURE p_migrate_case_narrative
   /***********************************************************************
    Name:        p_migrate_case_narrative
    Description: This procedure migrates data to case_narrative table.
    ***********************************************************************/
   AS

   BEGIN
      
	  INSERT INTO scase_narrative
                  (case_id, abbrev_narrative, narrative, deleted, enterprise_id)
			(SELECT cm.case_id, pcn.abbrev_narrative, pcn.narrative, pcn.deleted, pkg_util.g_enterprise_id
			        FROM scase_master cm, pnc_data_extract.pc_case_narrative pcn WHERE cm.src_case_id=pcn.case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_NARRATIVE'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_narrative',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_narrative INSERT '
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_narrative , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_narrative;
   
      PROCEDURE p_migrate_case_pat_info
/***********************************************************************
Name:        p_migrate_case_pat_info
Description: This procedure migrates data to case_pat_info table.
     ***********************************************************************/
   AS
   BEGIN
      
      INSERT INTO scase_pat_info
                   (case_id, pat_id, pat_subj_num, pat_initials, pat_firstname, pat_mi, pat_lastname, pat_address, pat_city,
					pat_state, pat_postal_code, pat_country, pat_phone, pat_dob, pat_dob_res, pat_age, age_unit_id, age_group_id,
					gender_id, pat_weight_lbs, pat_weight_kg, pat_height_in, pat_height_cm, ethnicity_id, occupation_id, pat_stat_preg,
					rel_test_id, pat_dob_partial, rand_num, confidential, number_of_patients, deleted, child_only, pat_bmi, pat_body_area,
					notes, pat_country_id, pat_age_at_vacc, age_unit_id_at_vacc, concom_therapy, enterprise_id)
					
         (SELECT cm.case_id, pcpi.pat_id, pcpi.pat_subj_num, pcpi.pat_initials, pcpi.pat_firstname,
				pcpi.pat_mi, pcpi.pat_lastname, pcpi.pat_address, pcpi.pat_city, pcpi.pat_state,
				pcpi.pat_postal_code, pcpi.pat_country, pcpi.pat_phone, pcpi.pat_dob, pcpi.pat_dob_res, pcpi.pat_age,
                pkg_util.f_get_argus_code ('LM_AGE_UNITS',
                                               pcpi.age_unit_id
                                              ),			   
				pkg_util.f_get_argus_code ('LM_AGE_GROUPS',
                                               pcpi.age_group_id
                                              ),
				pkg_util.f_get_argus_code ('LM_GENDER',
                                               pcpi.gender_id
                                              ),	
				pcpi.pat_weight_lbs, pcpi.pat_weight_kg, pcpi.pat_height_in, pcpi.pat_height_cm,
				pkg_util.f_get_argus_code ('LM_ETHNICITY',
                                               pcpi.ethnicity_id
                                              ),
				pkg_util.f_get_argus_code ('LM_OCCUPATIONS',
                                               pcpi.occupation_id
                                              ),
				pcpi.pat_stat_preg, pcpi.rel_test_id, pcpi.pat_dob_partial, pcpi.rand_num, pcpi.confidential,
				pcpi.number_of_patients, pcpi.deleted, pcpi.child_only, pcpi.pat_bmi, pcpi.pat_body_area, pcpi.notes,
				pkg_util.f_get_argus_code ('LM_COUNTRIES',
                                               pcpi.pat_country_id
                                              ),
				pcpi.pat_age_at_vacc, pcpi.age_unit_id_at_vacc, pcpi.concom_therapy,
				pkg_util.g_enterprise_id
           FROM scase_master cm, pnc_data_extract.pc_case_pat_info pcpi          
		   WHERE pcpi.case_id = cm.src_case_id
            AND cm.enterprise_id = pkg_util.g_enterprise_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PAT_INFO'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_pat_info',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_pat_info INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_pat_info , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_pat_info;

      PROCEDURE p_migrate_case_pat_tests
   AS
/***********************************************************************
 Name:        p_migrate_case_pat_tests
 Description: This procedure migrates case data to CASE_PAT_TESTS table.
 ***********************************************************************/
   BEGIN
      INSERT INTO scase_pat_tests
                  (rel_test_id, rel_tests, enterprise_id, deleted)
         SELECT cm.case_id, pcpt.rel_tests, pkg_util.g_enterprise_id,
                pcpt.deleted
           FROM scase_master cm, pnc_data_extract.pc_case_pat_tests pcpt WHERE cm.src_case_id = pcpt.rel_test_id;

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PAT_TESTS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_pat_tests',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_PAT_TESTS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_pat_tests , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_pat_tests;

   
   
   PROCEDURE p_migrate_case_hosp
   AS
      /***********************************************************************
      Name:        p_migrate_case_hosp
      Description: This procedure migrates case data to CASE_HOSP table.
      ***********************************************************************/

   BEGIN

            INSERT INTO scase_hosp
			           (case_id, seq_num, start_date, end_date, duration, prolonged, summary, event_caused, start_date_res,
					    start_date_partial, end_date_res, end_date_partial, deleted, enterprise_id)
				(SELECT cm.case_id, pch.seq_num, pch.start_date, pch.end_date, pch.duration, pch.prolonged, pch.summary, pch.event_caused, pch.start_date_res,
					    pch.start_date_partial, pch.end_date_res, pch.end_date_partial, pch.deleted, pkg_util.g_enterprise_id
				 FROM scase_master cm, pnc_data_extract.pc_case_hosp pch WHERE cm.src_case_id=pch.case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_HOSP'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_hosp',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_HOSP INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_hosp , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_hosp;


   PROCEDURE p_migrate_case_parent_info
   /***********************************************************************
    Name:        p_migrate_case_parent_info
    Description: This procedure migrates data to CASE_PARENT_INFO table.
        ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_parent_info
					(case_id, age_unit_id, date_of_lmp, gender_id, age, dob, dob_res, height_cm, initials,
					 weight_kg, dob_partial, weight_lbs, height_in, deleted, med_hist_text,
					 date_of_lmp_res, date_of_lmp_partial, enterprise_id)
			(SELECT cm.case_id, 
					pkg_util.f_get_argus_code ('LM_AGE_UNITS',
                                               pcpi.age_unit_id
                                              ),
					pcpi.date_of_lmp,
					pkg_util.f_get_argus_code ('LM_GENDER',
                                                pcpi.gender_id
                                               ),
					pcpi.age, pcpi.dob, pcpi.dob_res, pcpi.height_cm, pcpi.initials, pcpi.weight_kg, pcpi.dob_partial, 
					pcpi.weight_lbs, pcpi.height_in, pcpi.deleted, pcpi.med_hist_text, pcpi.date_of_lmp_res, 
					pcpi.date_of_lmp_partial, pkg_util.g_enterprise_id
			FROM scase_master cm, pnc_data_extract.pc_case_parent_info pcpi WHERE cm.src_case_id=pcpi.case_id); 

                  
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PARENT_INFO';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_parent_info',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_parent_info INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_parent_info , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_parent_info;   
   
   PROCEDURE p_migrate_case_product
   /***********************************************************************
   Name:        p_migrate_case_product
   Description: This procedure migrates case data to CASE_PRODUCT table.
   ***********************************************************************/
   AS
   
    v_count       NUMBER;
    l_seq_num     NUMBER;
	l_prod_id     NUMBER;
	l_prod_name   VARCHAR2(200);
	l_country_id  NUMBER;
	l_license_id  NUMBER;
	l_family_id	  NUMBER;
	l_generic_name VARCHAR2(1000);
	l_primary_event  NUMBER;
	
	CURSOR l_cur_prod IS 
			SELECT cm.case_id as case_id, pcp.case_id as src_case_id, pcp.seq_num as src_prod_seq_num, pcp.product_id as src_prod_id,
				   pcp.drug_type as drug_type, pcp.pat_exposure as pat_exposure, pcp.protocol_followed as protocol_followed, 
				   pcp.first_sus_prod as first_sus_prod, pcp.selected_view as selected_view, pcp.sort_id as sort_id, pcp.views_available as views_available,
					pcp.country_id as country_id, pcp.product_name as product_name, pcp.generic_name as generic_name, pcp.prod_reptd as prod_reptd,
					pcp.prod_coded as prod_coded, pcp.prod_code_status as prod_code_status, pcp.deleted as deleted, 
					pcp.STUDY_PRODUCT_NUM as STUDY_PRODUCT_NUM, pcp.primary_event as primary_event,
					pcp.family_id as family_id, pcp.existing_rpt_seq_mod_flg as existing_rpt_seq_mod_flg, pcp.notes as notes, pcp.prod_lic_id as prod_lic_id,
					pcp.LAST_UPDATE_TIME as LAST_UPDATE_TIME, pcp.VAERS_BLOCK_ID as VAERS_BLOCK_ID 
					FROM scase_master cm, pnc_data_extract.pc_case_product pcp
					WHERE pcp.case_id=cm.src_case_id;
	
	BEGIN		
	FOR rec IN l_cur_prod
		LOOP
		l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');
		
		SELECT COUNT(*) INTO v_count FROM scase_master cm, scase_event ce 
			WHERE cm.case_id=ce.case_id AND ce.ud_number_11= rec.primary_event AND cm.src_case_id=rec.src_case_id;
			
		IF v_count = 1 THEN
			
			SELECT ce.seq_num INTO l_primary_event FROM scase_master cm, scase_event ce
				WHERE cm.case_id=ce.case_id AND ce.ud_number_11= rec.primary_event AND cm.src_case_id=rec.src_case_id;
		ELSE
		
			l_primary_event:= NULL;
			
		END IF;
			
		
		IF rec.drug_type=1 THEN
		
			SELECT COUNT(*) INTO v_count
				FROM dm_product_mapping 
				WHERE s_prod_id = rec.src_prod_id
				AND s_prod_name = rec.product_name
				AND s_country_id = rec.country_id;
				--AND s_license_id = NVL(rec.license_id,0);
		
			IF	v_count >0 THEN
			     
				SELECT t_prod_id, t_prod_name, t_country_id, t_license_id INTO
					 l_prod_id, l_prod_name, l_country_id, l_license_id	
					FROM dm_product_mapping 
					WHERE s_prod_id = rec.src_prod_id
					AND s_prod_name = rec.product_name
					AND s_country_id = rec.country_id;
					--AND s_license_id = NVL(rec.license_id,0); 
					
				/*SELECT t_prod_id, t_prod_name, t_country_id INTO
					 l_prod_id, l_prod_name, l_country_id	
					FROM dm_product_mapping 
					WHERE s_prod_id = rec.src_prod_id
					AND s_prod_name = rec.product_name
					AND s_country_id = rec.country_id;	*/
					
				SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
					  WHERE product_id = l_prod_id;
				
				--l_license_id:= NULL;
				
			ELSE
				
				l_prod_id:= NULL;
				l_prod_name:= rec.product_name ;
				l_country_id:= pkg_util.f_get_argus_code ('LM_COUNTRIES',
														   rec.country_id
														  );
				l_license_id:= NULL;
				l_generic_name:= to_char(substr(rec.generic_name, 1, 1000));
			
			END IF;
			
			INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, protocol_followed, first_sus_prod, selected_view, sort_id, 
								views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status, deleted, 
								STUDY_PRODUCT_NUM, primary_event, family_id, existing_rpt_seq_mod_flg, notes, prod_lic_id, ud_number_11, VAERS_BLOCK_ID,
								LAST_UPDATE_TIME, enterprise_id)
					VALUES (rec.case_id, l_seq_num, l_prod_id, rec.drug_type, rec.pat_exposure, rec.protocol_followed, rec.first_sus_prod,
							rec.selected_view, rec.sort_id, rec.views_available, l_country_id, l_prod_name, l_generic_name, rec.prod_reptd, 
							rec.prod_coded, rec.prod_code_status, rec.deleted, rec.STUDY_PRODUCT_NUM, l_primary_event, l_family_id, 
							rec.existing_rpt_seq_mod_flg, rec.notes, l_license_id, rec.src_prod_seq_num, rec.VAERS_BLOCK_ID, 
						    rec.LAST_UPDATE_TIME, pkg_util.g_enterprise_id );
							
		ELSE
			
			INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, protocol_followed, first_sus_prod, selected_view, sort_id, 
								views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status, deleted, 
								STUDY_PRODUCT_NUM, primary_event, family_id, existing_rpt_seq_mod_flg, notes, prod_lic_id, ud_number_11, VAERS_BLOCK_ID,
								LAST_UPDATE_TIME, enterprise_id)
					VALUES (rec.case_id, l_seq_num, NULL, rec.drug_type, rec.pat_exposure, rec.protocol_followed, rec.first_sus_prod,
							rec.selected_view, rec.sort_id, rec.views_available, 
							pkg_util.f_get_argus_code ('LM_COUNTRIES',
                                                      rec.country_id
                                                     ),
							rec.product_name, rec.generic_name, rec.prod_reptd, 
							rec.prod_coded, rec.prod_code_status, rec.deleted, rec.STUDY_PRODUCT_NUM, l_primary_event, l_family_id, 
							rec.existing_rpt_seq_mod_flg, rec.notes, null, rec.src_prod_seq_num, rec.VAERS_BLOCK_ID,
							rec.LAST_UPDATE_TIME, pkg_util.g_enterprise_id );			
				
		END IF;

		END LOOP;
					
					
	    UPDATE dm_status
        SET status = 1,
             conv_end_time = SYSDATE
        WHERE table_name = 'CASE_PRODUCT';		

      COMMIT;
    EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_product',
                         'pkg_main',
                         SQLERRM,
                         'CASE product INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_product , please see table LOGGER_LOGS for detials'
            );
     END p_migrate_case_product;		
					
   
	PROCEDURE p_migrate_case_event_assess
   /***********************************************************************
    Name:        p_migrate_case_event_assess
    Description: This procedure migrates data to CASE_EVENT_ASSESS table.
    ***********************************************************************/
   AS
	  v_relatedness_cd             	varchar2(15);
	  v_rpt_relatedness_cd             	varchar2(15);
	  v_src_id						varchar2(15);
      v_det_causality_id           NUMBER;
      v_rpt_causality_id           NUMBER;
      v_det_listedness_id          NUMBER;
      v_sourc_chk                  NUMBER        := 0;
      v_source_id                  NUMBER;

      CURSOR c_cur_causality
      IS
          SELECT  cm.case_id AS case_id, cm.src_case_id AS src_case_id,
                  pkg_util.f_fetch_sequence ('CASE_EVENT_ASSESS') seq_num,
                  cp.seq_num AS prod_seq_num,
                  cp.ud_number_11 AS src_susp_drg_seq_nbr,
                  ce.seq_num AS event_seq_num,
                  ce.ud_number_11 AS src_event_seq_nbr, cm.deleted
             FROM scase_event ce, scase_product cp, scase_master cm
            WHERE ce.case_id = cp.case_id
              AND ce.case_id = cm.case_id
              AND cp.case_id = cm.case_id
              AND cp.drug_type = 1
              AND cp.product_id IS NOT NULL
         ORDER BY cm.case_id, cp.sort_id, ce.sort_id;

      CURSOR c_cur_listedness
      IS
          SELECT  cm.case_id as case_id, cm.src_case_id as src_case_id,
                  pkg_util.f_fetch_sequence ('CASE_EVENT_ASSESS') seq_num,
                  cp.seq_num AS prod_seq_num,
                  cp.ud_number_11 AS src_susp_drg_seq_nbr,
                  ce.seq_num AS event_seq_num,
                  ce.ud_number_11 AS src_event_seq_nbr, ll.license_id,
                  NVL (ll.datasheet_id, 0) datasheet_id, ld.sheet_name,
                  cm.deleted
             FROM scase_event ce,
                  scase_product cp,
                  scase_master cm,
                  lm_license ll,
                  lm_lic_products llp,
                  lm_datasheet ld
            WHERE ce.case_id = cp.case_id
              AND ce.case_id = cm.case_id
              AND cp.case_id = cm.case_id
              AND cp.drug_type = 1
              AND ll.license_id = llp.license_id
              AND NVL (ll.datasheet_id, 0) = ld.datasheet_id
              AND cp.product_id = llp.product_id
              AND ll.deleted IS NULL
              AND llp.deleted IS NULL
              AND ld.deleted IS NULL
         ORDER BY cm.case_id, cp.sort_id, ce.sort_id;
		 
   BEGIN
   
   

     FOR z IN c_cur_causality
      LOOP
         v_det_causality_id := NULL;
         v_rpt_causality_id := NULL;
         v_source_id := NULL;
		 


         SELECT COUNT (1)
           INTO v_sourc_chk
           FROM pnc_data_extract.pc_case_event_assess 
          WHERE case_id = z.src_case_id
            AND event_seq_num = z.src_event_seq_nbr
            AND prod_seq_num = z.src_susp_drg_seq_nbr
			AND license_id=0
			AND datasheet_id=0;
			


         IF v_sourc_chk > 0
         THEN
            SELECT DECODE (NVL (pcea.RPT_CAUSALITY_ID, 0),
                           0, NULL,
                           pkg_util.f_get_argus_code ('LM_CAUSALITY',
                                                      pcea.RPT_CAUSALITY_ID
                                                     )
                          ) ,
                   DECODE (NVL (pcea.DET_CAUSALITY_ID, 0),
                           0, NULL,
                           pkg_util.f_get_argus_code ('LM_CAUSALITY',
                                                      pcea.DET_CAUSALITY_ID
                                                     )
                          ) ,
					DECODE (NVL (pcea.SOURCE_ID, 0),
                             0, NULL,
                             pkg_util.f_get_argus_code ('LM_CAUSALITY_SOURCE',
                                                        pcea.SOURCE_ID
                                                        )
                            ) 
              INTO v_rpt_relatedness_cd,
                   v_relatedness_cd,
				   v_src_id
              FROM pnc_data_extract.pc_case_event_assess pcea
             WHERE pcea.case_id = z.src_case_id
               AND pcea.event_seq_num = z.src_event_seq_nbr
               AND pcea.prod_seq_num = z.src_susp_drg_seq_nbr
			   AND pcea.license_id=0
			   AND pcea.datasheet_id=0;
         END IF;
		 

		 v_det_causality_id := v_relatedness_cd;
         v_rpt_causality_id := v_rpt_relatedness_cd;
		 v_source_id := v_src_id;

         INSERT INTO scase_event_assess
                     (case_id, datasheet_id, license_id, det_causality_id,
                      rpt_causality_id, det_listedness_id, source_id,
                      event_seq_num, lam_assessed, prod_seq_num,
                      listed_justify, seq_num, cause_justify, enterprise_id,
                      deleted
                     )
              VALUES (z.case_id, 0, 0, v_det_causality_id,
                      v_rpt_causality_id, NULL, v_source_id,
                      z.event_seq_num, 0, z.prod_seq_num,
                      NULL, z.seq_num, NULL, pkg_util.g_enterprise_id,
                      z.deleted
                     );
      END LOOP;
	  


      FOR z IN c_cur_listedness
      LOOP
         v_det_listedness_id := NULL;
         v_source_id := NULL;


		 
         SELECT COUNT (1)
           INTO v_sourc_chk
           FROM pnc_data_extract.pc_case_event_assess
          WHERE case_id = z.src_case_id
            AND event_seq_num = z.src_event_seq_nbr
            AND prod_seq_num = z.src_susp_drg_seq_nbr
			   AND license_id<>0
			   AND datasheet_id<>0;

         IF v_sourc_chk > 0
         THEN
            SELECT DISTINCT pcea.DET_LISTEDNESS_ID,
					DECODE (NVL (pcea.SOURCE_ID, 0),
                             0, NULL,
                             pkg_util.f_get_argus_code ('LM_CAUSALITY_SOURCE',
                                                        pcea.SOURCE_ID
                                                        )
                            ) source_id
              INTO v_det_listedness_id,
				   v_source_id
              FROM pnc_data_extract.pc_case_event_assess pcea
             WHERE pcea.case_id = z.src_case_id
               AND pcea.event_seq_num = z.src_event_seq_nbr
               AND pcea.prod_seq_num = z.src_susp_drg_seq_nbr
			   AND pcea.license_id<>0
			   AND pcea.datasheet_id<>0;
         END IF;


		         
         INSERT INTO scase_event_assess
                     (case_id, datasheet_id, license_id, det_causality_id,
                      rpt_causality_id, det_listedness_id, source_id,
                      event_seq_num, lam_assessed, prod_seq_num,
                      listed_justify, seq_num, cause_justify, enterprise_id,
                      deleted
                     )
              VALUES (z.case_id, z.datasheet_id, z.license_id, NULL,
                      NULL, v_det_listedness_id, v_source_id,
                      z.event_seq_num, 0, z.prod_seq_num,
                      NULL, z.seq_num, NULL, pkg_util.g_enterprise_id,
                      z.deleted
                     );
      END LOOP;


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT_ASSESS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_event_assess',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_EVENT_ASSESS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_event_assess , please see table S_LOGGER_LOGS for detials'
            );
   END p_migrate_case_event_assess;
   
   PROCEDURE p_migrate_case_dose_regimens
/***********************************************************************
Name:        p_migrate_case_dose_regimens
Description: This procedure migrates case data to CASE_DOSE_REGIMENS table.
***********************************************************************/
   AS

   BEGIN
   
   
	  INSERT INTO scase_dose_regimens
				(case_id, seq_num, log_no, start_datetime, start_datetime_res, stop_datetime, stop_datetime_res, ongoing,
				 off_label, dose, dose_unit_id, freq_id, admin_route_id, daily_dose, daily_dose_unit_id, exp_date, exp_date_res,
				 total_reg_dose, tot_reg_dose_unit_id, sort_id, dose_no, par_admin_route,
				 exp_date_partial, start_date_partial, stop_date_partial, dose_description, package_id, lot_no, 
				 deleted, duration_seconds, last_update_time, vaers_block_10,
				 vaers_block_14, duration, enterprise_id)
			(SELECT cm.case_id, cp.seq_num, pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'), pcdr.start_datetime, 
			        pcdr.start_datetime_res, pcdr.stop_datetime, pcdr.stop_datetime_res, pcdr.ongoing,
				    pcdr.off_label, pcdr.dose, 
					pkg_util.f_get_argus_code ('LM_DOSE_UNITS',
                                                pcdr.dose_unit_id
                                                ),
					pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY',
                                                pcdr.freq_id
                                              ),
					pkg_util.f_get_argus_code ('LM_ADMIN_ROUTE',
                                                pcdr.admin_route_id
                                               ),
					pcdr.daily_dose,
				    pkg_util.f_get_argus_code ('LM_DOSE_UNITS',
                                                pcdr.daily_dose_unit_id
                                               ),
					pcdr.exp_date, pcdr.exp_date_res, pcdr.total_reg_dose, 
				    pkg_util.f_get_argus_code ('LM_DOSE_UNITS',
                                                pcdr.tot_reg_dose_unit_id
                                               ),
					pcdr.sort_id, pcdr.dose_no, 
                    pkg_util.f_get_argus_code ('LM_ADMIN_ROUTE',
                                                pcdr.par_admin_route
                                               ),
					pcdr.exp_date_partial, pcdr.start_date_partial, pcdr.stop_date_partial, pcdr.dose_description, pcdr.package_id, pcdr.lot_no,
					pcdr.deleted, pcdr.duration_seconds, pcdr.last_update_time, pcdr.vaers_block_10, pcdr.vaers_block_14, pcdr.duration, 
					pkg_util.g_enterprise_id
					FROM scase_master cm, scase_product cp, pnc_data_extract.pc_case_dose_regimens pcdr
					 WHERE cm.case_id = cp.case_id
					  AND  cp.ud_number_11 = pcdr.seq_num
					  AND  cm.src_case_id = pcdr.case_id);
      

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_DOSE_REGIMENS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_dose_regimens',
                         'pkg_main',
                         SQLERRM,
                         'case_dose_regimens INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_dose_regimens , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_dose_regimens;
  
   PROCEDURE p_migrate_case_prod_ingredient
/***********************************************************************
Name:        p_migrate_case_prod_ingredient
Description: This procedure migrates case data to CASE_PROD_INGREDIENT table.
***********************************************************************/
   AS

   BEGIN
	  EXECUTE IMMEDIATE 'analyze table scase_product compute statistics';
   
	  INSERT INTO scase_prod_ingredient
				(case_id, seq_num, item, ingredient_id, conc_unit_id,
                 concentration, enterprise_id, deleted)
         SELECT   cp.case_id, cp.seq_num,
                  pkg_util.f_fetch_sequence ('CASE_PROD_INGREDIENT'),
                  lpc.ingredient_id, lpc.conc_unit_id, lpc.concentration,
                  pkg_util.g_enterprise_id,
                  cp.deleted
             FROM scase_product cp, lm_product_concentrations lpc
            WHERE cp.product_id = lpc.product_id
              AND cp.product_id IS NOT NULL
              AND lpc.deleted IS NULL
         ORDER BY cp.case_id, cp.seq_num;
      
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PROD_INGREDIENT'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_prod_ingredient',
                         'pkg_main',
                         SQLERRM,
                         'case_dose_regimens INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_prod_ingredient , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_prod_ingredient;
   
   PROCEDURE p_migrate_case_prod_indication
   /***********************************************************************
   Name:        p_migrate_case_prod_indication
   Description: This procedure migrates case data to CASE_PROD_INDICATIONS table.
   ***********************************************************************/
   AS

   BEGIN
      INSERT INTO scase_prod_indications
                  (case_id, prod_seq_num, seq_num, sort_id, ind_reptd,
                   ind_code, ind_coded, ind_hlgt, ind_hlgt_code, ind_hlt,
                   ind_hlt_code, ind_llt, ind_llt_code, ind_soc,
                   ind_soc_code, ind_pref_term, deleted, enterprise_id)
         (SELECT   cm.case_id, cp.seq_num,
                  pkg_util.f_fetch_sequence ('CASE_PROD_INDICATIONS'),
                  pcpi.sort_id, pcpi.ind_reptd,
                  pcpi.ind_code, pcpi.ind_coded, pcpi.ind_hlgt, pcpi.ind_hlgt_code, pcpi.ind_hlt,
                  pcpi.ind_hlt_code, pcpi.ind_llt, pcpi.ind_llt_code, pcpi.ind_soc,
                  pcpi.ind_soc_code, pcpi.ind_pref_term, pcpi.deleted, pkg_util.g_enterprise_id 
             FROM pnc_data_extract.pc_case_prod_indications pcpi, scase_master cm, scase_product cp
            WHERE pcpi.prod_seq_num = cp.ud_number_11
              AND pcpi.case_id = cm.src_case_id
              AND cp.case_id= cm.case_id);

        
      UPDATE scase_prod_indications
         SET ind_code_dict = pkg_util.g_argus_meddra_dict_id,
             ind_code_status = 1
       WHERE ind_llt_code IS NOT NULL;


      UPDATE scase_prod_indications
         SET ind_code_status = 0
       WHERE ind_llt_code IS NULL;

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PROD_INDICATIONS';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_prod_indication',
                         'pkg_main',
                         SQLERRM,
                         'CASE_PROD_INDICATIONS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_prod_indication , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_prod_indication;

   
      PROCEDURE p_migrate_case_prod_drugs
   /***********************************************************************
   Name:        p_migrate_case_prod_drugs
   Description: This procedure migrates case data to CASE_PROD_DRUGS table.
   ***********************************************************************/
   AS

   BEGIN
      INSERT INTO scase_prod_drugs
				  (case_id, seq_num, formulation_id, concentration, conc_units_id, act_taken_id, severity_id, interaction,
				   contraind, first_dose, first_dose_res, last_dose, last_dose_res, total_dose, tot_dose_unit_id, prev_use,
				   abuse, overdose, protocol, dechallenge, dechall_date, rechallenge, rechall_start, rechall_stop, rechall_outcome,
				   sort_id, b_weight_grams, n_siblings, b_weight_ozs, first_dose_partial, last_dose_partial, tampering, cumulative_dose,
				   deleted, cumulative_dose_unit_id, obtain_drug_country_id, drug_auth_country_id, duration_seconds,
					latency_seconds, delay_seconds, duration, delay, latency, batch_in_spec, batch_out_spec, counterfeit, enterprise_id )
	            (SELECT cm.case_id, cp.seq_num,
						pkg_util.f_get_argus_code ('LM_FORMULATION',
													pcpd.formulation_id
													),
						pcpd.concentration,
						pkg_util.f_get_argus_code ('LM_DOSE_UNITS',
													pcpd.conc_units_id
													),
						pkg_util.f_get_argus_code ('LM_ACTION_TAKEN',
													pcpd.act_taken_id
													),
						pkg_util.f_get_argus_code ('LM_SEVERITY',
													pcpd.severity_id
													),	
						pcpd.interaction, pcpd.contraind, pcpd.first_dose, pcpd.first_dose_res, pcpd.last_dose, pcpd.last_dose_res, pcpd.total_dose,
						pkg_util.f_get_argus_code ('LM_DOSE_UNITS',
													pcpd.tot_dose_unit_id
													),
						pcpd.prev_use, pcpd.abuse, pcpd.overdose, pcpd.protocol, pcpd.dechallenge, pcpd.dechall_date, pcpd.rechallenge, 
						pcpd.rechall_start, pcpd.rechall_stop, pcpd.rechall_outcome, pcpd.sort_id, pcpd.b_weight_grams, pcpd.n_siblings, 
						pcpd.b_weight_ozs, pcpd.first_dose_partial, pcpd.last_dose_partial, pcpd.tampering, pcpd.cumulative_dose, pcpd.deleted,
						pkg_util.f_get_argus_code ('LM_DOSE_UNITS',
													pcpd.cumulative_dose_unit_id
													),	
						pkg_util.f_get_argus_code ('LM_COUNTRIES',
													pcpd.obtain_drug_country_id
													),
						pkg_util.f_get_argus_code ('LM_COUNTRIES',
													pcpd.drug_auth_country_id
													),
						pcpd.duration_seconds, pcpd.latency_seconds, pcpd.delay_seconds, pcpd.duration, pcpd.delay, pcpd.latency, pcpd.batch_in_spec, 
						pcpd.batch_out_spec, pcpd.counterfeit, pkg_util.g_enterprise_id 													
						FROM scase_master cm, scase_product cp, pnc_data_extract.pc_case_prod_drugs pcpd
						WHERE cm.case_id = cp.case_id 
						AND   cm.src_case_id = pcpd.case_id
						AND   cp.ud_number_11 = pcpd.seq_num);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PROD_DRUGS';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_prod_drugs',
                         'pkg_main',
                         SQLERRM,
                         'CASE_PROD_DRUGS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_prod_drugs , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_prod_drugs;
   
      PROCEDURE p_migrate_case_prod_devices
   /***********************************************************************
   Name:        p_migrate_case_prod_devices
   Description: This procedure migrates case data to CASE_PROD_DEVICES table.
   ***********************************************************************/
   AS

   BEGIN
      INSERT INTO scase_prod_devices(case_id, seq_num,DATE_EXPLANT_RES, EXP_DATE_RES, sort_id, deleted, MFG_DATE_RES,enterprise_id )
	         (SELECT cm.case_id, cp.seq_num, pcpd.DATE_EXPLANT_RES, pcpd.EXP_DATE_RES, pcpd.sort_id, pcpd.deleted, pcpd.MFG_DATE_RES, pkg_util.g_enterprise_id
     			 FROM scase_master cm, scase_product cp, pnc_data_extract.pc_case_prod_devices pcpd
				  WHERE cm.case_id=cp.case_id
				  AND cm.src_case_id=pcpd.case_id
				  AND cp.ud_number_11=pcpd.seq_num);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PROD_DEVICES';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_prod_devices',
                         'pkg_main',
                         SQLERRM,
                         'CASE_PROD_DEVICES INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_prod_devices , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_prod_devices;   
   
   
   
      PROCEDURE p_migrate_case_comments
   /***********************************************************************
   Name:        p_migrate_case_comments
   Description: This procedure migrates case data to CASE_COMMENTS table.
   ***********************************************************************/
   AS

   BEGIN
      INSERT INTO scase_comments(case_id, deleted, comment_txt, enterprise_id )
	         (SELECT cm.case_id, pcc.deleted, pcc.comment_txt, pkg_util.g_enterprise_id
                     FROM scase_master cm, pnc_data_extract.pc_case_comments pcc WHERE cm.src_case_id = pcc.case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_COMMENTS';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_comments',
                         'pkg_main',
                         SQLERRM,
                         'CASE_COMMENTS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_comments , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_comments;
   
     PROCEDURE p_migrate_case_company_cmts
   /***********************************************************************
   Name:        p_migrate_case_company_cmts
   Description: This procedure migrates case data to CASE_COMPANY_CMTS table.
   ***********************************************************************/
   AS

   BEGIN
      INSERT INTO scase_company_cmts(case_id, deleted, comment_txt, enterprise_id )
	         (SELECT cm.case_id, pcc.deleted, pcc.comment_txt, pkg_util.g_enterprise_id
                     FROM scase_master cm, pnc_data_extract.pc_case_company_cmts pcc WHERE cm.src_case_id = pcc.case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_COMPANY_CMTS';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_company_cmts',
                         'pkg_main',
                         SQLERRM,
                         'CASE_COMPANY_CMTS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_company_cmts , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_company_cmts;   
   
     PROCEDURE p_migrate_case_medwatch_data
   /***********************************************************************
   Name:        p_migrate_case_medwatch_data
   Description: This procedure migrates case data to CASE_MEDWATCH_DATA table.
   ***********************************************************************/
   AS

   BEGIN
      INSERT INTO scase_medwatch_data(case_id, adverse_event, product_problem, FACILITY_DISTRIBUTOR, report_source, 
									  pre1938, otc, narrative_text, enterprise_id)
                (SELECT cm.case_id, pcmd.adverse_event, pcmd.product_problem, pcmd.FACILITY_DISTRIBUTOR, pcmd.report_source, 
						pcmd.pre1938, pcmd.otc, pcmd.narrative_text, pkg_util.g_enterprise_id
				        FROM scase_master cm, pnc_data_extract.pc_case_medwatch_data pcmd 
						WHERE cm.src_case_id = pcmd.case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_MEDWATCH_DATA';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_medwatch_data',
                         'pkg_main',
                         SQLERRM,
                         'CASE_MEDWATCH_DATA INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_medwatch_data , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_medwatch_data;     

   PROCEDURE p_migrate_case_event_detail
   /***********************************************************************
   Name:        p_migrate_case_event_detail
   Description: This procedure migrates case data to CASE_EVENT_DETAIL table.
   ***********************************************************************/
   AS
   BEGIN
     
	 INSERT INTO scase_event_detail
				(case_id, seq_num, prod_seq_num, event_seq_num, onset_delay, onset_delay_seconds, onset_latency,
				onset_latency_seconds, deleted, enterprise_id)
	        (SELECT cm.case_id, pkg_util.f_fetch_sequence ('CASE_EVENT_DETAIL'), cp.seq_num, ce.seq_num,
					pced.onset_delay, pced.onset_delay_seconds, pced.onset_latency, pced.onset_latency_seconds, pced.deleted, pkg_util.g_enterprise_id
					FROM scase_master cm,
					     scase_event ce,
						 scase_product cp,
						 pnc_data_extract.pc_case_event_detail pced
					WHERE cm.case_id = ce.case_id
					AND   cm.case_id = ce.case_id
					AND   pced.case_id = cm.src_case_id
					AND   cp.ud_number_11 = pced.prod_seq_num
					AND   ce.ud_number_11 = pced.event_seq_num);
	  
	  
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT_DETAIL'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_event_detail',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_EVENT_DETAIL INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_event_detail , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_event_detail;   

   PROCEDURE p_migrate_case_ep_rptblty
   /***********************************************************************
   Name:        p_migrate_case_ep_rptblty
   Description: This procedure migrates case data to CASE_EVENT_PRODUCT_RPTBLTY table.
   ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_event_product_rptblty
                  (case_id, reportability, event_seq_num, prod_seq_num,
                   enterprise_id, deleted)
         (SELECT cm.case_id, 
				 pcepr.reportability,
				 ce.seq_num, cp.seq_num, pkg_util.g_enterprise_id, pcepr.deleted
				 FROM scase_master cm,
					  scase_event ce,
					  scase_product cp,
					  pnc_data_extract.pc_case_event_product_rptblty pcepr
				WHERE cm.case_id = ce.case_id
				AND   cm.case_id = cp.case_id
				AND   pcepr.case_id = cm.src_case_id
				AND   pcepr.event_seq_num = ce.ud_number_11
				AND   pcepr.prod_seq_num = cp.ud_number_11) ;

											
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_ep_rptblty',
                         'pkg_main',
                         SQLERRM,
                         'CASE_EVENT_PRODUCT_RPTBLTY INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_ep_rptblty , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_ep_rptblty;   

   
  -- PROCEDURE p_migrate_case_notes_attach
   /***********************************************************************
    Name:        p_migrate_case_notes_attach
    Description: This procedure migrates data to SCASE_NOTES_ATTACH table.
    ***********************************************************************/
 /*  AS


BEGIN


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_NOTES_ATTACH';


      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_notes_attach',
                         'PKG_MAIN',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_NOTES_ATTACH INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_notes_attach , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_notes_attach;
   */

   PROCEDURE p_migrate_case_eu_device
   /***********************************************************************
    Name:        p_migrate_case_eu_device
    Description: This procedure migrates data to CASE_EU_DEVICE table.
    ***********************************************************************/
   AS
   
BEGIN

	INSERT INTO SCASE_EU_DEVICE(case_id, expected_date_res, projected_timing, projected_timing_final, 
								corrective_action_final, investigation_result, further_investigation, current_dev_locations,
								countries_of_distribution, deleted, corrective_action, seq_num, prod_seq_num, update_to_report, final_report,
								manufacturer_comments, enterprise_id)
			(SELECT cm.case_id, pced.expected_date_res, pced.projected_timing, pced.projected_timing_final, pced.corrective_action_final, 
					pced.investigation_result, pced.further_investigation, pced.current_dev_locations, pced.countries_of_distribution, 
					pced.deleted, pced.corrective_action, pkg_util.f_fetch_sequence ('CASE_EU_DEVICE'), cp.seq_num, pced.update_to_report, 
					pced.final_report, pced.manufacturer_comments, pkg_util.g_enterprise_id
					FROM scase_master cm, scase_product cp, pnc_data_extract.pc_case_eu_device pced
					WHERE cm.case_id = cp.case_id
					AND   cp.ud_number_11 = pced.prod_seq_num
					AND   pced.case_id = cm.src_case_id);					

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EU_DEVICE';


      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_eu_device',
                         'PKG_MAIN',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_EU_DEVICE INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_eu_device , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_eu_device;
   
   PROCEDURE p_migrate_case_justifications
   /***********************************************************************
    Name:        p_migrate_case_justifications
    Description: This procedure migrates data to CASE_JUSTIFICATIONS table.
    ***********************************************************************/
   AS
   
BEGIN

	  INSERT INTO SCASE_JUSTIFICATIONS(case_id, field_id, primary_seq_num, alt_seq_num, j_text, UPDATED_TIME, deleted, enterprise_id) (SELECT cm.case_id, pcj.field_id, pcj.primary_seq_num, pcj.alt_seq_num, pcj.j_text, pkg_util.g_migration_date, pcj.deleted, pkg_util.g_enterprise_id
	   FROM scase_master cm, pnc_data_extract.pc_case_justifications pcj where pcj.case_id=cm.src_case_id and cm.state_id=1);
	  				

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_JUSTIFICATIONS';


      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_justifications',
                         'PKG_MAIN',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_JUSTIFICATIONS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_justifications , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_justifications;
  
   PROCEDURE p_migrate_case_vacc_vaers
   /***********************************************************************
    Name:        p_migrate_case_vacc_vaers
    Description: This procedure migrates data to CASE_VACC_VAERS table.
    ***********************************************************************/
   AS
   
BEGIN

	INSERT INTO sCASE_VACC_VAERS(case_id, enterprise_id)
			(SELECT cm.case_id, pkg_util.g_enterprise_id
			 FROM scase_master cm, pnc_data_extract.pc_case_vacc_vaers pcvv WHERE cm.src_case_id = pcvv.case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_VACC_VAERS';


      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_vacc_vaers',
                         'PKG_MAIN',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_VACC_VAERS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_vacc_vaers , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_vacc_vaers;
  
  
   PROCEDURE p_migrate_case_event_rech_prod
   /***********************************************************************
    Name:        p_migrate_case_event_rech_prod
    Description: This procedure migrates data to CASE_EVENT_RECH_PROD table.
    ***********************************************************************/
   AS
   
BEGIN

	INSERT INTO SCASE_EVENT_RECH_PROD(case_id, event_seq_num, seq_num, product_seq_num, deleted, enterprise_id)
			(SELECT cm.case_id, ce.seq_num, pkg_util.f_fetch_sequence ('CASE_EVENT_RECH_PROD'), cp.seq_num, pcerp.deleted, pkg_util.g_enterprise_id
			 FROM scase_master cm, 
			      scase_event ce,
				  scase_product cp,
				  pnc_data_extract.pc_case_event_rech_prod pcerp 
			WHERE cm.src_case_id = pcerp.case_id
			AND   cp.ud_number_11 = pcerp.product_seq_num
			AND   ce.ud_number_11 = pcerp.event_seq_num
			AND   cm.case_id = cp.case_id
			AND   cm.case_id = ce.case_id);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT_RECH_PROD';


      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_event_rech_prod',
                         'PKG_MAIN',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_EVENT_RECH_PROD INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_event_rech_prod , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_event_rech_prod;

   --   PROCEDURE p_migrate_case_access
  /***********************************************************************
    Name:        p_migrate_case_access
    Description: This procedure migrates data to case_access table.
    ***********************************************************************/
  /* AS
   BEGIN
      INSERT INTO scase_access
				(case_id, listedness, outcome, seriousness, serious_notes, agent_suspect, company_diagnosis, diagnosis_pref_code,
				  diagnosis_inc_code, diagnosis_inc_term, diagnosis_hlgt_code, diagnosis_hlgt, diagnosis_hlt_code, diagnosis_hlt, diagnosis_soc_code,
				  diagnosis_body_sys, evaluation, co_suspect_count, event_synopsis, event_primary, diagnosis_reptd, diagnosis_coded, diagnosis_syn_code,
				  diagnosis_code_status, deleted, enterprise_id)
			(SELECT cm.case_id, 
					pkg_util.f_get_argus_code ('LM_LISTEDNESS',
												pca.listedness),
					pkg_util.f_get_argus_code ('LM_EVT_OUTCOME',
												pca.outcome),
					pca.seriousness, pca.serious_notes, pca.agent_suspect, pca.company_diagnosis, pca.diagnosis_pref_code, pca.diagnosis_inc_code, 
					pca.diagnosis_inc_term, pca.diagnosis_hlgt_code, pca.diagnosis_hlgt, pca.diagnosis_hlt_code, pca.diagnosis_hlt, pca.diagnosis_soc_code,
				    pca.diagnosis_body_sys, pca.evaluation, pca.co_suspect_count, pca.event_synopsis, pca.event_primary, pca.diagnosis_reptd, 
					pca.diagnosis_coded, pca.diagnosis_syn_code, pca.diagnosis_code_status, pca.deleted, pkg_util.g_enterprise_id
             FROM scase_master cm, pnc_data_extract.pc_case_access pca
			 WHERE cm.src_case_id = pca.case_id);

      UPDATE scase_access
         SET diagnosis_dict_id = pkg_util.g_argus_meddra_dict_id
       WHERE diagnosis_inc_code IS NOT NULL;

		 

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_ACCESS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_access',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_access INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_access , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_access;*/


   PROCEDURE p_migrate_case_e2batchmt_stat

      /***********************************************************************
       Name:        p_migrate_case_e2batchmt_stat
       Description: This procedure migrates case data to CASE_E2BATTACHMENT_STATUS table.
       ***********************************************************************/
AS

	BEGIN
	
	  INSERT INTO scase_e2battachment_status(case_id, processed, enterprise_id)
			(SELECT cm.case_id, pces.processed, pkg_util.g_enterprise_id
			FROM scase_master cm, pnc_data_extract.pc_case_e2battachment_status pces WHERE cm.src_case_id = pces.case_id);


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_E2BATTACHMENT_STATUS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_e2batchmt_stat',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_E2BATTACHMENT_STATUS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_e2batchmt_stat , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_e2batchmt_stat;
   
    PROCEDURE p_migrate_case_cmn_reg_reports
/***********************************************************************
  Name:        p_migrate_case_cmn_reg_reports
  Description: This procedure migrates data to SCASE_REG_REPORTS and SCMN_REG_REPORTS table.
  ***********************************************************************/
   AS
   
   l_reg_report_id number;
   l_product_id number;
   l_license_id number;
   l_country_id number;
   l_seq_num   number;
   l_route_date date ;
   l_comment_txt varchar2(2000);
   l_to_state_id number; 
   l_from_report_state_id number;
   v_count number;
   l_case_id number;
   l_date_assessed date;
   test_count  number;
   
   
   CURSOR c1
      IS SELECT pcmnr.reg_report_id , pcmnr.due_date , pcmnr.date_approved, pcmnr.date_generated, 
				pcmnr.date_submitted, pcmnr.date_xmit, pcmnr.date_scheduled, pcmnr.aware_date, 
				pcmnr.product_id , pcmnr.license_id , pcmnr.country_id , pcmnr.report_form_id,
				pcmnr.pdf_id, pcmnr.language_id, pcmnr.followup_num, pcmnr.case_rev, pcmnr.state_id, 
				pcmnr.owner_id, pcmnr.last_state_id, pcmnr.timeframe, pcmnr.auto_sched, 
				pcmnr.agency_id, pcmnr.submit_required , pcmnr.template_id, pcmnr.method , 
				pcmnr.output_status, pcmnr.transmit_id, pcmnr.status, pcmnr.aware_method , 
				pcmnr.esm_report_id, pcmnr.tracking_num, pcmnr.event, pcmnr.body_sys, 
				pcmnr.status_text, pcmnr.submit_notes, pcmnr.protect, pcmnr.deleted, pcmnr.rpt_comment, 
				pcmnr.prod_seq_num, pcmnr.report_form_type, pcmnr.message_type_id, pcmnr.susar, 
				pcmnr.last_update_time, pcmnr.safety_date, pcmnr.auto_distribute_reports, 
				pcmnr.blind_study_product, pcmnr.nullification, pcmnr.prev_esm_rpt_id, 
				pcmnr.nullification_reason, pcmnr.no_followup_downgrade, pcmnr.local_report, 
				pcmnr.reg_rpt_rules_id , pcmnr.schedule_method , pcmnr.schedule_date , 
				pcmnr.generate_rpt, pcmnr.amendment
				FROM PNC_DATA_EXTRACT.pc_cmn_reg_reports pcmnr;

   BEGIN


   
		FOR z IN c1
        LOOP
			l_reg_report_id := pkg_util.f_fetch_sequence ('CMN_REG_REPORTS');
			
			
			SELECT COUNT(*) INTO v_count 
			FROM dm_product_mapping_report 
			WHERE s_product_id = z.product_id
			AND s_license_id = z.license_id
			AND s_country_id = z.country_id;
			
			IF v_count >0 THEN
			
			SELECT t_product_id, t_license_id, t_country_id INTO l_product_id, l_license_id, l_country_id 
			FROM dm_product_mapping_report 
			WHERE s_product_id = z.product_id
			AND s_license_id = z.license_id
			AND s_country_id = z.country_id;
			
			ELSE
			l_product_id:= NULL;
			l_license_id:= NULL;
			l_country_id:= NULL; 
			
			END IF;
			

		    INSERT INTO scmn_reg_reports(reg_report_id, due_date, date_approved, date_generated, date_submitted, date_xmit, 
									date_scheduled, aware_date, product_id, license_id, country_id, report_form_id,
									pdf_id, language_id, followup_num, case_rev, state_id, last_state_id, 
									timeframe, auto_sched, agency_id, submit_required, template_id, method, 
									output_status, transmit_id, status, aware_method, tracking_num, 
									event, body_sys, status_text, submit_notes, protect, deleted, rpt_comment, 
									prod_seq_num, report_form_type, message_type_id, susar, last_update_time, 
									safety_date, auto_distribute_reports, blind_study_product, nullification, 
									nullification_reason, no_followup_downgrade, local_report, 
									schedule_method, schedule_date, generate_rpt, amendment, enterprise_id)
			  VALUES(l_reg_report_id, z.due_date, z.date_approved, z.date_generated, z.date_submitted, z.date_xmit, 
					z.date_scheduled, z.aware_date, l_product_id, l_license_id, l_country_id, z.report_form_id,
					z.pdf_id,
					pkg_util.f_get_argus_code ('LM_LANGUAGE',
												z.language_id),
					z.followup_num, z.case_rev, z.state_id, z.last_state_id, z.timeframe, z.auto_sched,
					pkg_util.f_get_argus_code ('LM_REGULATORY_CONTACT',
												z.agency_id),
					z.submit_required, z.template_id, z.method, z.output_status, z.transmit_id, z.status, 
					z.aware_method, z.tracking_num, z.event, z.body_sys, z.status_text, z.submit_notes, z.protect, 
					z.deleted, z.rpt_comment, z.prod_seq_num, z.report_form_type, z.message_type_id, z.susar, z.last_update_time, 
					z.safety_date, z.auto_distribute_reports, z.blind_study_product, z.nullification, z.nullification_reason, 
					z.no_followup_downgrade, z.local_report, z.schedule_method, z.schedule_date, z.generate_rpt, z.amendment, 
					pkg_util.g_enterprise_id);			

			
   
		select count(*) into v_count from scase_master where case_num = z.tracking_num and state_id<>1;
		
			IF v_count>0 then
		
					select  case_id into l_case_id from scase_master where case_num = z.tracking_num ;
		
					select count(*)  into test_count from pnc_data_extract.pc_case_reg_reports where 
					case_id=(select case_id from scase_master where case_num=z.tracking_num) AND reg_report_id=z.reg_report_id
					AND date_assessed is not null;
					
				If test_count<>0 then
					select date_assessed  into l_date_assessed from pnc_data_extract.pc_case_reg_reports where 
					case_id=(select case_id from scase_master where case_num=z.tracking_num) AND reg_report_id=z.reg_report_id;
				else
					l_date_assessed :=null;
				end if;
		


		
                INSERT INTO scase_reg_reports
                     (case_id, reg_report_id,
                      seq_num, date_assessed, enterprise_id
                     )
              VALUES (l_case_id, l_reg_report_id,
                      pkg_util.f_fetch_sequence ('CASE_REG_REPORTS'),l_date_assessed, pkg_util.g_enterprise_id);
					  
			END IF;			  
					  
			SELECT 	count(*) into v_count from pnc_data_extract.pc_rpt_routing pr WHERE pr.reg_report_id = z.reg_report_id;
			
			IF v_count>0 then
			
			
			/*SELECT 	pr.seq_num, pr.route_date, pr.comment_txt, pr.to_state_id, pr.from_report_state_id INTO 
			        l_seq_num, l_route_date, l_comment_txt, l_to_state_id, l_from_report_state_id 
					from PNC_DATA_EXTRACT.pc_rpt_routing pr WHERE pr.reg_report_id = z.reg_report_id;
					  
					  
			INSERT INTO srpt_routing(reg_report_id, seq_num, route_date, user_id, comment_txt, to_state_id, from_report_state_id,
								enterprise_id)
					VALUES (z.reg_report_id, l_seq_num, l_route_date, pkg_util.g_migration_user, l_comment_txt,
					        l_to_state_id, l_from_report_state_id, pkg_util.g_enterprise_id );*/
			
			INSERT INTO srpt_routing(reg_report_id, seq_num, route_date, user_id, comment_txt, to_state_id, from_report_state_id,justification_id,
								enterprise_id)
					(SELECT l_reg_report_id, pr.seq_num, pr.route_date, pkg_util.g_migration_user, pr.comment_txt, pr.to_state_id, pr.from_report_state_id,
					pr.justification_id, pkg_util.g_enterprise_id FROM PNC_DATA_EXTRACT.pc_rpt_routing pr WHERE pr.reg_report_id = z.reg_report_id);			
								
		
							
							

			END IF;
		 
		END LOOP;
		 
      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name IN
                       ('CASE_REG_REPORTS', 'CMN_REG_REPORTS', 'RPT_ROUTING');

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_cmn_reg_reports',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CMN and CASE_REG_REPORTS  INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_cmn_reg_reports , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_cmn_reg_reports; 
   
   
   
  PROCEDURE p_main_migrate (
      pi_enterprise_abbrv   IN   VARCHAR2,
      pi_prod_cd            IN   VARCHAR2,
      pi_migration_user     IN   VARCHAR2,                
      pi_migration_site     IN   VARCHAR2, 
      pi_database           IN   VARCHAR2 
   )
   AS
      /***********************************************************************
      Name:        p_main_migrate
      Description: This procedure migrates case data from Source to Staging.
      ***********************************************************************/
     
      v_status      NUMBER;
      v_max         NUMBER;
      v_nextval     NUMBER;
      v_increment   NUMBER;
      v_count       NUMBER;
   BEGIN
      pkg_util.p_initialize (pi_enterprise_abbrv,
                             pi_prod_cd,
                             pi_migration_user,
                             pi_migration_site
                            );

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_MASTER'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_MASTER'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         p_migrate_case_master;
      END IF;
	  

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         p_migrate_case_event; 
      END IF;

	 
      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_REFERENCE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_REFERENCE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         p_migrate_case_reference;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_LAB_DATA'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_LAB_DATA'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         p_migrate_case_lab_data;
      END IF;

      
      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_DEATH'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_DEATH'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_death;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_DEATH_DETAILS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_DEATH_DETAILS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         p_migrate_case_death_details;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PAT_HIST'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name IN
                     ('CASE_PAT_HIST')
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_pat_hist;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_REPORTERS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_REPORTERS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_reporters;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_ROUTING'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_ROUTING'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_routing;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_STUDY'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_STUDY'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_study;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_ASSESS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_ASSESS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

		  
         p_migrate_case_assess;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_FOLLOWUP'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_FOLLOWUP'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_followup;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_LITERATURE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_LITERATURE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

        
         p_migrate_case_literature;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PREGNANCY'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PREGNANCY'
	   AND enterprise_id = pkg_util.g_enterprise_id;

        
         p_migrate_case_pregnancy;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_NARRATIVE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

     
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_NARRATIVE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_narrative;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PAT_INFO'
	   AND enterprise_id = pkg_util.g_enterprise_id;

     
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PAT_INFO'
	   AND enterprise_id = pkg_util.g_enterprise_id;

        
         p_migrate_case_pat_info;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PAT_TESTS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PAT_TESTS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_pat_tests;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_HOSP'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_HOSP'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_hosp;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PARENT_INFO'
	   AND enterprise_id = pkg_util.g_enterprise_id;

     
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PARENT_INFO'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_parent_info;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PRODUCT'
	   AND enterprise_id = pkg_util.g_enterprise_id;
	   
	   
	         IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PRODUCT'
	   AND enterprise_id = pkg_util.g_enterprise_id;

        
         p_migrate_case_product;
      END IF;

    

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT_ASSESS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT_ASSESS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

        
         p_migrate_case_event_assess;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_DOSE_REGIMENS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_DOSE_REGIMENS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

		  
         p_migrate_case_dose_regimens;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PROD_INGREDIENT'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PROD_INGREDIENT'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_prod_ingredient;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PROD_INDICATIONS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PROD_INDICATIONS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_prod_indication;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PROD_DRUGS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PROD_DRUGS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_prod_drugs;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PROD_DEVICES'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PROD_DEVICES'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_prod_devices;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_COMMENTS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_COMMENTS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_comments;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_COMPANY_CMTS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_COMPANY_CMTS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

        
         p_migrate_case_company_cmts;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_MEDWATCH_DATA'
	   AND enterprise_id = pkg_util.g_enterprise_id;

     
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_MEDWATCH_DATA'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_medwatch_data;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT_DETAIL'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT_DETAIL'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_event_detail;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_ep_rptblty;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EU_DEVICE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EU_DEVICE'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_eu_device;
      END IF;
	  
	  
	  
	  
	    SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_JUSTIFICATIONS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_JUSTIFICATIONS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_justifications;
      END IF;
	  
	  
	  
	   SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_VACC_VAERS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_VACC_VAERS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_vacc_vaers;
      END IF;
	  
	  
	  
	   SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT_RECH_PROD'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT_RECH_PROD'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_event_rech_prod;
      END IF;

	  
	  
	/*  SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_ACCESS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_ACCESS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_access;
      END IF; */

	  
	  SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_E2BATTACHMENT_STATUS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_E2BATTACHMENT_STATUS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_e2batchmt_stat;
      END IF;
	  
	  
	  
      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_REG_REPORTS'
	   AND enterprise_id = pkg_util.g_enterprise_id;

      
      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name IN
                       ('CASE_REG_REPORTS', 'CMN_REG_REPORTS', 'RPT_ROUTING')
	   AND enterprise_id = pkg_util.g_enterprise_id;

         
         p_migrate_case_cmn_reg_reports;
      END IF;
 
      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_main_migrate',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'p_main_migrate'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_main_migrate , please see table LOGGER_LOGS for detials'
            );
   END p_main_migrate;   
 
   PROCEDURE p_push_argus_app
   AS
      /****************************************************************************
      Name:        p_push_argus_app
      Description: This procedure migrates case data from Staging to Argus schema
      *****************************************************************************/
      v_count            NUMBER;
      v_check            NUMBER;
      l_sort_id          NUMBER;
      l_first_sus_prod   NUMBER;
      l_case_id          NUMBER;
      l_sort_id_2        NUMBER;
      l_sort_id_1        NUMBER;



	 
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM dm_status
       WHERE status = 1;

      IF (v_count <> 0)
      THEN
         SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_MASTER'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_master
                    (case_id, case_num, rev, workflow_seq_num, last_workflow_seq_num, create_time, init_rept_date, user_id, 
				   last_update_time, last_update_user_id, requires_followup, followup_date, owner_id, state_id, country_id, lang_id, priority,
                   site_id,	seriousness, rpt_type_id, last_state_id, assessment_needed, priority_override, sid, safety_date, normal_time, max_time,
					report_scheduling, priority_assessment, close_user_id, close_date, close_notes, date_locked, deleted, due_soon, global_num, 
					priority_date_assessed, lam_assess_done, e2b_ww_number, worklist_owner_id, susar, last_update_event, initial_justification, force_soon,
					lock_status_id, medically_confirm, enterprise_id)
               (SELECT case_id, case_num, rev, workflow_seq_num, last_workflow_seq_num, create_time, init_rept_date, user_id, 
				   last_update_time, last_update_user_id, requires_followup, followup_date, owner_id, state_id, country_id, lang_id, priority,
                   site_id,	seriousness, rpt_type_id, last_state_id, assessment_needed, priority_override, sid, safety_date, normal_time, max_time,
					report_scheduling, priority_assessment, close_user_id, close_date, close_notes, date_locked, deleted, due_soon, global_num, 
					priority_date_assessed, lam_assess_done, e2b_ww_number, worklist_owner_id, susar, last_update_event, initial_justification, force_soon,
					lock_status_id, medically_confirm, enterprise_id
                 FROM scase_master);

				  
            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_MASTER';
			 
            COMMIT;
         END IF;

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event
               (SELECT *
                  FROM scase_event);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT';

			 
            COMMIT;
         END IF;
		 
		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_REFERENCE'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_reference
               (SELECT *
                  FROM scase_reference);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_REFERENCE';

			 
            COMMIT;
         END IF;		 
		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_LAB_DATA'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_lab_data
               (SELECT *
                  FROM scase_lab_data);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_LAB_DATA';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_DEATH'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_death
               (SELECT *
                  FROM scase_death);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_DEATH';

            COMMIT;
         END IF;		 
		 
		 


		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_DEATH_DETAILS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_death_details
               (SELECT *
                  FROM scase_death_details);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_DEATH_DETAILS';

			 
            COMMIT;
         END IF;		 
		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PAT_HIST'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_pat_hist
               (SELECT *
                  FROM scase_pat_hist);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PAT_HIST';

			 
            COMMIT;
         END IF;		 
		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_REPORTERS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_reporters
               (SELECT *
                  FROM scase_reporters);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_REPORTERS';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_ROUTING'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_routing
               (SELECT *
                  FROM SCASE_ROUTING);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_ROUTING';

			 
            COMMIT;
         END IF;		 
		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_STUDY'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_study
               (SELECT *
                  FROM scase_study);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_STUDY';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_ASSESS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_assess
               (SELECT *
                  FROM scase_assess);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_ASSESS';

			 
            COMMIT;
         END IF;		 
		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_FOLLOWUP'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_followup
               (SELECT *
                  FROM scase_followup);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_FOLLOWUP';

			 
            COMMIT;
         END IF;	


		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_LITERATURE'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_literature
               (SELECT *
                  FROM scase_literature);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_LITERATURE';

			 
            COMMIT;
         END IF;


		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PREGNANCY'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_pregnancy
               (SELECT *
                  FROM scase_pregnancy);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PREGNANCY';

			 
            COMMIT;
         END IF;
	
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_NARRATIVE'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_narrative
               (SELECT *
                  FROM scase_narrative);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_NARRATIVE';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PAT_INFO'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_pat_info
               (SELECT *
                  FROM scase_pat_info);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PAT_INFO';

			 
            COMMIT;
         END IF;

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PAT_TESTS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_pat_tests
               (SELECT *
                  FROM scase_pat_tests);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PAT_TESTS';

			 
            COMMIT;
         END IF;

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_HOSP'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_hosp
               (SELECT *
                  FROM scase_hosp);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_HOSP';

			 
            COMMIT;
         END IF;
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PARENT_INFO'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_parent_info
               (SELECT *
                  FROM scase_parent_info);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PARENT_INFO';

			 
            COMMIT;
         END IF;		 
		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PRODUCT'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_product
               (SELECT *
                  FROM scase_product);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PRODUCT';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT_ASSESS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event_assess
               (SELECT *
                  FROM scase_event_assess);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT_ASSESS';

			 
            COMMIT;
         END IF;


		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PROD_DRUGS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_prod_drugs
               (SELECT *
                  FROM scase_prod_drugs);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PROD_DRUGS';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_DOSE_REGIMENS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_dose_regimens
               (SELECT *
                  FROM scase_dose_regimens);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_DOSE_REGIMENS';

			 
            COMMIT;
         END IF;		 

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PROD_INGREDIENT'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_prod_ingredient
               (SELECT *
                  FROM scase_prod_ingredient);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PROD_INGREDIENT';

			 
            COMMIT;
         END IF;		 
		 

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PROD_INDICATIONS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_prod_indications
               (SELECT *
                  FROM scase_prod_indications);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PROD_INDICATIONS';

			 
            COMMIT;
         END IF;

------------------------------------------------------------

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PROD_DEVICES'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_prod_devices
               (SELECT *
                  FROM scase_prod_devices);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PROD_DEVICES';

			 
            COMMIT;
         END IF;

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_COMMENTS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_comments
               (SELECT *
                  FROM scase_comments);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_COMMENTS';

			 
            COMMIT;
         END IF;

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_COMPANY_CMTS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_company_cmts
               (SELECT *
                  FROM scase_company_cmts);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_COMPANY_CMTS';

			 
            COMMIT;
         END IF;
		 
-------------------------------------------------------		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_MEDWATCH_DATA'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_medwatch_data
               (SELECT *
                  FROM scase_medwatch_data);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_MEDWATCH_DATA';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT_DETAIL'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event_detail
               (SELECT *
                  FROM scase_event_detail);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT_DETAIL';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event_product_rptblty
                  (SELECT *
                  FROM scase_event_product_rptblty);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY';

			 
            COMMIT;
         END IF;		 
		 
		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EU_DEVICE'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_eu_device
               (SELECT *
                  FROM scase_eu_device);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EU_DEVICE';

			 
            COMMIT;
         END IF;		 

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_JUSTIFICATIONS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_justifications
               (SELECT *
                  FROM scase_justifications);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_JUSTIFICATIONS';

			 
            COMMIT;
         END IF;

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_VACC_VAERS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_vacc_vaers
               (SELECT *
                  FROM scase_vacc_vaers);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_VACC_VAERS';

			 
            COMMIT;
         END IF;


		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT_RECH_PROD'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event_rech_prod
               (SELECT *
                  FROM scase_event_rech_prod);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT_PROD_RECH';

			 
            COMMIT;
         END IF;

		 
	/*	 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_ACCESS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_access
               (SELECT *
                  FROM scase_access);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_ACCESS';

			 
            COMMIT;
         END IF;		*/ 

		 SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_E2BATTACHMENT_STATUS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_e2battachment_status
               (SELECT *
                  FROM scase_e2battachment_status);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_E2BATTACHMENT_STATUS';

			 
            COMMIT;
         END IF;

         SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CMN_REG_REPORTS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.cmn_reg_reports
               (SELECT *
                  FROM scmn_reg_reports);
				  
            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CMN_REG_REPORTS';

			 
            COMMIT;
         END IF;

         SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'RPT_ROUTING'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.rpt_routing
               (SELECT *
                  FROM srpt_routing);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'RPT_ROUTING';

            COMMIT;
         END IF;

         SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_REG_REPORTS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_reg_reports
               (SELECT *
                  FROM scase_reg_reports);


            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_REG_REPORTS';

            
            COMMIT;
         END IF;


         UPDATE dm_status ds
            SET staging_count =
                   (SELECT TO_NUMBER
                              (EXTRACTVALUE
                                  (XMLTYPE
                                      (DBMS_XMLGEN.getxml
                                                 (   'select count(*) c from '
                                                  || ut.table_name
                                                 )
                                      ),
                                   '/ROWSET/ROW/C'
                                  )
                              ) count11
                      FROM user_tables ut
                     WHERE ut.table_name = 'S' || ds.table_name)
          WHERE ds.status = 1;


         COMMIT;
      END IF;




            UPDATE argus_app.case_event
               SET ud_number_11 = null;
     
            UPDATE argus_app.case_product
              SET ud_number_11 = NULL;
     
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_push_argus_app',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'p_push_argus_app'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_push_argus_app , please see table LOGGER_LOGS for detials'
            );
   END p_push_argus_app; 
   
END pkg_main;
/