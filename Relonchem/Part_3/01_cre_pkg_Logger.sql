/* Formatted on 2017/01/04 14:21 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PACKAGE logger
AS
   PROCEDURE f_debug (
      vprocedure   IN   VARCHAR2,
      vpackage     IN   VARCHAR2,
      vmessage     IN   VARCHAR2,
      vmodule      IN   VARCHAR2
   );

   PROCEDURE f_error (
      vprocedure   IN   VARCHAR2,
      vpackage     IN   VARCHAR2,
      vmessage     IN   VARCHAR2,
      vmodule      IN   VARCHAR2
   );

   PROCEDURE f_pusherror (
      vtablename   IN   VARCHAR2,
      case_id      IN   NUMBER,
      vmessage     IN   VARCHAR2
   );
END logger;
/

CREATE OR REPLACE PACKAGE BODY logger
AS
   vouterrormessage   VARCHAR2 (4000);

   PROCEDURE f_debug (
      vprocedure   IN   VARCHAR2,
      vpackage     IN   VARCHAR2,
      vmessage     IN   VARCHAR2,
      vmodule      IN   VARCHAR2
   )
   IS
   BEGIN
      INSERT INTO logger_logs
           VALUES (logger_seq.NEXTVAL, vpackage || vprocedure, SYSDATE,
                   'Debug:' || vmessage, vmodule, 'Debug', NULL);
   /*
     INSERT INTO logger_logs
           VALUES (logger_seq.NEXTVAL, vpackage || vprocedure, SYSDATE,
                   'Debug:' || vmessage, vmodule, 'Debug', pkg_util.g_enterprise_id);
             */
   EXCEPTION
      -- Catch any unexpected fatal exceptions or errors.
      WHEN OTHERS
      THEN
         vouterrormessage := 'Error-Ins Debug';
   END f_debug;

   PROCEDURE f_error (
      vprocedure   IN   VARCHAR2,
      vpackage     IN   VARCHAR2,
      vmessage     IN   VARCHAR2,
      vmodule      IN   VARCHAR2
   )
   IS
   BEGIN
      INSERT INTO logger_logs
           VALUES (logger_seq.NEXTVAL, vpackage || vprocedure, SYSDATE,
                   'Error:' || vmessage, vmodule, 'Error', NULL);
   /*
    INSERT INTO logger_logs
           VALUES (logger_seq.NEXTVAL, vpackage || vprocedure, SYSDATE,
                   'Error:' || vmessage, vmodule, 'Error', pkg_util.g_enterprise_id);
               */
   EXCEPTION
      -- Catch any unexpected fatal exceptions or errors.
      WHEN OTHERS
      THEN
         vouterrormessage := 'Error-Ins Error';
   END f_error;

   PROCEDURE f_pusherror (
      vtablename   IN   VARCHAR2,
      case_id      IN   NUMBER,
      vmessage     IN   VARCHAR2
   )
   IS
   BEGIN
      INSERT INTO logger_push
           VALUES (loggerseq.NEXTVAL, vtablename, SYSDATE, vmessage,
                   'INSERT ERROR', NULL);
   /*
    INSERT INTO logger_push
          VALUES (loggerseq.NEXTVAL, vtablename, SYSDATE,
                  vmessage, 'INSERT ERROR', pkg_util.g_enterprise_id);
              */
   EXCEPTION
      -- Catch any unexpected fatal exceptions or errors.
      WHEN OTHERS
      THEN
         vouterrormessage := 'Error-Ins End';
   END f_pusherror;
END logger;
/