prompt -----------------------------------;
prompt - Loading Code List Mappings     --;
prompt -----------------------------------;
prompt
accept argus_instance     char prompt 'Enter TNSNAMES entry to connect to Argus database : '
accept enterprise_abbrv   char prompt 'Enter the enterprise abbreviation : '

prompt
prompt Connecting to &argus_instance. as s_Argus_App
prompt 
connect s_Argus_App/manager#123@&argus_instance.

prompt Press Enter if connected successfully...;
pause  Else Close Window and Re-Execute with appropriate Username/Password....
prompt 

prompt 
prompt ################################################################################;
prompt # Validating the input parameters 
prompt ################################################################################;

set serveroutput on;
set verify off;
Declare cnt NUMBER;
enterprise_id NUMBER;
BEGIN
pkg_rls.set_context (
         'admin',
         0,
         'ARGUS_SAFETY',
         ''
      );
	  
      SELECT count(*)
        INTO cnt
        FROM cfg_enterprise
       WHERE TRIM (UPPER (enterprise_abbrv)) =
                                            TRIM (UPPER ('&enterprise_abbrv'));	
	 
       IF(cnt =0)
	then
		dbms_output.put_line('Enterprise Abbreviation : INVALID');
	
else
		dbms_output.put_line('Enterprise Abbreviation : VALID');

end if;	

end;
/


prompt Press Enter if all the parameters are valid;
pause  If any of the parameters is invalid, close the Window and Re-Execute with correct parameters....

DELETE FROM DM_MAPPING_RAW WHERE UPPER(TRIM(ENTERPRISE_ABBREV)) = UPPER(TRIM('&enterprise_abbrv'));


COMMIT;

EXIT;