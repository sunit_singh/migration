OPTIONS(skip=1)
load data 
infile 'CodeListMappings.csv'
BADFILE 'insert_s_load_code_list_mappings.bad'
DISCARDFILE 'insert_s_load_code_list_mappings.dsc'
APPEND into table dm_mapping_raw
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS
(CODE_LIST CHAR,
SOURCE_VALUE CHAR,
TARGET_VALUE CHAR,
PROD_CD CHAR,
ENTERPRISE_ABBREV CHAR)