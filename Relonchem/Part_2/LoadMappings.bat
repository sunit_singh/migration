echo off
REM #############################################################################################
REM ## File      :  MigrationExecution.bat .. Batch File                                      ##
REM ## Purpose   :  Migrate the data for client                                               ##
REM #############################################################################################

cls

sqlplus /nolog @.\CleanRawTables.sql

cls

echo *****************************************************************************
echo Please put "s_argus_app/<pwd>@<argus_db_name>" for Username if prompted	       
echo *****************************************************************************

sqlldr control=loadDMMapping.ctl log=DMMappingLog.LOG
