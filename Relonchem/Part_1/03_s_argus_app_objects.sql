
CREATE SEQUENCE loggerseq
   MINVALUE 1
   MAXVALUE 999999999999999999999999999
   START WITH 1
   INCREMENT BY 1
   CACHE 20;


CREATE TABLE LOGGER_PUSH
(
   ID           NUMBER (10),
   TABLE_NAME   VARCHAR2 (1000),
   TIME_STAMP   DATE,
   TEXT         VARCHAR2 (4000),
   ACTION       VARCHAR2 (100),
   ENTERPRISE_ID NUMBER
);

CREATE TABLE LOGGER_LOGS
(
   ID           NUMBER (10),
   ROUTINE      VARCHAR2 (1000),
   TIME_STAMP   DATE,
   MODULE       VARCHAR2 (4000),
   TEXT         VARCHAR2 (4000),
   ACTION       VARCHAR2 (100),
   ENTERPRISE_ID NUMBER
);

CREATE SEQUENCE logger_seq
   MINVALUE 1
   MAXVALUE 999999999999999999999999999
   START WITH 1
   INCREMENT BY 1
   CACHE 20;

CREATE TABLE S_ARGUS_APP.DM_WARNINGS
(
   SEQ_NUM        NUMBER,
   PACAKGE_NAME   VARCHAR2 (30),
   PROC_NAME      VARCHAR2 (30),
   MESSAGE        VARCHAR2 (4000),
   ENTERPRISE_ID  NUMBER
);


CREATE TABLE S_ARGUS_APP.DM_PRODUCT_MAPPING
(
s_prod_id  NUMBER,
s_prod_name   VARCHAR2(2000),
s_country_id   NUMBER,
t_prod_id  NUMBER,
t_prod_name   VARCHAR2(2000),
t_country_id   NUMBER,
t_license_id   NUMBER
);


CREATE TABLE S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT
(
s_product_id  NUMBER,
s_country_id   NUMBER,
s_license_id   NUMBER,
t_product_id  NUMBER,
t_country_id   NUMBER,
t_license_id   NUMBER
);

CREATE TABLE S_ARGUS_APP.DM_PRODUCT_MAPPING_EXCEPTION
       (
       DESCRIPTION   VARCHAR2 (500),
       MIG_DATE     DATE
       );
	   


CREATE TABLE S_ARGUS_APP.DM_STATUS
(
   TABLE_NAME        VARCHAR2 (200),
   STATUS            NUMBER,
   STAGING_COUNT     NUMBER,
   ARGUS_APP_COUNT   NUMBER,
   CONV_START_TIME   DATE,
   CONV_END_TIME     DATE,
   ENTERPRISE_ID 	 NUMBER
);


CREATE TABLE S_ARGUS_APP.DM_MAPPING_RAW
(
  CODE_LIST          VARCHAR2(100 CHAR),
  SOURCE_VALUE         VARCHAR2(1000 CHAR),
  TARGET_VALUE        VARCHAR2(1000 CHAR),
  PROD_CD            VARCHAR2(40 CHAR),
  ENTERPRISE_ABBREV  VARCHAR2(40 CHAR)
);

CREATE TABLE S_ARGUS_APP.DM_MAPPING
(
  TARGET_TABLE_NAME  VARCHAR2(30 CHAR),
  TARGET_COL_NAME    VARCHAR2(30 CHAR),
  TARGET_COL_VALUE   VARCHAR2(4000 CHAR),
  COL_TO_BE_FETCHED VARCHAR2(30 CHAR),
  TARGET_LM_CODE     NUMBER,
  SOURCE_COL_VALUE    VARCHAR2(4000 CHAR),
  SOURCE_LM_CODE	NUMBER,
  PROD_CD           VARCHAR2(500 CHAR),
  ENTERPRISE_ID		NUMBER
);

CREATE TABLE S_ARGUS_APP.DM_MAPPING_REFERENCE
(
  CODE_LIST          VARCHAR2(100 CHAR),
  TARGET_TABLE_NAME   VARCHAR2(30 CHAR),
  TARGET_COL_NAME     VARCHAR2(30 CHAR),
  COL_TO_BE_FETCHED  VARCHAR2(30 CHAR)
);


COMMIT;

insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_MASTER',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_EVENT',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_REFERENCE',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_LAB_DATA',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_DEATH',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PAT_HIST',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_REPORTERS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_ROUTING',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_STUDY',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_ASSESS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_FOLLOWUP',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_LITERATURE',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PREGNANCY',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_NARRATIVE',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PAT_INFO',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PAT_TESTS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_DEATH_DETAILS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_HOSP',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PARENT_INFO',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_COMMENTS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_COMPANY_CMTS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_MEDWATCH_DATA',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PRODUCT',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_DOSE_REGIMENS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PROD_INGREDIENT',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PROD_INDICATIONS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PROD_DRUGS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_PROD_DEVICES',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_EVENT_ASSESS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_EVENT_DETAIL',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_EVENT_PRODUCT_RPTBLTY',0, 1);
--insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_NOTES_ATTACH',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_EU_DEVICE',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_JUSTIFICATIONS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_VACC_VAERS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_EVENT_RECH_PROD',0, 1);
--insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_ACCESS',0, 1);
--insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_ACTIONS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_E2BATTACHMENT_STATUS',0, 1);
--insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_NOTES_E2B_ATTACH',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CMN_REG_REPORTS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('CASE_REG_REPORTS',0, 1);
insert into S_ARGUS_APP.DM_STATUS (TABLE_NAME,STATUS, ENTERPRISE_ID) values ('RPT_ROUTING',0, 1);

insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Accidental Exposure','LM_ACCIDENTAL_EXPOSURE','DESCRIPTION','ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Action Taken','LM_ACTION_TAKEN','ACTION_TAKEN','ACT_TAKEN_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Action Type','LM_ACTION_ITEM_TYPE','ACTION_TYPE','ACTION_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Age Groups','LM_AGE_GROUPS','GROUP_NAME','AGE_GROUP_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Age Units','LM_AGE_UNITS','AGE_UNIT','AGE_UNIT_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Attachment Classification','LM_CLASSIFICATION','CLASSIFICATION','CLASSIFICATION_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Birth Type','LM_BIRTH_TYPE','BIRTH_TYPE','BIRTH_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Case Classification','LM_CASE_CLASSIFICATION','DESCRIPTION','CLASSIFICATION_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Causality Category','LM_CAUSALITY','CAUSALITY','CAUSALITY_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Causality Source','LM_CAUSALITY_SOURCE','SOURCE','SOURCE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Clinical Reference Type','LM_CLINICAL_REF_TYPES','REF_TYPE_DESC','REF_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Condition Type','LM_CONDITION_TYPE','COND_TYPE','CONDITION_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Contact type','LM_CONTACT_TYPE','CONTACT_TYPE','CONTACT_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Countries','LM_COUNTRIES','A3','COUNTRY_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Date Ranges','LM_DATE_RANGES','RANGE_NAME','RANGE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Delivery Types','LM_DELIVERY_TYPES','DELIVERY_TYPE','DELIVERY_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Device Preliminary Comments','LM_DEVICE_PRE_COMMENTS','PRE_COMMENTS','PRE_COMMENTS_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Device Subcomponents','LM_DEVICE_SUBCOMPONENTS','SUBCOMPONENT_NAME','SUBCOMPONENT_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Device Type','LM_DEVICE_TYPE','DEVICE_TYPE_DESC','DEVICE_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Dosage Frequency','LM_DOSE_FREQUENCY','FREQ','FREQ_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Dosage Units','LM_DOSE_UNITS','UNIT','UNIT_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Ethnicity','LM_ETHNICITY','ETHNICITY','ETHNICITY_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Evaluation Reason','LM_MFR_EVAL_REASON','MFR_EVAL_REASON','MFR_EVAL_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Event Frequency','LM_EVT_FREQUENCY','EVT_FREQ','EVT_FREQ_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Event Intensity','LM_EVT_INTENSITY','EVT_INTENSITY','EVT_INTENSITY_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Event Outcome','LM_EVT_OUTCOME','EVT_OUTCOME','EVT_OUTCOME_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Fetal Outcome','LM_FETAL_OUTCOME','FETAL_OUTCOME','FETAL_OUTCOME_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Formulation','LM_FORMULATION','FORMULATION','FORMULATION_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Gender','LM_GENDER','GENDER','GENDER_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Intermediary','LM_INTERMEDIARY','INTERMEDIARY','INTERMEDIARY_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Lab Result Assessment Terms','LM_LAB_ASSESSMENT','ASSESSMENT','ASSESSMENT_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Lab Test Group','LM_LAB_TEST_GROUP','LAB_TEST_GROUP','LAB_TEST_GROUP_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Local Evaluator Comment Type','LM_EVALUATOR_TYPE','EVALUATOR_TYPE','EVALUATOR_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Manufacturers','LM_MANUFACTURER','MANU_NAME','MANUFACTURER_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Medical Status','LM_MEDICAL_STATUS','LABEL','MED_STATUS_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Nature of Event','LM_EVT_NATURE','EVT_NATURE','EVT_NATURE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Occupations','LM_OCCUPATIONS','OCCUPATION','OCCUPATION_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Reference Type','LM_REF_TYPES','TYPE_DESC','REF_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Report Form','LM_REPORT_FORMS','FORM_DESC','REPORT_FORM_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Report Media','LM_REPORT_MEDIA','MEDIA','MEDIA_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Report Type','LM_REPORT_TYPE','REPORT_TYPE','RPT_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Reporter Type','LM_REPORTER_TYPE','REPORTER_TYPE','RPTR_TYPE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Routes of Administration','LM_ADMIN_ROUTE','ROUTE','ADMIN_ROUTE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Study Development Phase','LM_DEV_PHASE','DEV_PHASE','DEV_PHASE_ID');
insert into S_ARGUS_APP.DM_MAPPING_REFERENCE VALUES ('Reporting Destination','LM_REGULATORY_CONTACT','AGENCY_NAME','AGENCY_ID');


COMMIT;

insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (100999,'Lipsore 5% w/w cream',222,100001,'Lipsore 5% w/w cream',222,100002);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101000,'Aciclovir',222,100120,'Aciclovir',222,100159);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101000,'Aciclovir',100001,100120,'Aciclovir',100000,100160);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101000,'Lipsore 5% w/w cream',222,100003,'Lipsore 5% w/w cream',222,100002);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101001,'Alendronic Acid',222,100123,'Alendronic Acid',222,100163);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101002,'Alendronic Acid',222,100128,'Alendronic Acid',222,100163);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101002,'Alendronic Acid',100001,100128,'Alendronic Acid',100000,100170);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101003,'Allopurinol',222,100134,'Allopurinol',222,100177);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101004,'Allopurinol',222,100136,'Allopurinol',222,100177);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101004,'Allopurinol',100001,100136,'Allopurinol',100000,100178);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101006,'Amiloride Hydrochlorothiazide',222,100139,'Amiloride Hydrochlorothiazide',222,100181);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101006,'Amiloride Hydrochlorothiazide',100001,100140,'Amiloride Hydrochloride/Hydrochlorothiazide',100000,100016);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101006,'Co-Amilozide',222,100140,'Co-Amilozide',222,100183);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101007,'Amitriptyline',222,100141,'Amitriptyline',222,100184);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101008,'Amitriptyline',222,100142,'Amitriptyline',222,100185);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101009,'Amitriptyline',222,100143,'Amitriptyline',222,100186);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101009,'Amitriptyline',100001,100143,'Amitriptyline hydrochloride',100000,100156);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101010,'Aspirin',222,100181,'Aspirin',222,100224);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101010,'Aspirin',100001,100181,'Aspirin',100000,100226);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101011,'Aspirin',222,100183,'Aspirin',222,100224);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101011,'Aspirin',100001,100183,'Aspirin',100000,100226);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101013,'Azathioprine',222,100144,'Azathioprine',222,100187);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101014,'Azathioprine',222,100145,'Azathioprine',222,100187);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101014,'Azathioprine',100001,100145,'Azathioprine',100000,100188);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101017,'Bicalutamide',222,100146,'Bicalutamide',222,100189);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101017,'Bicalutamide',100001,100146,'Bicalutamide',100000,100034);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101019,'Cetirizine',222,100184,'Cetirizine',222,100227);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101019,'Cetirizine',100001,100184,'Cetirizine',100000,100228);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101020,'Bell''s Healthcare Allergy Relief',222,100046,'Bell''s Healthcare Allergy Relief 10 mg film coated tablets',222,100048);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101020,'Cetirizine',222,100185,'Cetirizine',222,100227);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101020,'Cetirizine',100001,100185,'Cetirizine',100000,100228);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101021,'Diazepam',222,100147,'Diazepam',222,100190);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101022,'Diazepam',222,100148,'Diazepam',222,100190);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101022,'Diazepam',100001,100148,'Diazepam',100000,100191);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101023,'Finasteride',222,100154,'Finasteride',222,100197);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101025,'Finasteride',222,100157,'Finasteride',222,100197);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101025,'Finasteride',100001,100157,'Finasteride',100000,100200);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101028,'Fluconazole',222,100162,'Fluconazole',222,100205);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101028,'Fluconazole',100001,100162,'Fluconazole',100000,100069);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101029,'Fluoxetine',222,100158,'Fluoxetine',222,100201);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101031,'Fluoxetine',222,100159,'Fluoxetine',222,100201);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101031,'Fluoxetine',100001,100159,'Fluoxetine',100000,100202);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101032,'Folic Acid',222,100167,'Folic Acid',222,100210);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101033,'Folic Acid',222,100170,'Folic Acid',222,100210);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101033,'Folic Acid',100001,100170,'Folic Acid',100000,100214);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101034,'Furosemide',222,100173,'Furosemide',222,100216);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101035,'Furosemide',222,100174,'Furosemide',222,100216);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101035,'Furosemide',100001,100174,'Furosemide',100000,100217);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101036,'Gliclazide',222,100180,'Gliclazide',222,100223);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101037,'Gliclazide',222,100182,'Gliclazide',222,100223);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101037,'Gliclazide',100001,100182,'Gliclazide',100000,100225);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101039,'Ibuprofen',222,100188,'Ibuprofen',222,100231);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101039,'Ibuprofen',100001,100188,'Ibuprofen',100000,100232);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101040,'Ibuprofen',222,100190,'Ibuprofen',222,100231);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101040,'Ibuprofen',100001,100190,'Ibuprofen',100000,100232);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101043,'Imipramine',222,100187,'Imipramine',222,100230);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101043,'Imipramine',100001,100187,'Imipramine Hydrochloride',100000,100109);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101045,'Isosorbide',222,100186,'Isosorbide',222,100229);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101046,'Lisinopril',222,100118,'Lisinopril',222,100157);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101047,'Lisinopril',222,100119,'Lisinopril',222,100157);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101047,'Lisinopril',100001,100119,'Lisinopril',100000,100158);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101048,'Bell''s Healthcare Hayfever and Allergy',222,100094,'Bell''s Healthcare Hayfever and Allergy 10mg tablets',222,100132);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101048,'Loratadine',222,100121,'Loratadine',222,100161);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101048,'Tesco one a day Hayfever and Allergy',222,100094,'Tesco one a day Hayfever and Allergy 10mg tablets',222,100135);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101049,'Loratadine',222,100122,'Loratadine',222,100161);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101049,'Loratadine',100001,100122,'Loratadine',100000,100162);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101052,'Losartan Hydrochlorothiazide',222,100114,'Losartan Hydrochlorothiazide',222,100164);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101052,'Losartan Hydrochlorothiazide',100001,100114,'Losartan /Hydrochlorothiazide',100000,100153);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101054,'Metformin',222,100124,'Metformin',222,100165);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101055,'Metformin',222,100125,'Metformin',222,100165);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101055,'Metformin',100001,100125,'Metformin',100000,100166);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101057,'Methyldopa',222,100126,'Methyldopa',222,100168);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101057,'Methyldopa',100001,100126,'Methyldopa',100000,100147);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101059,'Metoclopramide',222,100127,'Metoclopramide Hydrochloride',222,100169);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101059,'Metoclopramide',100001,100127,'Metoclopramide',100000,100144);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101060,'Mirtazapine',222,100129,'Mirtazapine',222,100171);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101062,'Mirtazapine',222,100130,'Mirtazapine',222,100172);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101063,'Mirtazapine',222,100131,'Mirtazapine',222,100171);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101063,'Mirtazapine',100001,100131,'Mirtazapine',100000,100173);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101065,'Oxytetracycline',222,100132,'Oxytetracycline',222,100174);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101068,'Paracetamol',222,100133,'Paracetamol',222,100175);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101069,'Paracetamol',222,100135,'Paracetamol',222,100175);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101069,'Paracetamol',100001,100135,'Paracetamol',100000,100176);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101071,'Paroxetine',222,100137,'Paroxetine',222,100179);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101072,'Paroxetine',222,100138,'Paroxetine',222,100179);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101072,'Paroxetine',100001,100138,'Paroxetine',100000,100180);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101073,'Phenobarbital',222,100149,'Phenobarbital',222,100192);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101074,'Phenobarbital',222,100150,'Phenobarbital',222,100192);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101074,'Phenobarbital',100001,100150,'Phenobarbital',100000,100193);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101076,'Piroxicam',222,100151,'Piroxicam',222,100194);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101077,'Prednisolone',222,100152,'Prednisolone',222,100195);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101078,'Prednisolone',222,100153,'Prednisolone',222,100195);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101078,'Prednisolone',100001,100153,'Prednisolone',100000,100196);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101080,'Propranolol',222,100155,'Propranolol',222,100198);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101081,'Propranolol',222,100156,'Propranolol',222,100198);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101081,'Propranolol',100001,100156,'Propranolol',100000,100199);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101083,'Ranitidine',222,100160,'Ranitidine',222,100203);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101084,'Ranitidine',222,100161,'Ranitidine',222,100203);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101084,'Ranitidine',100001,100161,'Ranitidine',100000,100204);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101085,'Risperidone',222,100163,'Risperidone',222,100206);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101086,'Risperidone',222,100164,'Risperidone',222,100207);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101087,'Risperidone',222,100165,'Risperidone',222,100207);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101087,'Risperidone',100001,100165,'Risperidone',100000,100208);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101089,'Simvastatin',222,100166,'Simvastatin',222,100209);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101090,'Simvastatin',222,100168,'Simvastatin',222,100209);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101090,'Simvastatin',100001,100168,'Simvastatin',100000,100211);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101093,'Sodium Bicarbonate',100001,100026,'Sodium Bicarbonate',100000,100036);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101094,'Spironolactone',222,100169,'Spironolactone',222,100212);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101095,'Spironolactone',222,100171,'Spironolactone',222,100212);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101095,'Spironolactone',100001,100171,'Spironolactone',100000,100213);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101098,'Sumatriptan',222,100172,'Sumatriptan',222,100215);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101098,'Sumatriptan',100001,100172,'Sumatriptan',100000,100026);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101100,'Tamoxifen',222,100175,'Tamoxifen',222,100218);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101100,'Tamoxifen',100001,100175,'Tamoxifen Citrate',100000,100021);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101101,'Terbinafine',222,100176,'Terbinafine',222,100219);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101102,'Terbinafine',222,100177,'Terbinafine',222,100219);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101102,'Terbinafine',100001,100177,'Terbinafine',100000,100220);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101104,'Tramadol',222,100178,'Tramadol',222,100221);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101105,'Tramadol',222,100179,'Tramadol',222,100221);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING(s_prod_id, s_prod_name, s_country_id, t_prod_id, t_prod_name, t_country_id, t_license_id) values (101105,'Tramadol',100001,100179,'Tramadol',100000,100222);


COMMIT;

insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101100,100001,101003,100175,100001,100021);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101048,100001,100940,100121,100001,100162);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101087,100001,100988,100165,100001,100208);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101011,100001,100886,100183,100001,100226);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101105,100001,101010,100179,100001,100222);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101010,100001,100886,100181,100001,100226);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101025,100001,100903,100157,100001,100200);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101054,100001,100947,100124,100001,100166);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101069,100001,100962,100135,100001,100176);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101002,240,100872,100128,100001,100170);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101063,222,100954,100131,222,100171);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101036,222,100916,100180,222,100225);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101013,100001,100890,100144,100001,100188);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101002,222,100871,100128,222,100163);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101100,222,101001,100175,222,100218);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101083,100001,100985,100160,100001,100204);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101031,100001,100909,100159,100001,100202);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101055,100001,100947,100125,100001,100166);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101002,100001,100873,100128,100001,100170);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101006,100001,100880,100140,100001,100016);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101022,100001,100900,100148,100001,100191);
insert into S_ARGUS_APP.DM_PRODUCT_MAPPING_REPORT(s_product_id, s_country_id, s_license_id, t_product_id, t_country_id, t_license_id) values(101063,100001,100956,100131,100001,100173);

COMMIT;
