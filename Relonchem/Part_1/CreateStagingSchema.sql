prompt -----------------------------------;
prompt - Migration Script Execution     --;
prompt -----------------------------------;
prompt
accept instance           		  char prompt 'Enter TNSNAMES entry to connect to Target ARGUS database : '
accept sys_pwd            		  char prompt 'Enter system password for Target ARGUS database          : ' hide
accept argus_app_pass     		  char prompt 'Enter Argus_App password                       : ' hide
accept dest_datafiles     		  char prompt 'Enter the file directory where Target datatabase files are : '
accept source_argus_instance      char prompt 'Enter TNSNAMES entry to connect to Source ARGUS database  		: '
accept source_argus_owner	      char prompt 'Enter Source ARGUS Schema Owner Name                      		: ' 
accept log_file                   char prompt 'Enter the log file name : '



prompt
prompt Connecting to &instance. as Argus_App
prompt 


connect Argus_App/&argus_app_pass.@&instance.

prompt Press Enter if connected successfully...;
pause  Else Close Window and Re-Execute with appropriate Username/Password....
prompt 

prompt
prompt Connecting to &instance. as system
prompt 
connect system/&sys_pwd.@&instance.
prompt Press Enter if connected successfully...;
pause  Else Close Window and Re-Execute with appropriate Username/Password....
prompt 

SPOOL .\&log_file..LOG


prompt -----------------------------------------------------------;
prompt - Creating tablespaces                               ------;
prompt -----------------------------------------------------------;
prompt
prompt 

CREATE TEMPORARY TABLESPACE T_201 TEMPFILE 
  '&dest_datafiles./T_2014.dbf' SIZE 1024M AUTOEXTEND ON NEXT 640K MAXSIZE UNLIMITED,
  '&dest_datafiles./T_2015.dbf' SIZE 1024M AUTOEXTEND OFF
TABLESPACE GROUP ''
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;

CREATE TABLESPACE S_201 DATAFILE 
    '&dest_datafiles./S_2016.dbf' SIZE 4096M AUTOEXTEND ON NEXT 640K MAXSIZE UNLIMITED
LOGGING
ONLINE
PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO
FLASHBACK ON;


prompt Please Check whether the tablespaces are created .. Enter if created successfully...;
pause  Else Close Window and Re-Execute with path for the datafiles

prompt -----------------------------------------------------------;
prompt - Creating staging user S_ARGUS_APP                  ------;
prompt -----------------------------------------------------------;
prompt
prompt 

@.\01_Eniv_Setup_system.sql

prompt -----------------------------------;
prompt - Create Stage Objects           --;
prompt -----------------------------------;
prompt
prompt 
prompt Connecting as S_ARGUS_APP
prompt 
connect S_ARGUS_APP/manager#123@&instance.
prompt 
prompt 

@.\02_create_argus_app_objects.sql
@.\03_s_argus_app_objects.sql


SPOOL OFF

prompt ################################################################################;
prompt # Verify .\&log_file..LOG For errors, if any....;
prompt ################################################################################;

Pause  Press Enter to continue......

EXIT;
