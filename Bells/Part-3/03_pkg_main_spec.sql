/* Formatted on 2017/01/04 14:15 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PACKAGE pkg_main
AS
   /*   Description:Procedures to migrate tables from Source to Target   */
PROCEDURE p_migrate_case_master;
PROCEDURE p_migrate_case_death;
PROCEDURE p_migrate_case_reporters;
PROCEDURE p_migrate_case_routing;
PROCEDURE p_migrate_case_study;
PROCEDURE p_migrate_case_assess;
PROCEDURE p_migrate_case_literature;
PROCEDURE p_migrate_case_pregnancy;
PROCEDURE p_migrate_case_narrative;
PROCEDURE p_migrate_case_pat_info;
PROCEDURE p_migrate_case_classification;
PROCEDURE p_migrate_case_contact_log;
PROCEDURE p_migrate_case_medwatch_data;
PROCEDURE p_migrate_case_event;
PROCEDURE p_migrate_case_product;
PROCEDURE p_migrate_case_dose_regimens;
PROCEDURE p_migrate_case_prod_ingredient;
PROCEDURE p_migrate_case_prod_drugs;
PROCEDURE p_migrate_case_event_assess;
PROCEDURE p_migrate_case_event_detail;
PROCEDURE p_migrate_case_ep_rptblty;
PROCEDURE p_migrate_case_access;
PROCEDURE p_migrate_case_justifications;
PROCEDURE p_migrate_case_reference;


   PROCEDURE p_main_migrate (
      pi_enterprise_abbrv   IN   VARCHAR2,
      pi_prod_cd            IN   VARCHAR2,
      pi_migration_user     IN   VARCHAR2,
      pi_migration_site     IN   VARCHAR2,
      pi_database           IN   VARCHAR2 := 'NOR'
   );

   PROCEDURE p_push_argus_app;

END pkg_main;
/
