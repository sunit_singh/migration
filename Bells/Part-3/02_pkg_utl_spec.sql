CREATE OR REPLACE PACKAGE pkg_util
AS
-- GLOBAL VARIABLES
--------------------------------CONSTANTS---------------------------------------------
g_migration_user            NUMBER;
   g_migration_user_fullname   VARCHAR2 (100);
   g_migration_date            DATE;
   g_migration_site_id         NUMBER;
   g_argus_meddra_dict_id      NUMBER;
   g_argus_who_dict_id         NUMBER;
   g_enterprise_id             NUMBER;
   g_enterprise_abbrev         VARCHAR2 (20);
   g_prod_cd                   VARCHAR2(100);

   TYPE gt_meddra_terms IS RECORD (
      lltcode      NUMBER         := NULL,
      lltname      VARCHAR2 (250) := NULL,
      ptcode       NUMBER         := NULL,
      ptname       VARCHAR2 (250) := NULL,
      soccode      NUMBER         := NULL,
      socname      VARCHAR2 (250) := NULL,
      hltcode      NUMBER         := NULL,
      hltname      VARCHAR2 (250) := NULL,
      hlgtcode     NUMBER         := NULL,
      hlgtname     VARCHAR2 (250) := NULL,
      dictid       NUMBER         := NULL,
      codestatus   NUMBER         := NULL
   );


  PROCEDURE p_initialize (
      pi_enterprise_abbrv   VARCHAR2,
      pi_prod_cd            VARCHAR2,
      pi_migration_user     VARCHAR2,
      pi_migration_site     VARCHAR2
   );

    FUNCTION f_fetch_sequence (
      pi_table_name      VARCHAR2,
      pi_display_order   NUMBER := NULL
  )RETURN NUMBER;


   PROCEDURE p_insert_dm_mapping;

   PROCEDURE p_update_dm_mapping;

   PROCEDURE p_get_current_meddra_hierarchy (
      pi_source     IN       VARCHAR2,
      pi_llt_name   IN       VARCHAR2,
      po_terms      OUT      pkg_util.gt_meddra_terms
   );


   FUNCTION f_get_argus_code (
      pi_target_table_name   IN   VARCHAR2,
      pi_source_value     IN   VARCHAR2
   ) RETURN NUMBER;

    FUNCTION isfatal (pi_outcome VARCHAR2) RETURN NUMBER;

    FUNCTION isserious (piserious_nonserious VARCHAR2) RETURN NUMBER;

    FUNCTION f_get_meddra_term (
      pi_source     IN   VARCHAR2,
      pi_llt_term   IN   VARCHAR2,
      pi_in_term    IN   VARCHAR2
   )
      RETURN VARCHAR2;

    FUNCTION isdate (pidate VARCHAR2, piformat VARCHAR2)
      RETURN NUMBER;

    FUNCTION f_get_argus_value ( pi_target_table_name   VARCHAR2,
                                pi_source_value    VARCHAR2
                              )
      RETURN VARCHAR2 ;

END pkg_util;
/
