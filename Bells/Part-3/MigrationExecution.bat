echo off
REM #############################################################################################
REM ## File      :  MigrationExecution.bat .. Batch File                                      ##
REM ## Purpose   :  Migrate the data for client                                               ##
REM #############################################################################################
echo off
cls
sqlplus /nolog @.\MigrationExecution.sql