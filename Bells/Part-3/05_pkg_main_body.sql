CREATE OR REPLACE PACKAGE BODY S_ARGUS_APP.pkg_main
AS

   PROCEDURE p_migrate_case_master

/***********************************************************************
 Name:        p_migrate_case_master
 Description: This procedure migrates data to CASE_MASTER table.
 ***********************************************************************/

    AS

  l_sid                   VARCHAR2 (200);


   BEGIN

      SELECT dbname
        INTO l_sid
        FROM control_table
       WHERE ROWNUM <= 1;

      INSERT INTO scase_master
                  (case_id, case_num, rev, workflow_seq_num, last_workflow_seq_num, create_time, init_rept_date, user_id,
           last_update_time, last_update_user_id, requires_followup, owner_id, state_id, country_id,
                   site_id, rpt_type_id, last_state_id, sid,
          priority_assessment, close_user_id, close_date, date_locked,
          worklist_owner_id,
          lock_status_id, LAM_ASSESS_DONE,ud_text_1 ,ud_text_12, enterprise_id)
         (SELECT s_case_maste_case_id.NEXTVAL case_id,
                 concat('BHC_', scm.MAH_NUMBER) case_num,
         0 rev,
         100007 workflow_seq_num,
         null last_workflow_seq_num,
         sysdate create_time,
         scm.date_of_notification init_rept_date,
         pkg_util.g_migration_user,
         sysdate last_update_time,
         pkg_util.g_migration_user,
         0 requires_followup,
         pkg_util.g_migration_user,
         100005,
         pkg_util.f_get_argus_code('LM_COUNTRIES', scm.country_of_origin),
         100000,
         pkg_util.f_get_argus_code('LM_REPORT_TYPE',scm.report_source),
         100000 last_state_id,
         l_sid ,
         1,
         pkg_util.g_migration_user,
         pkg_util.g_migration_date,
         pkg_util.g_migration_date,
         pkg_util.g_migration_user,
         3,0,
         scm.expeditable_case_event,
         scm.MAH_NUMBER,
         pkg_util.g_enterprise_id
         FROM src_case_main scm
        );


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_MASTER'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;



   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_master',
                         'PKG_MAIN',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE MASTER INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_master , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_master;


    PROCEDURE p_migrate_case_event
   /***********************************************************************
    Name:        p_migrate_case_event
    Description: This procedure migrates data to CASE_EVENT table.
    ***********************************************************************/
   AS
   l_sort_id           NUMBER;
   l_onset_latency_seconds    NUMBER;
   BEGIN
      INSERT INTO scase_event
                  (case_id, seq_num, desc_reptd, desc_coded, diagnosis, onset, onset_res,
          stop_date_res, onset_latency, evt_outcome_id, rpt_serious, sc_death, art_code, pref_term, inc_term, body_sys,
          med_serious, seriousness, onset_date_partial, hlgt, hlt, hlt_code, hlgt_code, soc_code, inc_code, duration_unit_e2b,
          onset_latency_unit_e2b, onset_delay_unit_e2b, ud_number_12, ud_text_11, ud_text_12, enterprise_id)
    (SELECT cm.case_id case_id, s_case_event.NEXTVAL AS seq_num, sce.reaction_description desc_reptd,
        pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'lltname'
                                           ) AS desc_coded,--ADDED TO_DATE BELOW
        1 diagnosis, to_date(sce.event_date_time_to_onset,'dd-mm-yyyy') onset, decode(sce.event_date_time_to_onset, null, 0,8) onset_res,
        0 stop_date_res, sce.event_onset_latency|| ' '|| sce.event_onset_latency_unit onset_latency,
        pkg_util.f_get_argus_code('LM_EVT_OUTCOME',sce.outcome) evt_outcome_id, pkg_util.isserious(sce.serious_nonserious) rpt_serious,
        decode(nvl(pkg_util.isserious(sce.serious_nonserious),0),1,nvl(pkg_util.isfatal(sce.outcome),0),0,0) sc_death,
        pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'ptcode'
                                           ) AS art_code,
        pkg_util.f_get_meddra_term('Source: CASE_EVENT',
                                           sce.reaction_description,
                                           'ptname'
                                           ) AS pref_term,
        pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'lltname'
                                           ) AS inc_term,
        pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'socname'
                                           ) AS body_sys,
        decode(pkg_util.isserious(sce.serious_nonserious),1,nvl(nullif(pkg_util.isserious(sce.serious_nonserious),pkg_util.isfatal(sce.outcome)),0),0) med_serious,
        nvl(pkg_util.isserious(sce.serious_nonserious),0) seriousness, ---added nvl in this line
        to_date(sce.event_date_time_to_onset, 'DD-MM-YYYY')/*to_char(sce.event_date_time_to_onset, 'DD-MON-YYYY')*/ onset_date_partial,---added to_date in this line
                pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'hlgtname'
                                           ) AS hlgt,
                pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'hltname'
                                           ) AS hlt,
                pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'hltcode'
                                           ) AS hlt_code,
                pkg_util.f_get_meddra_term('Source: CASE_EVENT',
                                           sce.reaction_description,
                                           'hlgtcode'
                                          ) AS hlgt_code,
                pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                           sce.reaction_description,
                                            'soccode'
                                           ) AS soccode,
                pkg_util.f_get_meddra_term ('Source: CASE_EVENT',
                                            sce.reaction_description,
                                            'lltcode'
                                           ) AS inc_code,
        -1 duration_unit_e2b, decode(sce.event_date_time_to_onset, null, -1,804) onset_latency_unit_e2b, -1 onset_delay_unit_e2b,
        sce.sr_no,
        sce.reaction_description, TO_char(sce.mah_number), 1
        FROM src_case_event sce, scase_master cm
        WHERE sce.mah_number=cm.ud_text_12);

        commit;

      FOR rec IN (SELECT DISTINCT case_id FROM scase_event)
      LOOP
      l_sort_id:= 1;
      FOR rec1 IN (SELECT case_id, seq_num FROM scase_event WHERE case_id=rec.case_id)
        LOOP
          UPDATE scase_event SET sort_id=l_sort_id WHERE seq_num=rec1.seq_num AND case_id=rec1.case_id;
          l_sort_id:=l_sort_id + 1;
        END LOOP;
      END LOOP;

      FOR rec IN (SELECT ce.case_id, ce.seq_num, ce.onset_latency, sce.event_onset_latency, sce.event_onset_latency_unit
            FROM scase_event ce, scase_master cm, src_case_event sce
            WHERE ce.case_id=cm.case_id
            AND cm.ud_text_12=sce.mah_number
            AND CE.ud_number_12 = sce.sr_no
            AND ce.onset_latency IS NOT NULL)
      LOOP
          IF SUBSTR(UPPER(TRIM(rec.event_onset_latency_unit)),1,3)='SEC' THEN
            l_onset_latency_seconds:= rec.event_onset_latency*1;
          ELSIF SUBSTR(UPPER(TRIM(rec.event_onset_latency_unit)),1,3)='MIN' THEN
              l_onset_latency_seconds:= rec.event_onset_latency*60;
          ELSIF SUBSTR(UPPER(TRIM(rec.event_onset_latency_unit)),1,3)='HOU' THEN
              l_onset_latency_seconds:= rec.event_onset_latency*60*60;
          ELSIF SUBSTR(UPPER(TRIM(rec.event_onset_latency_unit)),1,3)='DAY' THEN
              l_onset_latency_seconds:= rec.event_onset_latency*60*60*24;
          ELSIF SUBSTR(UPPER(TRIM(rec.event_onset_latency_unit)),1,3)='WEE' THEN
              l_onset_latency_seconds:= rec.event_onset_latency*60*60*24*7;
          ELSIF SUBSTR(UPPER(TRIM(rec.event_onset_latency_unit)),1,3)='MON' THEN
              l_onset_latency_seconds:= rec.event_onset_latency*60*60*24*30;
          ELSIF SUBSTR(UPPER(TRIM(rec.event_onset_latency_unit)),1,3)='YEA' THEN
              l_onset_latency_seconds:= rec.event_onset_latency*60*60*24*365;
          ELSE
            l_onset_latency_seconds:= NULL;
          END IF;
          --added here  argus_app. because of insufficient priviliges error
        UPDATE /*argus_app.*/scase_event SET onset_latency_seconds = l_onset_latency_seconds WHERE case_id=rec.case_id and seq_num=rec.seq_num;

      END LOOP;


      UPDATE scase_event
         SET dict_id = pkg_util.g_argus_meddra_dict_id,
             code_status = 1
       WHERE inc_code IS NOT NULL AND enterprise_id = pkg_util.g_enterprise_id;

     UPDATE scase_event
         SET dict_id = pkg_util.g_argus_meddra_dict_id,
             code_status = 0
       WHERE inc_code IS NULL AND enterprise_id = pkg_util.g_enterprise_id;

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_event',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE event INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_event , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_event;




   PROCEDURE p_migrate_case_death
/***********************************************************************
 Name:        p_migrate_case_death
 Description: This procedure migrates data to CASE_DEATH table.
 ***********************************************************************/
   AS
    BEGIN
         INSERT INTO scase_death
          (case_id, autopsy, death_date_res, enterprise_id)
     (SELECT cm.case_id, 0, 0, pkg_util.g_enterprise_id FROM scase_master cm);

      UPDATE dm_status
       SET status = 1,
         conv_end_time = SYSDATE
       WHERE table_name = 'CASE_DEATH'
       AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_death',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE Death INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_death , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_death;


  PROCEDURE p_migrate_case_reporters
   /***********************************************************************
    Name:        p_migrate_case_reporters
    Description: This procedure migrates data to CASE_REPORTERS table.
    ***********************************************************************/
  AS
  l_hcp_flag       scase_reporters.hcp_flag%TYPE;
  l_reporter_type    scase_reporters.reporter_type%TYPE;
  l_occupation_id    scase_reporters.occupation_id%TYPE;
  l_last_name      scase_reporters.last_name%TYPE;
  l_intermediary_id  scase_reporters.intermediary_id%TYPE;

  BEGIN

    FOR rec IN (SELECT cm.case_id, scm.mah_number, scm.report_source, scm.country_of_origin, scm.reporter_name, scm.hcp_confirmed, scm.aspr_consumer,
          scm.source_of_literature FROM src_case_main scm, scase_master cm WHERE scm.mah_number=cm.ud_text_12)
      LOOP
        IF  UPPER(TRIM(rec.report_source))='ASPR' THEN
          IF upper(trim(rec.aspr_consumer))='TRUE' THEN
            l_hcp_flag:=0;
            SELECT rptr_type_id INTO l_reporter_type FROM lm_reporter_type WHERE upper(trim(reporter_type)) LIKE 'CONSUMER';
            l_last_name:= 'Unknown';
            SELECT occupation_id INTO l_occupation_id FROM lm_occupations WHERE upper(trim(occupation)) LIKE 'CONSUMER%';
            SELECT   intermediary_id INTO l_intermediary_id FROM lm_intermediary WHERE UPPER(TRIM(INTERMEDIARY)) LIKE 'REGULATORY%AUTHORITY%';
          ELSE
            l_hcp_flag:=1;
            SELECT rptr_type_id INTO l_reporter_type FROM lm_reporter_type WHERE upper(trim(reporter_type)) LIKE 'OTHER HEALTH PROFESSIONAL';
            l_last_name:= 'Unknown';
            SELECT occupation_id INTO l_occupation_id FROM lm_occupations WHERE upper(trim(occupation)) LIKE 'OTHER HEALTH PROFESSIONAL';
            SELECT   intermediary_id INTO l_intermediary_id FROM lm_intermediary WHERE UPPER(TRIM(INTERMEDIARY)) LIKE 'REGULATORY%AUTHORITY%';
          END IF;
        ELSE
          IF UPPER(TRIM(rec.hcp_confirmed))='YES' THEN
            l_hcp_flag:=1;
            SELECT rptr_type_id INTO l_reporter_type FROM lm_reporter_type WHERE upper(trim(reporter_type)) LIKE 'OTHER HEALTH PROFESSIONAL';
            SELECT occupation_id INTO l_occupation_id FROM lm_occupations WHERE upper(trim(occupation)) LIKE 'OTHER HEALTH PROFESSIONAL';
          ELSIF UPPER(TRIM(rec.hcp_confirmed))='NO' THEN
            l_hcp_flag:=0;
            SELECT rptr_type_id INTO l_reporter_type FROM lm_reporter_type WHERE upper(trim(reporter_type)) LIKE 'CONSUMER';
            SELECT occupation_id INTO l_occupation_id FROM lm_occupations WHERE upper(trim(occupation)) LIKE 'CONSUMER%';
          ELSE
            l_hcp_flag:=2;
            SELECT rptr_type_id INTO l_reporter_type FROM lm_reporter_type WHERE upper(trim(reporter_type)) LIKE 'OTHER';
            SELECT occupation_id INTO l_occupation_id FROM lm_occupations WHERE upper(trim(occupation)) LIKE 'OTHER';
          END IF;
          l_intermediary_id:= NULL;
          l_last_name:= rec.reporter_name;
        END IF;



        INSERT INTO scase_reporters(case_id, seq_num, occupation_id, hcp_flag, primary_contact, reporter_type, intermediary_id,
                  sort_id, country_id, last_name, notes, enterprise_id)
        VALUES(rec.case_id, s_case_reporters.nextval, l_occupation_id, l_hcp_flag, 1, l_reporter_type, l_intermediary_id, 1,
          pkg_util.f_get_argus_code('LM_COUNTRIES', rec.country_of_origin), l_last_name, rec.source_of_literature,
          pkg_util.g_enterprise_id);

    END LOOP;

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_REPORTERS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_reporters',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_REPORTERS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_reporters , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_reporters;


   PROCEDURE p_migrate_case_routing
   AS
   /***********************************************************************
     Name:        p_migrate_case_routing
     Description: This procedure migrates case data to CASE_ROUTING table.
     ***********************************************************************/
   BEGIN

      INSERT INTO scase_routing
                  (case_id, seq_num, route_date, user_id, sort_id,
                   comment_txt, to_state_id, fr_state_id, last_update_time,
                   report_scheduling, case_status, deleted, enterprise_id)
         (SELECT cm.case_id, pkg_util.f_fetch_sequence ('CASE_ROUTING'),
                gss_util.to_gmt (pkg_util.g_migration_date),
                pkg_util.g_migration_user, 1,
                   'Case Migrated from Sorce by MIGRATON_USER',
                100005, NULL, pkg_util.g_migration_date, 0, 3, cm.deleted,
                pkg_util.g_enterprise_id
           FROM scase_master cm);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_ROUTING'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_routing',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE Routing INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_routing , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_routing;


   PROCEDURE p_migrate_case_study
   /***********************************************************************
   Name:        p_migrate_case_study
   Description: This procedure migrates case data to CASE_STUDY table.
   ***********************************************************************/
   AS
   BEGIN
      INSERT INTO scase_study
          (case_id, product_count, enterprise_id)
    (SELECT cm.case_id, 0, pkg_util.g_enterprise_id FROM scase_master cm);


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_STUDY'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_study',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_STUDY INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_study , please see table LOGGER_LOGS for details'
            );
   END p_migrate_case_study;


    PROCEDURE p_migrate_case_literature
/***********************************************************************
Name:        p_migrate_case_literature
Description: This procedure migrates case data to CASE_LITERATURE table.
***********************************************************************/
   AS

   BEGIN

-- commented the below insert statement, need to review it later
    /*INSERT INTO scase_literature(case_id, seq_num, sort_id, journal, author, pgs, enterprise_id)
                (SELECT cm.case_id, s_case_literature.nextval, 1,
              scm.journal, scm.author, scm.page_number, pkg_util.g_enterprise_id
              FROM src_case_main cm, c WHERE cm.src_case_id = pcl.case_id);*/


 -- ADDED BELOW INSERT STATEMENT
     INSERT INTO scase_literature
       (case_id, seq_num, sort_id, journal, author, pgs, enterprise_id)
       (SELECT (SELECT cm.case_id
                  FROM scase_master cm
                 WHERE cm.ud_text_12 = scm.mah_number),
               pkg_util.f_fetch_sequence('CASE_LITERATURE') /*s_case_literature.nextval*/,
               1,
               SUBSTR(TRIM(scm.journal),1,80),
               scm.author_name,
               scm.page_number,
               1
          FROM SRC_CASE_MAIN scm
         ); --- need to add the case numbers


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_LITERATURE'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_literature',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_LITERATURE'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_literature , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_literature;

 PROCEDURE p_migrate_case_product
   /***********************************************************************
   Name:        p_migrate_case_product
   Description: This procedure migrates case data to CASE_PRODUCT table.
   ***********************************************************************/
   AS

  v_count                      NUMBER;
  l_seq_num                    NUMBER;
  l_prod_id                    NUMBER;
  l_prod_name                  VARCHAR2(200);
  l_manufacturer_id            NUMBER;
  l_country_id                 NUMBER;
  l_license_id                 NUMBER;
  l_family_id                  NUMBER;
  l_generic_name               VARCHAR2(1000);
  l_primary_event              NUMBER;

  CURSOR l_cur_prod IS
      SELECT DISTINCT case_id, mah_number,
          license_number_1, product_name_1,
          license_number_2, product_name_2,
          license_number_3, product_name_3,
          license_number_4, product_name_4,
          license_number_5, product_name_5,
          license_number_6, product_name_6,
          license_number_7, product_name_7
        FROM src_case_event sce, scase_master cm WHERE cm.ud_text_12=sce.mah_number;


  BEGIN
  FOR rec IN l_cur_prod
    LOOP


    SELECT COUNT(*) INTO v_count FROM  scase_event ce
      WHERE ce.case_id=rec.case_id AND ce.sort_id=1;

    IF v_count = 1 THEN
      SELECT ce.seq_num INTO l_primary_event FROM scase_event ce
        WHERE ce.case_id=rec.case_id AND ce.sort_id=1;
    ELSE
        l_primary_event:= NULL;
    END IF;


    --------------- Product-1--------------------------

    IF rec.license_number_1 IS NOT NULL AND rec.product_name_1 IS NOT NULL THEN

      l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');

      SELECT COUNT(*) INTO v_count
        FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_1
        AND s_prod_name = rec.product_name_1;

      IF  v_count >0 THEN

        SELECT t_prod_id, t_country_id, t_license_id INTO
           l_prod_id, l_country_id, l_license_id
          FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_1
        AND s_prod_name = rec.product_name_1;

        SELECT trade_name INTO l_prod_name FROM lm_license WHERE deleted IS NULL AND license_id= l_license_id;

        SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
            WHERE product_id = l_prod_id;

        l_manufacturer_id:= 100000;

      ELSE

        l_prod_id:= NULL;
        l_prod_name:= rec.product_name_1 ;
        SELECT country_id INTO l_country_id FROM lm_countries WHERE country like 'UNK%';
        l_license_id:= NULL;
        l_generic_name:= to_char(substr(rec.product_name_1, 1, 1000));
        l_manufacturer_id:= NULL;

      END IF;

      INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, manufacturer_id, first_sus_prod, selected_view, sort_id,
                views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status,
                STUDY_PRODUCT_NUM, primary_event, family_id, prod_lic_id,
                LAST_UPDATE_TIME, ud_text_10, ud_text_11, ud_text_12, enterprise_id)
          VALUES (rec.case_id, l_seq_num, l_prod_id, 1, 0, l_manufacturer_id, 1, 1, 1, 1, l_country_id, l_prod_name, l_generic_name,
              rec.product_name_1,l_prod_name, 1, 0, l_primary_event, l_family_id,
               l_license_id, pkg_util.g_migration_date, rec.license_number_1, rec.product_name_1, rec.mah_number, pkg_util.g_enterprise_id );

    END IF;

    --------------- Product-2--------------------------

    l_seq_num           :=NULL;
    l_prod_id        :=NULL;
    l_prod_name        :=NULL;
    l_manufacturer_id    :=NULL;
    l_country_id      :=NULL;
    l_license_id      :=NULL;
    l_family_id        :=NULL;
    l_generic_name      :=NULL;

    IF rec.license_number_2 IS NOT NULL AND rec.product_name_2 IS NOT NULL THEN

      l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');

      SELECT COUNT(*) INTO v_count
        FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_2
        AND s_prod_name = rec.product_name_2;

      IF  v_count >0 THEN

        SELECT t_prod_id, t_country_id, t_license_id INTO
           l_prod_id, l_country_id, l_license_id
          FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_2
        AND s_prod_name = rec.product_name_2;

        SELECT trade_name INTO l_prod_name FROM lm_license WHERE deleted IS NULL AND license_id= l_license_id;

        SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
            WHERE product_id = l_prod_id;

        l_manufacturer_id:= 100000;

      ELSE

        l_prod_id:= NULL;
        l_prod_name:= rec.product_name_2 ;
        SELECT country_id INTO l_country_id FROM lm_countries WHERE country like 'UNK%';
        l_license_id:= NULL;
        l_generic_name:= to_char(substr(rec.product_name_2, 1, 1000));
        l_manufacturer_id:= NULL;

      END IF;

      INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, manufacturer_id, first_sus_prod, selected_view, sort_id,
                views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status,
                STUDY_PRODUCT_NUM, primary_event, family_id, prod_lic_id,
                LAST_UPDATE_TIME, ud_text_10, ud_text_11, ud_text_12, enterprise_id)
          VALUES (rec.case_id, l_seq_num, l_prod_id, 1, 0, l_manufacturer_id, 0, 1, 2, 1, l_country_id, l_prod_name, l_generic_name,
              rec.product_name_2,l_prod_name, 1, 0, l_primary_event, l_family_id,
               l_license_id, pkg_util.g_migration_date, rec.license_number_2, rec.product_name_2,  rec.mah_number, pkg_util.g_enterprise_id );

    END IF;


    --------------- Product-3--------------------------

    l_seq_num           :=NULL;
    l_prod_id        :=NULL;
    l_prod_name        :=NULL;
    l_manufacturer_id    :=NULL;
    l_country_id      :=NULL;
    l_license_id      :=NULL;
    l_family_id        :=NULL;
    l_generic_name      :=NULL;

    IF rec.license_number_3 IS NOT NULL AND rec.product_name_3 IS NOT NULL THEN

      l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');

      SELECT COUNT(*) INTO v_count
        FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_3
        AND s_prod_name = rec.product_name_3;

      IF  v_count >0 THEN

        SELECT t_prod_id, t_country_id, t_license_id INTO
           l_prod_id, l_country_id, l_license_id
          FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_3
        AND s_prod_name = rec.product_name_3;

        SELECT trade_name INTO l_prod_name FROM lm_license WHERE deleted IS NULL AND license_id= l_license_id;

        SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
            WHERE product_id = l_prod_id;

        l_manufacturer_id:= 100000;

      ELSE

        l_prod_id:= NULL;
        l_prod_name:= rec.product_name_3 ;
        SELECT country_id INTO l_country_id FROM lm_countries WHERE country like 'UNK%';
        l_license_id:= NULL;
        l_generic_name:= to_char(substr(rec.product_name_3, 1, 1000));
        l_manufacturer_id:= NULL;

      END IF;

      INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, manufacturer_id, first_sus_prod, selected_view, sort_id,
                views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status,
                STUDY_PRODUCT_NUM, primary_event, family_id, prod_lic_id,
                LAST_UPDATE_TIME, ud_text_10, ud_text_11, ud_text_12, enterprise_id)
          VALUES (rec.case_id, l_seq_num, l_prod_id, 1, 0, l_manufacturer_id, 0, 1, 3, 1, l_country_id, l_prod_name, l_generic_name,
              rec.product_name_3,l_prod_name, 1, 0, l_primary_event, l_family_id,
               l_license_id, pkg_util.g_migration_date, rec.license_number_3, rec.product_name_3,  rec.mah_number, pkg_util.g_enterprise_id );

    END IF;


    --------------- Product-4--------------------------

    l_seq_num           :=NULL;
    l_prod_id        :=NULL;
    l_prod_name        :=NULL;
    l_manufacturer_id    :=NULL;
    l_country_id      :=NULL;
    l_license_id      :=NULL;
    l_family_id        :=NULL;
    l_generic_name      :=NULL;

    IF rec.license_number_4 IS NOT NULL AND rec.product_name_4 IS NOT NULL THEN

      l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');

      SELECT COUNT(*) INTO v_count FROM  scase_event ce
          WHERE ce.case_id=rec.case_id AND ce.sort_id=1;

      IF v_count = 1 THEN

        SELECT ce.seq_num INTO l_primary_event FROM scase_event ce
        WHERE ce.case_id=rec.case_id AND ce.sort_id=1;
      ELSE
        l_primary_event:= NULL;
      END IF;


      SELECT COUNT(*) INTO v_count
        FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_4
        AND s_prod_name = rec.product_name_4;

      IF  v_count >0 THEN

        SELECT t_prod_id, t_country_id, t_license_id INTO
           l_prod_id, l_country_id, l_license_id
          FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_4
        AND s_prod_name = rec.product_name_4;

        SELECT trade_name INTO l_prod_name FROM lm_license WHERE deleted IS NULL AND license_id= l_license_id;

        SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
            WHERE product_id = l_prod_id;

        l_manufacturer_id:= 100000;

      ELSE

        l_prod_id:= NULL;
        l_prod_name:= rec.product_name_4 ;
        SELECT country_id INTO l_country_id FROM lm_countries WHERE country like 'UNK%';
        l_license_id:= NULL;
        l_generic_name:= to_char(substr(rec.product_name_4, 1, 1000));
        l_manufacturer_id:= 100000;

      END IF;

      INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, manufacturer_id, first_sus_prod, selected_view, sort_id,
                views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status,
                STUDY_PRODUCT_NUM, primary_event, family_id, prod_lic_id,
                LAST_UPDATE_TIME, ud_text_10, ud_text_11, ud_text_12, enterprise_id)
          VALUES (rec.case_id, l_seq_num, l_prod_id, 1, 0, l_manufacturer_id, 0, 1, 4, 1, l_country_id, l_prod_name, l_generic_name,
              rec.product_name_4,l_prod_name, 1, 0, l_primary_event, l_family_id,
               l_license_id, pkg_util.g_migration_date, rec.license_number_4, rec.product_name_4,  rec.mah_number, pkg_util.g_enterprise_id );

    END IF;


    --------------- Product-5--------------------------

    l_seq_num           :=NULL;
    l_prod_id        :=NULL;
    l_prod_name        :=NULL;
    l_manufacturer_id    :=NULL;
    l_country_id      :=NULL;
    l_license_id      :=NULL;
    l_family_id        :=NULL;
    l_generic_name      :=NULL;

    IF rec.license_number_5 IS NOT NULL AND rec.product_name_5 IS NOT NULL THEN

      l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');

      SELECT COUNT(*) INTO v_count FROM  scase_event ce
          WHERE ce.case_id=rec.case_id AND ce.sort_id=1;

      IF v_count = 1 THEN

        SELECT ce.seq_num INTO l_primary_event FROM scase_event ce
        WHERE ce.case_id=rec.case_id AND ce.sort_id=1;
      ELSE
        l_primary_event:= NULL;
      END IF;


      SELECT COUNT(*) INTO v_count
        FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_5
        AND s_prod_name = rec.product_name_5;

      IF  v_count >0 THEN

        SELECT t_prod_id, t_country_id, t_license_id INTO
           l_prod_id, l_country_id, l_license_id
          FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_5
        AND s_prod_name = rec.product_name_5;

        SELECT trade_name INTO l_prod_name FROM lm_license WHERE deleted IS NULL AND license_id= l_license_id;

        SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
            WHERE product_id = l_prod_id;

        l_manufacturer_id:= 100000;

      ELSE

        l_prod_id:= NULL;
        l_prod_name:= rec.product_name_5 ;
        SELECT country_id INTO l_country_id FROM lm_countries WHERE country like 'UNK%';
        l_license_id:= NULL;
        l_generic_name:= to_char(substr(rec.product_name_5, 1, 1000));
        l_manufacturer_id:= 100000;

      END IF;

      INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, manufacturer_id, first_sus_prod, selected_view, sort_id,
                views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status,
                STUDY_PRODUCT_NUM, primary_event, family_id, prod_lic_id,
                LAST_UPDATE_TIME, ud_text_10, ud_text_11, ud_text_12, enterprise_id)
          VALUES (rec.case_id, l_seq_num, l_prod_id, 1, 0, l_manufacturer_id, 0, 1, 5, 1, l_country_id, l_prod_name, l_generic_name,
              rec.product_name_5,l_prod_name, 1, 0, l_primary_event, l_family_id,
               l_license_id, pkg_util.g_migration_date, rec.license_number_5, rec.product_name_5,  rec.mah_number, pkg_util.g_enterprise_id );

    END IF;

    --------------- Product-6--------------------------

    l_seq_num           :=NULL;
    l_prod_id        :=NULL;
    l_prod_name        :=NULL;
    l_manufacturer_id    :=NULL;
    l_country_id      :=NULL;
    l_license_id      :=NULL;
    l_family_id        :=NULL;
    l_generic_name      :=NULL;

    IF rec.license_number_6 IS NOT NULL AND rec.product_name_6 IS NOT NULL THEN

      l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');

      SELECT COUNT(*) INTO v_count FROM  scase_event ce
          WHERE ce.case_id=rec.case_id AND ce.sort_id=1;

      IF v_count = 1 THEN

        SELECT ce.seq_num INTO l_primary_event FROM scase_event ce
        WHERE ce.case_id=rec.case_id AND ce.sort_id=1;
      ELSE
        l_primary_event:= NULL;
      END IF;


      SELECT COUNT(*) INTO v_count
        FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_6
        AND s_prod_name = rec.product_name_6;

      IF  v_count >0 THEN

        SELECT t_prod_id, t_country_id, t_license_id INTO
           l_prod_id, l_country_id, l_license_id
          FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_6
        AND s_prod_name = rec.product_name_6;

        SELECT trade_name INTO l_prod_name FROM lm_license WHERE deleted IS NULL AND license_id= l_license_id;

        SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
            WHERE product_id = l_prod_id;

        l_manufacturer_id:= 100000;

      ELSE

        l_prod_id:= NULL;
        l_prod_name:= rec.product_name_6 ;
        SELECT country_id INTO l_country_id FROM lm_countries WHERE country like 'UNK%';
        l_license_id:= NULL;
        l_generic_name:= to_char(substr(rec.product_name_6, 1, 1000));
        l_manufacturer_id:= 100000;

      END IF;

      INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, manufacturer_id, first_sus_prod, selected_view, sort_id,
                views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status,
                STUDY_PRODUCT_NUM, primary_event, family_id, prod_lic_id,
                LAST_UPDATE_TIME, ud_text_10, ud_text_11, ud_text_12, enterprise_id)
          VALUES (rec.case_id, l_seq_num, l_prod_id, 1, 0, l_manufacturer_id, 0, 1, 6, 1, l_country_id, l_prod_name, l_generic_name,
              rec.product_name_6,l_prod_name, 1, 0, l_primary_event, l_family_id,
               l_license_id, pkg_util.g_migration_date, rec.license_number_6, rec.product_name_6,  rec.mah_number, pkg_util.g_enterprise_id );

    END IF;

    --------------- Product-7--------------------------

    l_seq_num           :=NULL;
    l_prod_id        :=NULL;
    l_prod_name        :=NULL;
    l_manufacturer_id    :=NULL;
    l_country_id      :=NULL;
    l_license_id      :=NULL;
    l_family_id        :=NULL;
    l_generic_name      :=NULL;

    IF rec.license_number_7 IS NOT NULL AND rec.product_name_7 IS NOT NULL THEN

      l_seq_num := pkg_util.f_fetch_sequence ('CASE_PRODUCT');

      SELECT COUNT(*) INTO v_count FROM  scase_event ce
          WHERE ce.case_id=rec.case_id AND ce.sort_id=1;

      IF v_count = 1 THEN

        SELECT ce.seq_num INTO l_primary_event FROM scase_event ce
        WHERE ce.case_id=rec.case_id AND ce.sort_id=1;
      ELSE
        l_primary_event:= NULL;
      END IF;


      SELECT COUNT(*) INTO v_count
        FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_7
        AND s_prod_name = rec.product_name_7;

      IF  v_count >0 THEN

        SELECT t_prod_id, t_country_id, t_license_id INTO
           l_prod_id, l_country_id, l_license_id
          FROM dm_product_mapping
        WHERE s_anda_number = rec.license_number_7
        AND s_prod_name = rec.product_name_7;

        SELECT trade_name INTO l_prod_name FROM lm_license WHERE deleted IS NULL AND license_id= l_license_id;

        SELECT family_id, to_char(SUBSTR(prod_generic_name, 1, 1000)) INTO l_family_id, l_generic_name FROM LM_PRODUCT
            WHERE product_id = l_prod_id;

        l_manufacturer_id:= 100000;

      ELSE

        l_prod_id:= NULL;
        l_prod_name:= rec.product_name_7 ;
        SELECT country_id INTO l_country_id FROM lm_countries WHERE country like 'UNK%';
        l_license_id:= NULL;
        l_generic_name:= to_char(substr(rec.product_name_7, 1, 1000));
        l_manufacturer_id:= 100000;

      END IF;

      INSERT INTO scase_product(case_id, seq_num, product_id, drug_type, pat_exposure, manufacturer_id, first_sus_prod, selected_view, sort_id,
                views_available, country_id, product_name, generic_name, prod_reptd, prod_coded, prod_code_status,
                STUDY_PRODUCT_NUM, primary_event, family_id, prod_lic_id,
                LAST_UPDATE_TIME, ud_text_10, ud_text_11, ud_text_12, enterprise_id)
          VALUES (rec.case_id, l_seq_num, l_prod_id, 1, 0, l_manufacturer_id, 0, 1, 7, 1, l_country_id, l_prod_name, l_generic_name,
              rec.product_name_7,l_prod_name, 1, 0, l_primary_event, l_family_id,
               l_license_id, pkg_util.g_migration_date, rec.license_number_7, rec.product_name_7,  rec.mah_number, pkg_util.g_enterprise_id );

    END IF;

    END LOOP;


      UPDATE dm_status
        SET status = 1,
             conv_end_time = SYSDATE
        WHERE table_name = 'CASE_PRODUCT';

      COMMIT;
    EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_product',
                         'pkg_main',
                         SQLERRM,
                         'CASE product INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_product , please see table LOGGER_LOGS for detials'
            );
     END p_migrate_case_product;


   PROCEDURE p_migrate_case_dose_regimens
/***********************************************************************
Name:        p_migrate_case_dose_regimens
Description: This procedure migrates case data to CASE_DOSE_REGIMENS table.
***********************************************************************/
   AS
    v_count                 NUMBER;
    l_case_id               NUMBER;
    l_prod_seq_num          NUMBER;
    l_duration_seconds      NUMBER;
   BEGIN


  FOR rec in (SELECT DISTINCT MAH_NUMBER, LICENSE_NUMBER_1, PRODUCT_NAME_1, DOSE_VALUE_1, DOSE_UNIT_1, ROUTE_OF_ADMIN_1, FREQUENCY_1,
            START_DATE_TREATMENT_1, STOP_DATE_TREATMENT_1, TREATMENT_DURATION_VALUE_1, TREATMENT_DURATION_UNIT_1,
            LICENSE_NUMBER_2, PRODUCT_NAME_2, DOSE_VALUE_2, DOSE_UNIT_2, ROUTE_OF_ADMIN_2, FREQUENCY_2,
            START_DATE_TREATMENT_2, STOP_DATE_TREATMENT_2, TREATMENT_DURATION_VALUE_2, TREATMENT_DURATION_UNIT_2,
            LICENSE_NUMBER_3, PRODUCT_NAME_3, DOSE_VALUE_3, DOSE_UNIT_3, ROUTE_OF_ADMIN_3, FREQUENCY_3,
            START_DATE_TREATMENT_3, STOP_DATE_TREATMENT_3, TREATMENT_DURATION_VALUE_3, TREATMENT_DURATION_UNIT_3,
            LICENSE_NUMBER_4,PRODUCT_NAME_4,DOSE_VALUE_4,DOSE_UNIT_4,ROUTE_OF_ADMIN_4,FREQUENCY_4,
            START_DATE_TREATMENT_4,STOP_DATE_TREATMENT_4,TREATMENT_DURATION_VALUE_4,TREATMENT_DURATION_UNIT_4,
            LICENSE_NUMBER_5,PRODUCT_NAME_5,DOSE_VALUE_5,DOSE_UNIT_5,ROUTE_OF_ADMIN_5,FREQUENCY_5,
            START_DATE_TREATMENT_5,STOP_DATE_TREATMENT_5,TREATMENT_DURATION_VALUE_5,TREATMENT_DURATION_UNIT_5,
            LICENSE_NUMBER_6,PRODUCT_NAME_6,DOSE_VALUE_6,DOSE_UNIT_6,ROUTE_OF_ADMIN_6,FREQUENCY_6,
            START_DATE_TREATMENT_6,STOP_DATE_TREATMENT_6,TREATMENT_DURATION_VALUE_6,TREATMENT_DURATION_UNIT_6,
            LICENSE_NUMBER_7,PRODUCT_NAME_7,DOSE_VALUE_7,DOSE_UNIT_7,ROUTE_OF_ADMIN_7,FREQUENCY_7,
            START_DATE_TREATMENT_7,STOP_DATE_TREATMENT_7,TREATMENT_DURATION_VALUE_7,TREATMENT_DURATION_UNIT_7
            FROM src_case_event)
  LOOP

--------------Product 1-----------------------------------------------
    SELECT COUNT(*)  INTO v_count FROM scase_product cp --changed to scase_product
            WHERE cp.ud_text_12=rec.mah_number
            AND cp.ud_text_11=rec.product_name_1
            AND cp.ud_text_10=rec.license_number_1;

    IF v_count>0 THEN

      SELECT case_id INTO l_case_id FROM scase_master WHERE ud_text_12=rec.mah_number;

      SELECT seq_num INTO l_prod_seq_num FROM scase_product CP WHERE cp.ud_text_12=rec.mah_number
                                  AND cp.ud_text_11=rec.product_name_1
                                  AND cp.ud_text_10=rec.license_number_1;

      IF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_1)),1,3)='SEC' THEN
        l_duration_seconds:= rec.treatment_duration_value_1*1;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_1)),1,3)='MIN' THEN
          l_duration_seconds:= rec.treatment_duration_value_1*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_1)),1,3)='HOU' THEN
          l_duration_seconds:= rec.treatment_duration_value_1*60*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_1)),1,3)='DAY' THEN
        l_duration_seconds:= rec.treatment_duration_value_1*60*60*24;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_1)),1,3)='WEE' THEN
          l_duration_seconds:= rec.treatment_duration_value_1*60*60*24*7;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_1)),1,3)='MON' THEN
          l_duration_seconds:= rec.treatment_duration_value_1*60*60*24*30;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_1)),1,3)='YEA' THEN
          l_duration_seconds:= rec.treatment_duration_value_1*60*60*24*365;
      ELSE
        l_duration_seconds:= NULL;
      END IF;


      INSERT INTO scase_dose_regimens
        (case_id,
         seq_num,
         log_no,
         start_datetime,
         start_datetime_res,
         stop_datetime,
         stop_datetime_res,
         dose,
         dose_unit_id,
         freq_id,
         admin_route_id,
         sort_id,
         dose_description,
         duration_seconds,
         vaers_block_10,
         vaers_block_14,
         duration,
         enterprise_id)
      VALUES
        (l_case_id,
         l_prod_seq_num,
         pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'),
         DECODE(pkg_util.isdate(rec.START_DATE_TREATMENT_1,'dd-mm-yyyy'),1,to_date(rec.START_DATE_TREATMENT_1,'dd-mm-yyyy'),null),
         0,
         DECODE(pkg_util.isdate(rec.STOP_DATE_TREATMENT_1,'dd-mm-yyyy'),1,to_date(rec.STOP_DATE_TREATMENT_1,'dd-mm-yyyy'),null),
         0,
         rec.DOSE_VALUE_1,
         pkg_util.f_get_argus_code('LM_DOSE_UNITS', rec.DOSE_UNIT_1),
         pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY', rec.FREQUENCY_1),
         pkg_util.f_get_argus_code('LM_ADMIN_ROUTE', rec.ROUTE_OF_ADMIN_1),
         1,
         DECODE(rec.DOSE_VALUE_1,NULL, NULL, rec.DOSE_VALUE_1 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_UNITS', rec.DOSE_UNIT_1)||','|| rec.FREQUENCY_1 ||' ' || pkg_util.f_get_argus_value('LM_DOSE_FREQUENCY', rec.FREQUENCY_1)),
         l_duration_seconds,--changed above line
         0,
         0,
         l_duration_seconds,
         pkg_util.g_enterprise_id);
    END IF;

------------------Product 2 -----------------

  SELECT COUNT(*)  INTO v_count FROM scase_product cp --changed to scase_product
            WHERE cp.ud_text_12=rec.mah_number
            AND cp.ud_text_11=rec.product_name_2
            AND cp.ud_text_10=rec.license_number_2;

    IF v_count>0 THEN

      SELECT case_id INTO l_case_id FROM scase_master WHERE ud_text_12=rec.mah_number;

      SELECT seq_num INTO l_prod_seq_num FROM scase_product cp WHERE cp.ud_text_12=rec.mah_number
                                  AND cp.ud_text_11=rec.product_name_2
                                  AND cp.ud_text_10=rec.license_number_2;

      IF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_2)),1,3)='SEC' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_2*1;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_2)),1,3)='MIN' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_2*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_2)),1,3)='HOU' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_2*60*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_2)),1,3)='DAY' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_2*60*60*24;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_2)),1,3)='WEE' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_2*60*60*24*7;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_2)),1,3)='MON' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_2*60*60*24*30;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_2)),1,3)='YEA' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_2*60*60*24*365;
      ELSE
        l_duration_seconds:= NULL;
      END IF;


     INSERT INTO scase_dose_regimens
        (case_id,
         seq_num,
         log_no,
         start_datetime,
         start_datetime_res,
         stop_datetime,
         stop_datetime_res,
         dose,
         dose_unit_id,
         freq_id,
         admin_route_id,
         sort_id,
         dose_description,
         duration_seconds,
         vaers_block_10,
         vaers_block_14,
         duration,
         enterprise_id)
      VALUES
        (l_case_id,
         l_prod_seq_num,
         pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'),
         DECODE(pkg_util.isdate(rec.START_DATE_TREATMENT_2,'dd-mm-yyyy'),1,to_date(rec.START_DATE_TREATMENT_2,'dd-mm-yyyy'),null),
         0,
         DECODE(pkg_util.isdate(rec.STOP_DATE_TREATMENT_2,'dd-mm-yyyy'),1,to_date(rec.STOP_DATE_TREATMENT_2,'dd-mm-yyyy'),null),
         0,
         rec.DOSE_VALUE_2,
         pkg_util.f_get_argus_code('LM_DOSE_UNITS', rec.DOSE_UNIT_2),
         pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY', rec.FREQUENCY_2),
         pkg_util.f_get_argus_code('LM_ADMIN_ROUTE', rec.ROUTE_OF_ADMIN_2),
         1,
         DECODE(rec.DOSE_VALUE_2,NULL,NULL,rec.DOSE_VALUE_2 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_UNITS', rec.DOSE_UNIT_2) ||','|| rec.FREQUENCY_2 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_FREQUENCY', rec.FREQUENCY_2)),
         l_duration_seconds,--changed above line
         0,
         0,
         l_duration_seconds,
         pkg_util.g_enterprise_id);

      END IF;

------------------Product 3 -----------------

  SELECT COUNT(*)  INTO v_count FROM scase_product cp --changed to scase_product
            WHERE cp.ud_text_12=rec.mah_number
            AND cp.ud_text_11=rec.product_name_3
            AND cp.ud_text_10=rec.license_number_3;

    IF v_count>0 THEN

      SELECT case_id INTO l_case_id FROM scase_master WHERE ud_text_12=rec.mah_number;

      SELECT seq_num INTO l_prod_seq_num FROM scase_product cp WHERE cp.ud_text_12=rec.mah_number
                                  AND cp.ud_text_11=rec.product_name_3
                                  AND cp.ud_text_10=rec.license_number_3;

      IF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_3)),1,3)='SEC' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_3*1;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_3)),1,3)='MIN' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_3*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_3)),1,3)='HOU' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_3*60*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_3)),1,3)='DAY' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_3*60*60*24;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_3)),1,3)='WEE' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_3*60*60*24*7;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_3)),1,3)='MON' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_3*60*60*24*30;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_3)),1,3)='YEA' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_3*60*60*24*365;
      ELSE
        l_duration_seconds:= NULL;
      END IF;


      INSERT INTO scase_dose_regimens
        (case_id,
         seq_num,
         log_no,
         start_datetime,
         start_datetime_res,
         stop_datetime,
         stop_datetime_res,
         dose,
         dose_unit_id,
         freq_id,
         admin_route_id,
         sort_id,
         dose_description,
         duration_seconds,
         vaers_block_10,
         vaers_block_14,
         duration,
         enterprise_id)
      VALUES
        (l_case_id,
         l_prod_seq_num,
         pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'),
         DECODE(pkg_util.isdate(rec.START_DATE_TREATMENT_3,'dd-mm-yyyy'),1,to_date(rec.START_DATE_TREATMENT_3,'dd-mm-yyyy'),null),
         0,
         DECODE(pkg_util.isdate(rec.STOP_DATE_TREATMENT_3,'dd-mm-yyyy'),1,to_date(rec.STOP_DATE_TREATMENT_3,'dd-mm-yyyy'),null),
         0,
         rec.DOSE_VALUE_3,
         pkg_util.f_get_argus_code('LM_DOSE_UNITS', rec.DOSE_UNIT_3),
         pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY', rec.FREQUENCY_3),
         pkg_util.f_get_argus_code('LM_ADMIN_ROUTE', rec.ROUTE_OF_ADMIN_3),
         1,
         DECODE(rec.DOSE_VALUE_3,NULL,NULL,rec.DOSE_VALUE_3 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_UNITS', rec.DOSE_UNIT_3)||','|| rec.FREQUENCY_3 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_FREQUENCY', rec.FREQUENCY_3)),
         l_duration_seconds,--changed above line
         0,
         0,
         l_duration_seconds,
         pkg_util.g_enterprise_id);

    END IF;

------------------Product 4 -----------------

  SELECT COUNT(*)  INTO v_count FROM scase_product cp  --changed to scase_product
            WHERE cp.ud_text_12=rec.mah_number
            AND cp.ud_text_11=rec.product_name_4
            AND cp.ud_text_10=rec.license_number_4;

    IF v_count>0 THEN

      SELECT case_id INTO l_case_id FROM scase_master WHERE ud_text_12=rec.mah_number;

      SELECT seq_num INTO l_prod_seq_num FROM scase_product cp WHERE cp.ud_text_12=rec.mah_number
                                  AND cp.ud_text_11=rec.product_name_4
                                  AND cp.ud_text_10=rec.license_number_4;

      IF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_4)),1,3)='SEC' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_4*1;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_4)),1,3)='MIN' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_4*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_4)),1,3)='HOU' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_4*60*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_4)),1,3)='DAY' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_4*60*60*24;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_4)),1,3)='WEE' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_4*60*60*24*7;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_4)),1,3)='MON' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_4*60*60*24*30;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_4)),1,3)='YEA' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_4*60*60*24*365;
      ELSE
        l_duration_seconds:= NULL;
      END IF;


      INSERT INTO scase_dose_regimens
        (case_id,
         seq_num,
         log_no,
         start_datetime,
         start_datetime_res,
         stop_datetime,
         stop_datetime_res,
         dose,
         dose_unit_id,
         freq_id,
         admin_route_id,
         sort_id,
         dose_description,
         duration_seconds,
         vaers_block_10,
         vaers_block_14,
         duration,
         enterprise_id)
      VALUES
        (l_case_id,
         l_prod_seq_num,
         pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'),
         DECODE(pkg_util.isdate(rec.START_DATE_TREATMENT_4,'dd-mm-yyyy'),1,to_date(rec.START_DATE_TREATMENT_4,'dd-mm-yyyy'),null),
         0,
         DECODE(pkg_util.isdate(rec.STOP_DATE_TREATMENT_4,'dd-mm-yyyy'),1,to_date(rec.STOP_DATE_TREATMENT_4,'dd-mm-yyyy'),null),
         0,
         rec.DOSE_VALUE_4,
         pkg_util.f_get_argus_code('LM_DOSE_UNITS', rec.DOSE_UNIT_4),
         pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY', rec.FREQUENCY_4),
         pkg_util.f_get_argus_code('LM_ADMIN_ROUTE', rec.ROUTE_OF_ADMIN_4),
         1,
         DECODE(rec.DOSE_VALUE_4,NULL,NULL,rec.DOSE_VALUE_4 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_UNITS', rec.DOSE_UNIT_4)||','|| rec.FREQUENCY_4 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_FREQUENCY', rec.FREQUENCY_4)),
         l_duration_seconds,--changed above line
         0,
         0,
         l_duration_seconds,
         pkg_util.g_enterprise_id);

    END IF;

   ------------------Product 5 -----------------

  SELECT COUNT(*)  INTO v_count FROM scase_product cp  --changed to scase_product
            WHERE cp.ud_text_12=rec.mah_number
            AND cp.ud_text_11=rec.product_name_5
            AND cp.ud_text_10=rec.license_number_5;

    IF v_count>0 THEN

      SELECT case_id INTO l_case_id FROM scase_master WHERE ud_text_12=rec.mah_number;

      SELECT seq_num INTO l_prod_seq_num FROM scase_product cp WHERE cp.ud_text_12=rec.mah_number
                                  AND cp.ud_text_11=rec.product_name_5
                                  AND cp.ud_text_10=rec.license_number_5;

      IF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_5)),1,3)='SEC' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_5*1;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_5)),1,3)='MIN' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_5*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_5)),1,3)='HOU' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_5*60*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_5)),1,3)='DAY' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_5*60*60*24;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_5)),1,3)='WEE' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_5*60*60*24*7;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_5)),1,3)='MON' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_5*60*60*24*30;
      ELSIF SUBSTR(UPPER(TRIM(rec.TREATMENT_DURATION_UNIT_5)),1,3)='YEA' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_5*60*60*24*365;
      ELSE
        l_duration_seconds:= NULL;
      END IF;


      INSERT INTO scase_dose_regimens
        (case_id,
         seq_num,
         log_no,
         start_datetime,
         start_datetime_res,
         stop_datetime,
         stop_datetime_res,
         dose,
         dose_unit_id,
         freq_id,
         admin_route_id,
         sort_id,
         dose_description,
         duration_seconds,
         vaers_block_10,
         vaers_block_14,
         duration,
         enterprise_id)
      VALUES
        (l_case_id,
         l_prod_seq_num,
         pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'),
         DECODE(pkg_util.isdate(rec.START_DATE_TREATMENT_5,'dd-mm-yyyy'),1,to_date(rec.START_DATE_TREATMENT_5,'dd-mm-yyyy'),null),
         0,
         DECODE(pkg_util.isdate(rec.STOP_DATE_TREATMENT_5,'dd-mm-yyyy'),1,to_date(rec.STOP_DATE_TREATMENT_5,'dd-mm-yyyy'),null),
         0,
         rec.DOSE_VALUE_5,
         pkg_util.f_get_argus_code('LM_DOSE_UNITS', rec.DOSE_UNIT_5),
         pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY', rec.FREQUENCY_5),
         pkg_util.f_get_argus_code('LM_ADMIN_ROUTE', rec.ROUTE_OF_ADMIN_5),
         1,
         DECODE(rec.DOSE_VALUE_5,NULL,NULL,rec.DOSE_VALUE_5 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_UNITS', rec.DOSE_UNIT_5)||','|| rec.FREQUENCY_5|| ' ' || pkg_util.f_get_argus_value('LM_DOSE_FREQUENCY', rec.FREQUENCY_5)),
         l_duration_seconds,--changed above line
         0,
         0,
         l_duration_seconds,
         pkg_util.g_enterprise_id);

    END IF;

    ----end of product 5------

    ------------------Product 6 -----------------

  SELECT COUNT(*)  INTO v_count FROM scase_product cp  --changed to scase_product
            WHERE cp.ud_text_12=rec.mah_number
            AND cp.ud_text_11=rec.product_name_6
            AND cp.ud_text_10=rec.license_number_6;

    IF v_count>0 THEN

      SELECT case_id INTO l_case_id FROM scase_master WHERE ud_text_12=rec.mah_number;

      SELECT seq_num INTO l_prod_seq_num FROM scase_product cp WHERE cp.ud_text_12=rec.mah_number
                                  AND cp.ud_text_11=rec.product_name_6
                                  AND cp.ud_text_10=rec.license_number_6;

      IF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_6)),1,3)='SEC' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_6*1;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_6)),1,3)='MIN' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_6*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_6)),1,3)='HOU' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_6*60*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_6)),1,3)='DAY' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_6*60*60*24;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_6)),1,3)='WEE' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_6*60*60*24*7;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_6)),1,3)='MON' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_6*60*60*24*30;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_6)),1,3)='YEA' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_6*60*60*24*365;
      ELSE
        l_duration_seconds:= NULL;
      END IF;

     INSERT INTO scase_dose_regimens
        (case_id,
         seq_num,
         log_no,
         start_datetime,
         start_datetime_res,
         stop_datetime,
         stop_datetime_res,
         dose,
         dose_unit_id,
         freq_id,
         admin_route_id,
         sort_id,
         dose_description,
         duration_seconds,
         vaers_block_10,
         vaers_block_14,
         duration,
         enterprise_id)
      VALUES
        (l_case_id,
         l_prod_seq_num,
         pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'),
         DECODE(pkg_util.isdate(rec.START_DATE_TREATMENT_6,'dd-mm-yyyy'),1,to_date(rec.START_DATE_TREATMENT_6,'dd-mm-yyyy'),null),
         0,
         DECODE(pkg_util.isdate(rec.STOP_DATE_TREATMENT_6,'dd-mm-yyyy'),1,to_date(rec.STOP_DATE_TREATMENT_6,'dd-mm-yyyy'),null),
         0,
         rec.DOSE_VALUE_6,
         pkg_util.f_get_argus_code('LM_DOSE_UNITS', rec.DOSE_UNIT_6),
         pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY', rec.FREQUENCY_6),
         pkg_util.f_get_argus_code('LM_ADMIN_ROUTE', rec.ROUTE_OF_ADMIN_6),
         1,
         DECODE(rec.DOSE_VALUE_6,NULL,NULL,rec.DOSE_VALUE_6 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_UNITS', rec.DOSE_UNIT_6)||','|| rec.FREQUENCY_6 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_FREQUENCY', rec.FREQUENCY_6)),
         l_duration_seconds,--changed above line
         0,
         0,
         l_duration_seconds,
         pkg_util.g_enterprise_id);

    END IF;

   --- end of product no 6----

     ------------------Product 7 -----------------

  SELECT COUNT(*)  INTO v_count FROM scase_product cp  --changed to scase_product
            WHERE cp.ud_text_12=rec.mah_number
            AND cp.ud_text_11=rec.product_name_7
            AND cp.ud_text_10=rec.license_number_7;

    IF v_count>0 THEN

      SELECT case_id INTO l_case_id FROM scase_master WHERE ud_text_12=rec.mah_number;

      SELECT seq_num INTO l_prod_seq_num FROM scase_product cp WHERE cp.ud_text_12=rec.mah_number
                                  AND cp.ud_text_11=rec.product_name_7
                                  AND cp.ud_text_10=rec.license_number_7;

      IF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_7)),1,3)='SEC' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_7*1;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_7)),1,3)='MIN' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_7*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_7)),1,3)='HOU' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_7*60*60;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_7)),1,3)='DAY' THEN
        l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_7*60*60*24;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_7)),1,3)='WEE' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_7*60*60*24*7;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_7)),1,3)='MON' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_7*60*60*24*30;
      ELSIF SUBSTR(UPPER(TRIM(rec.treatment_duration_unit_7)),1,3)='YEA' THEN
          l_duration_seconds:= rec.TREATMENT_DURATION_VALUE_7*60*60*24*365;
      ELSE
        l_duration_seconds:= NULL;
      END IF;


      INSERT INTO scase_dose_regimens
        (case_id,
         seq_num,
         log_no,
         start_datetime,
         start_datetime_res,
         stop_datetime,
         stop_datetime_res,
         dose,
         dose_unit_id,
         freq_id,
         admin_route_id,
         sort_id,
         dose_description,
         duration_seconds,
         vaers_block_10,
         vaers_block_14,
         duration,
         enterprise_id)
      VALUES
        (l_case_id,
         l_prod_seq_num,
         pkg_util.f_fetch_sequence('CASE_DOSE_REGIMENS'),
         DECODE(pkg_util.isdate(rec.START_DATE_TREATMENT_7,'dd-mm-yyyy'),1,to_date(rec.START_DATE_TREATMENT_7,'dd-mm-yyyy'),null),
         0,
         DECODE(pkg_util.isdate(rec.STOP_DATE_TREATMENT_7,'dd-mm-yyyy'),1,to_date(rec.STOP_DATE_TREATMENT_7,'dd-mm-yyyy'),null),
         0,
         rec.DOSE_VALUE_7,
         pkg_util.f_get_argus_code('LM_DOSE_UNITS', rec.DOSE_UNIT_7),
         pkg_util.f_get_argus_code('LM_DOSE_FREQUENCY', rec.FREQUENCY_7),
         pkg_util.f_get_argus_code('LM_ADMIN_ROUTE', rec.ROUTE_OF_ADMIN_7),
         1,
         DECODE(rec.DOSE_VALUE_7,NULL,NULL,rec.DOSE_VALUE_7 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_UNITS', rec.DOSE_UNIT_7)||','|| rec.FREQUENCY_7 || ' ' || pkg_util.f_get_argus_value('LM_DOSE_FREQUENCY', rec.FREQUENCY_7)),
         l_duration_seconds,--changed above line
         0,
         0,
         l_duration_seconds,
         pkg_util.g_enterprise_id);

    END IF;

   ---- end of product no 7 ------


  END LOOP; ---ADDED END LOOP HERE  ALSO MIGHT NEED TO ADD UPTO PRODUCT 7

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_DOSE_REGIMENS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_dose_regimens',
                         'pkg_main',
                         SQLERRM,
                         'case_dose_regimens INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_dose_regimens , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_dose_regimens;


PROCEDURE p_migrate_case_prod_ingredient
/***********************************************************************
Name:        p_migrate_case_prod_ingredient
Description: This procedure migrates case data to CASE_PROD_INGREDIENT table.
***********************************************************************/
   AS

   BEGIN
    EXECUTE IMMEDIATE 'analyze table scase_product compute statistics';

    INSERT INTO scase_prod_ingredient
        (case_id, seq_num, item, ingredient_id, conc_unit_id,
                 concentration, enterprise_id, deleted)
         SELECT   cp.case_id, cp.seq_num,
                  pkg_util.f_fetch_sequence ('CASE_PROD_INGREDIENT'),
                  lpc.ingredient_id, lpc.conc_unit_id, lpc.concentration,
                  pkg_util.g_enterprise_id,
                  cp.deleted
             FROM scase_product cp, lm_product_concentrations lpc
            WHERE cp.product_id = lpc.product_id
              AND cp.product_id IS NOT NULL
              AND lpc.deleted IS NULL
         ORDER BY cp.case_id, cp.seq_num;

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PROD_INGREDIENT'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_prod_ingredient',
                         'pkg_main',
                         SQLERRM,
                         'case_dose_regimens INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_prod_ingredient , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_prod_ingredient;

      PROCEDURE p_migrate_case_prod_drugs
   /***********************************************************************
   Name:        p_migrate_case_prod_drugs
   Description: This procedure migrates case data to CASE_PROD_DRUGS table.
   ***********************************************************************/
   AS

  l_formulation_id    NUMBER;
  l_sort_id           NUMBER(5);
   BEGIN

    SELECT formulation_id INTO l_formulation_id FROM lm_formulation WHERE UPPER(TRIM(formulation))=UPPER(TRIM('UNKNOWN'));

    INSERT INTO scase_prod_drugs
      (case_id,
       seq_num,
       formulation_id,
	   obtain_drug_country_id,
       drug_auth_country_id,
       first_dose_res,
       last_dose_res,
       enterprise_id)
      (SELECT cp.case_id,
              cp.seq_num,
              l_formulation_id,
			  222,
              222,
              0,
              0,
              pkg_util.g_enterprise_id
         FROM scase_product cp);


    FOR rec IN (SELECT DISTINCT case_id FROM scase_prod_drugs)
      LOOP
      l_sort_id:= 1;
      FOR rec1 IN (SELECT case_id, seq_num FROM scase_prod_drugs WHERE case_id=rec.case_id)
        LOOP
          UPDATE scase_prod_drugs SET sort_id=l_sort_id WHERE seq_num=rec1.seq_num AND case_id=rec1.case_id;
          l_sort_id:=l_sort_id + 1;
      END LOOP;
    END LOOP;


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_PROD_DRUGS';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_prod_drugs',
                         'pkg_main',
                         SQLERRM,
                         'CASE_PROD_DRUGS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_prod_drugs , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_prod_drugs;

  PROCEDURE p_migrate_case_event_assess
   /***********************************************************************
    Name:        p_migrate_case_event_assess
    Description: This procedure migrates data to CASE_EVENT_ASSESS table.
    ***********************************************************************/
   AS
    v_det_causality_id           	  NUMBER;
      v_rpt_causality_id           	NUMBER;
      v_det_listedness_id          	NUMBER;
      v_sourc_chk                  	NUMBER        := 0;
      v_source_id                  	NUMBER;
	  v_count						              NUMBER;
	  l_product_name_1				        VARCHAR2(200);
	  l_reported_causality_1          VARCHAR2(200);
	  l_company_causality_1           VARCHAR2(200);
	  l_product_name_2                VARCHAR2(200);
	  l_reported_causality_2          VARCHAR2(200);
	  l_company_causality_2           VARCHAR2(200);
	  l_product_name_3                VARCHAR2(200);
	  l_reported_causality_3          VARCHAR2(200);
	  l_company_causality_3           VARCHAR2(200);
	  l_product_name_4                VARCHAR2(200);
	  l_reported_causality_4          VARCHAR2(200);
	  l_company_causality_4           VARCHAR2(200);
	  l_product_name_5                VARCHAR2(200);
	  l_reported_causality_5          VARCHAR2(200);
	  l_company_causality_5           VARCHAR2(200);
	  l_product_name_6                VARCHAR2(200);
	  l_reported_causality_6          VARCHAR2(200);
	  l_company_causality_6           VARCHAR2(200);
	  l_product_name_7                VARCHAR2(200);
	  l_reported_causality_7          VARCHAR2(200);
	  l_company_causality_7           VARCHAR2(200);
	  l_listed_unlisted_1			        VARCHAR2(50);
	  l_listed_unlisted_2			        VARCHAR2(50);
	  l_listed_unlisted_3			        VARCHAR2(50);
	  l_listed_unlisted_4			        VARCHAR2(50);
	  l_listed_unlisted_5			        VARCHAR2(50);
	  l_listed_unlisted_6			        VARCHAR2(50);
	  l_listed_unlisted_7			        VARCHAR2(50);

      CURSOR c_cur_causality
      IS
          SELECT  cm.case_id AS case_id, cm.ud_text_12 AS src_case_num,
                  pkg_util.f_fetch_sequence ('CASE_EVENT_ASSESS') seq_num,
                  cp.seq_num AS prod_seq_num, cp.ud_text_11 src_prod,
                  ce.seq_num AS event_seq_num, ce.ud_number_12 src_sr_no, ce.ud_text_11 AS src_event
             FROM scase_event ce, scase_product cp, scase_master cm
            WHERE ce.case_id = cp.case_id
              AND ce.case_id = cm.case_id
              AND cp.case_id = cm.case_id
              AND cp.drug_type = 1
              AND cp.product_id IS NOT NULL
         ORDER BY cm.case_id, cp.sort_id, ce.sort_id;

      CURSOR c_cur_listedness
      IS
          SELECT  cm.case_id as case_id, --cm.src_case_id as src_case_id,
                  pkg_util.f_fetch_sequence ('CASE_EVENT_ASSESS') seq_num,
                  cp.seq_num AS prod_seq_num,
                  cp.ud_text_11 AS src_prod,
                  ce.seq_num AS event_seq_num,
                  ce.ud_number_12 AS src_sr_no, ll.license_id,
                  NVL (ll.datasheet_id, 0) datasheet_id, ld.sheet_name,
                  cm.deleted
             FROM scase_event ce,
                  scase_product cp,
                  scase_master cm,
                  lm_license ll,
                  lm_lic_products llp,
                  lm_datasheet ld
            WHERE ce.case_id = cp.case_id
              AND ce.case_id = cm.case_id
              AND cp.case_id = cm.case_id
              AND cp.drug_type = 1
              AND ll.license_id = llp.license_id
              AND NVL (ll.datasheet_id, 0) = ld.datasheet_id
              AND cp.product_id = llp.product_id
              AND ll.deleted IS NULL
              AND llp.deleted IS NULL
              AND ld.deleted IS NULL
         ORDER BY cm.case_id, cp.sort_id, ce.sort_id;

   BEGIN



     FOR z IN c_cur_causality
      LOOP
         v_det_causality_id := NULL;
         v_rpt_causality_id := NULL;
		 v_count := NULL;
		 l_product_name_1 := NULL;
	     l_reported_causality_1 := NULL;
	     l_company_causality_1 := NULL;
	     l_product_name_2 := NULL;
	     l_reported_causality_2 := NULL;
	     l_company_causality_2 := NULL;
	     l_product_name_3 := NULL;
	     l_reported_causality_3 := NULL;
	     l_company_causality_3 := NULL;
	     l_product_name_4 := NULL;
	     l_reported_causality_4 := NULL;
	     l_company_causality_4 := NULL;
	     l_product_name_5  := NULL;
	     l_reported_causality_5 := NULL;
	     l_company_causality_5 := NULL;
	     l_product_name_6 := NULL;
	     l_reported_causality_6 := NULL;
	     l_company_causality_6 := NULL;
	     l_product_name_7 := NULL;
	     l_reported_causality_7 := NULL;
	     l_company_causality_7 := NULL;

		SELECT COUNT(*) INTO v_count FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no;

			IF v_count>0 THEN
				SELECT 	product_name_1, reported_causality_1, company_causality_1,
					product_name_2, reported_causality_2, company_causality_2,
					product_name_3, reported_causality_3, company_causality_3,
					product_name_4, reported_causality_4, company_causality_4,
					product_name_5, reported_causality_5, company_causality_5,
					product_name_6, reported_causality_6, company_causality_6,
					product_name_7, reported_causality_7, company_causality_7 INTO
					l_product_name_1, l_reported_causality_1, l_company_causality_1,
					l_product_name_2, l_reported_causality_2, l_company_causality_2,
					l_product_name_3, l_reported_causality_3, l_company_causality_3,
					l_product_name_4, l_reported_causality_4, l_company_causality_4,
					l_product_name_5, l_reported_causality_5, l_company_causality_5,
					l_product_name_6, l_reported_causality_6, l_company_causality_6,
					l_product_name_7, l_reported_causality_7, l_company_causality_7
				FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no;

				IF  UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_1)) THEN

						SELECT DECODE(UPPER(TRIM(sce.reported_causality_1)),'NOT REPORTED',7,'POSSIBLE',3,6),
							DECODE(UPPER(TRIM(sce.company_causality_1)),'NOT REPORTED',7,'POSSIBLE',3,6)
							INTO v_rpt_causality_id, v_det_causality_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_1))= UPPER(TRIM(l_product_name_1));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_2)) THEN

						SELECT DECODE(UPPER(TRIM(sce.reported_causality_2)),'NOT REPORTED',7,'POSSIBLE',3,6),
						  DECODE(UPPER(TRIM(sce.company_causality_2)),'NOT REPORTED',7,'POSSIBLE',3,6)
						  INTO v_rpt_causality_id, v_det_causality_id
						  FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_2))= UPPER(TRIM(l_product_name_2));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_3)) THEN

						SELECT DECODE(UPPER(TRIM(sce.reported_causality_3)),'NOT REPORTED',7,'POSSIBLE',3,6),
						  DECODE(UPPER(TRIM(sce.company_causality_3)),'NOT REPORTED',7,'POSSIBLE',3,6)
						  INTO v_rpt_causality_id, v_det_causality_id
						  FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_3))= UPPER(TRIM(l_product_name_3));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_4)) THEN

						SELECT DECODE(UPPER(TRIM(sce.reported_causality_4)),'NOT REPORTED',7,'POSSIBLE',3,6),
						  DECODE(UPPER(TRIM(sce.company_causality_4)),'NOT REPORTED',7,'POSSIBLE',3,6)
						  INTO v_rpt_causality_id, v_det_causality_id
						  FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_4))= UPPER(TRIM(l_product_name_4));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_5)) THEN

						SELECT DECODE(UPPER(TRIM(sce.reported_causality_5)),'NOT REPORTED',7,'POSSIBLE',3,6),
						  DECODE(UPPER(TRIM(sce.company_causality_5)),'NOT REPORTED',7,'POSSIBLE',3,6)
						  INTO v_rpt_causality_id, v_det_causality_id
						  FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_5))= UPPER(TRIM(l_product_name_5));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_6)) THEN

						SELECT DECODE(UPPER(TRIM(sce.reported_causality_6)),'NOT REPORTED',7,'POSSIBLE',3,6),
						  DECODE(UPPER(TRIM(sce.company_causality_6)),'NOT REPORTED',7,'POSSIBLE',3,6)
						  INTO v_rpt_causality_id, v_det_causality_id
						  FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_6))= UPPER(TRIM(l_product_name_6));

				ELSIF UPPER(TRIM(z.src_prod)) =  UPPER(TRIM(l_product_name_7)) THEN-- ADDED ELSIF HERE

						SELECT DECODE(UPPER(TRIM(sce.reported_causality_7)),'NOT REPORTED',7,'POSSIBLE',3,6),
						  DECODE(UPPER(TRIM(sce.company_causality_7)),'NOT REPORTED',7,'POSSIBLE',3,6)
						  INTO v_rpt_causality_id, v_det_causality_id
						  FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_7))= UPPER(TRIM(l_product_name_7));
				END IF;

				INSERT INTO scase_event_assess
                     (case_id, datasheet_id, license_id, det_causality_id,
                      rpt_causality_id, det_listedness_id, source_id,
                      event_seq_num, lam_assessed, prod_seq_num,
                      listed_justify, seq_num, cause_justify, enterprise_id,
                      deleted
                     )
              VALUES (z.case_id, 0, 0, v_det_causality_id,
                      v_rpt_causality_id, NULL, NULL,
                      z.event_seq_num, 0, z.prod_seq_num,
                      NULL, z.seq_num, NULL, pkg_util.g_enterprise_id,
                      NULL
                     );
			END IF;
        END LOOP;



      FOR z IN c_cur_listedness
      LOOP
         v_det_listedness_id := NULL;
         v_source_id := NULL;
		 l_product_name_1 := NULL;
	     l_listed_unlisted_1 := NULL;
	     l_product_name_2 := NULL;
	     l_listed_unlisted_2 := NULL;
	     l_product_name_3 := NULL;
	     l_listed_unlisted_3 := NULL;
	     l_product_name_4 := NULL;
	     l_listed_unlisted_4 := NULL;
	     l_product_name_5  := NULL;
	     l_listed_unlisted_5 := NULL;
	     l_product_name_6 := NULL;
	     l_listed_unlisted_6 := NULL;
	     l_product_name_7 := NULL;
	     l_listed_unlisted_7 := NULL;


		SELECT COUNT(*) INTO v_source_id FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no;

			IF v_source_id>0 THEN
				SELECT 	product_name_1, listed_unlisted_1,
					product_name_2, listed_unlisted_2,
					product_name_3, listed_unlisted_3,
					product_name_4, listed_unlisted_4,
					product_name_5, listed_unlisted_5,
					product_name_6, listed_unlisted_6,
					product_name_7, listed_unlisted_7 INTO
					l_product_name_1, l_listed_unlisted_1,
					l_product_name_2, l_listed_unlisted_2,
					l_product_name_3, l_listed_unlisted_3,
					l_product_name_4, l_listed_unlisted_4,
					l_product_name_5, l_listed_unlisted_5,
					l_product_name_6, l_listed_unlisted_6,
					l_product_name_7, l_listed_unlisted_7
				FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no;

				IF  UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_1)) THEN

						SELECT DECODE(UPPER(TRIM(sce.listed_unlisted_1)),'LISTED',1,'UNLISTED',2,3)
							INTO v_det_listedness_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_1))= UPPER(TRIM(l_product_name_1));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_2)) THEN

						SELECT DECODE(UPPER(TRIM(sce.listed_unlisted_2)),'LISTED',1,'UNLISTED',2,3)
							INTO v_det_listedness_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_2))= UPPER(TRIM(l_product_name_2));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_3)) THEN

						SELECT DECODE(UPPER(TRIM(sce.listed_unlisted_3)),'LISTED',1,'UNLISTED',2,3)
							INTO v_det_listedness_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_3))= UPPER(TRIM(l_product_name_3));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_4)) THEN

						SELECT DECODE(UPPER(TRIM(sce.listed_unlisted_4)),'LISTED',1,'UNLISTED',2,3)
							INTO v_det_listedness_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_4))= UPPER(TRIM(l_product_name_4));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_5)) THEN

						SELECT DECODE(UPPER(TRIM(sce.listed_unlisted_5)),'LISTED',1,'UNLISTED',2,3)
							INTO v_det_listedness_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_5))= UPPER(TRIM(l_product_name_5));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_6)) THEN

						SELECT DECODE(UPPER(TRIM(sce.listed_unlisted_6)),'LISTED',1,'UNLISTED',2,3)
							INTO v_det_listedness_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_6))= UPPER(TRIM(l_product_name_6));

				ELSIF UPPER(TRIM(z.src_prod))= UPPER(TRIM(l_product_name_7)) THEN--ADDED ELSIF HERE

						SELECT DECODE(UPPER(TRIM(sce.listed_unlisted_7)),'LISTED',1,'UNLISTED',2,3)
							INTO v_det_listedness_id
							FROM src_case_event sce WHERE sce.sr_no=z.src_sr_no AND UPPER(TRIM(sce.product_name_7))= UPPER(TRIM(l_product_name_7));
				END IF;



				INSERT INTO scase_event_assess
                     (case_id, datasheet_id, license_id, det_causality_id,
                      rpt_causality_id, det_listedness_id, source_id,
                      event_seq_num, lam_assessed, prod_seq_num,
                      listed_justify, seq_num, cause_justify, enterprise_id,
                      deleted
                     )
				VALUES (z.case_id, z.datasheet_id, z.license_id, NULL,
                      NULL, v_det_listedness_id, NULL,
                      z.event_seq_num, 0, z.prod_seq_num,
                      NULL, z.seq_num, NULL, pkg_util.g_enterprise_id,
                      NULL
                     );
			END IF;
      END LOOP;


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT_ASSESS'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_event_assess',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_EVENT_ASSESS INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_event_assess , please see table S_LOGGER_LOGS for detials'
            );

   END p_migrate_case_event_assess;


   PROCEDURE p_migrate_case_event_detail
   /***********************************************************************
   Name:        p_migrate_case_event_detail
   Description: This procedure migrates case data to CASE_EVENT_DETAIL table.
   ***********************************************************************/
   AS
   BEGIN

   INSERT INTO scase_event_detail
        (case_id, seq_num, prod_seq_num, event_seq_num, enterprise_id)
          (SELECT cm.case_id, pkg_util.f_fetch_sequence ('CASE_EVENT_DETAIL'), cp.seq_num, ce.seq_num, pkg_util.g_enterprise_id
          FROM scase_master cm,
               scase_event ce,
             scase_product cp
          WHERE cm.case_id = ce.case_id
          AND   cm.case_id = cp.case_id);


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT_DETAIL'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_event_detail',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_EVENT_DETAIL INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_event_detail , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_event_detail;

   PROCEDURE p_migrate_case_ep_rptblty
   /***********************************************************************
   Name:        p_migrate_case_ep_rptblty
   Description: This procedure migrates case data to CASE_EVENT_PRODUCT_RPTBLTY table.
   ***********************************************************************/
   AS
   BEGIN


      INSERT INTO scase_event_product_rptblty
                  (case_id, reportability, event_seq_num, prod_seq_num, enterprise_id)
         (SELECT cm.case_id,
         NVL((SELECT reportability FROM lm_causality WHERE causality_id=
         (SELECT cea.det_causality_id FROM scase_event_assess cea WHERE cea.case_id=cm.case_id
            AND cea.event_seq_num=ce.seq_num AND cea.prod_seq_num=cp.seq_num
            AND cea.datasheet_id=0 AND cea.license_id=0)),2),
         ce.seq_num, cp.seq_num,pkg_util.g_enterprise_id
         FROM scase_master cm,
            scase_event ce,
            scase_product cp
        WHERE cm.case_id = ce.case_id
        AND   cm.case_id = cp.case_id
        AND    ce.sort_id=1 ) ;


      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
         AND enterprise_id = pkg_util.g_enterprise_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_ep_rptblty',
                         'pkg_main',
                         SQLERRM,
                         'CASE_EVENT_PRODUCT_RPTBLTY INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_ep_rptblty , please see table S_ARGUS_APP.LOGGER_LOGS for detials'
            );
   END p_migrate_case_ep_rptblty;

PROCEDURE p_migrate_case_access
/***********************************************************************
Name:        p_migrate_case_access
Description: This procedure migrates case data to CASE_ACCESS table.
***********************************************************************/
AS

BEGIN

  INSERT INTO scase_access
    (case_id,
     site_id,
     study_key,
     product_id,
     enterprise_id )
    (SELECT scm.case_id,
            100000,
            '',
            scm.product_id,
            1
       FROM scase_product scm
      WHERE scm.product_id IS NOT NULL
        AND scm.drug_type = 1);


  UPDATE dm_status
     SET status = 1,
         conv_end_time = SYSDATE
   WHERE table_name = 'CASE_ACCESS'
     AND enterprise_id = 1;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_access',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_ACCESS'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_access , please see table LOGGER_LOGS for detials'
            );
END p_migrate_case_access;



PROCEDURE p_migrate_case_assess
   /***********************************************************************
    Name:        p_migrate_case_assess
    Description: This procedure migrates data to case_assess table.
    ***********************************************************************/
AS

  CURSOR c_case_master IS
    SELECT s.case_id, s.case_num, s.ud_text_12
      FROM scase_master s;

  l_seriousness_flag              NUMBER(10);
  l_count_serious                 NUMBER(10);
  l_count_listedness              NUMBER(10);
  l_count_unlistedness            NUMBER(10);
  l_count_na                      NUMBER(10);
  l_listedness_flag               NUMBER(10);
  l_outcome_flag                  NUMBER(10);
  l_reaction_desc                 VARCHAR2(4000);

  l_prod_name_count1              NUMBER(10);
  l_prod_name_count2              NUMBER(10);
  l_prod_name_count3              NUMBER(10);
  l_prod_name_count4              NUMBER(10);
  l_co_suspect_count              NUMBER(10);

  l_outcome_count                 NUMBER(10);
  l_event_synopsis                VARCHAR2(5);
  l_diagnosis_code_status         VARCHAR2(4000);


BEGIN

  FOR REC1 IN c_case_master LOOP

    SELECT COUNT(1)
      INTO l_count_serious
      FROM src_case_event sce
     WHERE sce.mah_number = rec1.ud_text_12
       AND UPPER(TRIM(sce.serious_nonserious)) = 'SERIOUS';

    IF ( l_count_serious = 0 ) THEN

      l_seriousness_flag := 0;
    ELSE
      l_seriousness_flag := 1;
    END IF;

    SELECT COUNT(1)
      INTO l_count_listedness
      FROM src_case_event sce
     WHERE sce.mah_number = rec1.ud_text_12
       AND ( UPPER(TRIM(sce.listed_unlisted_1)) = 'LISTED' OR UPPER(TRIM(sce.Listed_Unlisted_2)) = 'LISTED'
             OR UPPER(TRIM(sce.Listed_Unlisted_3)) = 'LISTED' OR UPPER(TRIM(sce.Listed_Unlisted_4)) = 'LISTED');

    SELECT COUNT(1)
      INTO l_count_unlistedness
      FROM src_case_event sce
     WHERE sce.mah_number = rec1.ud_text_12
       AND ( UPPER(TRIM(sce.listed_unlisted_1)) = 'UNLISTED' OR UPPER(TRIM(sce.Listed_Unlisted_2)) = 'UNLISTED'
             OR UPPER(TRIM(sce.Listed_Unlisted_3)) = 'UNLISTED' OR UPPER(TRIM(sce.Listed_Unlisted_4)) = 'UNLISTED');

    IF ( l_count_listedness > 0 ) THEN

      l_listedness_flag := 1;
    ELSIF ( l_count_listedness = 0 and l_count_unlistedness > 0 ) THEN
      l_listedness_flag := 2;
    ELSIF ( l_count_listedness = 0 and l_count_unlistedness = 0) THEN
      l_listedness_flag := 3;
    END IF;

    SELECT pkg_util.f_get_argus_code('L_EVT_OUTCOME', sce.outcome)
      INTO l_outcome_flag
      FROM src_case_event sce
     WHERE sce.mah_number = rec1.ud_text_12
       AND ROWNUM = 1;

    SELECT se.reaction_description
      INTO l_reaction_desc
      FROM src_case_event se
     WHERE se.mah_number = rec1.ud_text_12
       AND ROWNUM = 1;

    SELECT COUNT(DISTINCT TRIM(sce.product_name_1)), COUNT(DISTINCT TRIM(sce.product_name_2)), COUNT(DISTINCT TRIM(sce.product_name_3)), COUNT(DISTINCT TRIM(sce.product_name_4))
      INTO l_prod_name_count1, l_prod_name_count2, l_prod_name_count3, l_prod_name_count4
      FROM src_case_event sce
     WHERE sce.mah_number = rec1.ud_text_12
       AND ROWNUM = 1;

    l_co_suspect_count := nvl(l_prod_name_count1,0) + nvl(l_prod_name_count2,0) + nvl(l_prod_name_count3,0) + nvl(l_prod_name_count4,0);

    SELECT COUNT(sce.outcome)
      INTO l_outcome_count
      FROM src_case_event sce
     WHERE sce.mah_number = rec1.ud_text_12
       AND UPPER(TRIM(sce.outcome)) = 'FATAL';

     IF ( l_outcome_count > 0 ) THEN

       l_event_synopsis := 'F';
     ELSIF ( l_outcome_count = 0 AND l_seriousness_flag = 1 ) THEN
       l_event_synopsis := 'LT';
     ELSIF ( l_outcome_count = 0 AND l_seriousness_flag = 0 ) THEN
       l_event_synopsis := 'NO';
     END IF;

     SELECT pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                        l_reaction_desc,
                                        'ptcode')
       INTO l_diagnosis_code_status
       FROM dual;


    --------EVALUATION - NULL
    --------CO_SUSPECT_COUNT - TOTAL NO OF PRODUCTS - 1
    --------EVENT PRIMARY - PREF TERM
    --------EVENT SYNOPSIS  - if fatal then F esle if serious but not fatal then LT ELSE DEFAULT
    --------Diagnosis Reported - event description where rownum - 1
    --------Diagnosis coded - PREF TERM where rownum - 1
    --------SYN CODE - NULL
  ----------CODE STATUS---IF PREF CODE VALUE IS NULL THEN 0 ELSE 1;
  ----------COMPANY DIAGNOSIS - PREF TERM of event description

      INSERT INTO scase_assess
        (case_id,
         listedness,
         updated,
         outcome,
         seriousness,
         company_diagnosis,
         diagnosis_pref_code,
         diagnosis_inc_code,
         diagnosis_inc_term,
         diagnosis_hlgt_code,
         diagnosis_hlgt,
         diagnosis_hlt_code,
         diagnosis_hlt,
         diagnosis_soc_code,
         diagnosis_body_sys,
         co_suspect_count,
         event_synopsis,
         event_primary,
         diagnosis_reptd,
         diagnosis_coded,
         diagnosis_code_status,
         enterprise_id)
 VALUES (rec1.case_id,
         l_listedness_flag,
         sysdate,
         l_outcome_flag,
         l_seriousness_flag,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',-----company diagnosis
                                           l_reaction_desc,
                                           'ptname'
                                           )/*pref_term*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'ptcode') /*pref_code*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'lltcode') /*inc_code*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'lltname') /*inc_term*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'hlgtcode') /*hlgt_code*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'hlgtname') /*hlgt*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'hltcode') /*hlt_code*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'hltname') /*hlt*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'soccode') /*soccode*/,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                    l_reaction_desc,
                                    'socname') /*body_sys*/,
         l_co_suspect_count - 1,
         l_event_synopsis,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                           l_reaction_desc,
                                           'ptname'
                                           )/*pref_term*/,---event primary
         l_reaction_desc,
         pkg_util.f_get_meddra_term('Source: CASE_ASSESS',
                                           l_reaction_desc,
                                           'ptname'
                                           )/*pref_term*/,--diagnosis coded
         (CASE WHEN l_diagnosis_code_status IS NULL THEN 0 ELSE 1 END),
          1
         );

  END LOOP;

  ---for updating diagnosis_dict_id

  UPDATE scase_assess
         SET diagnosis_dict_id  = pkg_util.g_argus_meddra_dict_id,
             diagnosis_code_status = 1
       WHERE diagnosis_inc_code  IS NOT NULL AND enterprise_id = 1;

     UPDATE scase_assess
         SET diagnosis_dict_id = pkg_util.g_argus_meddra_dict_id,
             diagnosis_code_status = 0
       WHERE diagnosis_inc_code IS NULL AND enterprise_id = 1;

   ---for updating diagnosis_dict_id

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_ASSESS'
         AND enterprise_id = 1/*pkg_util.g_enterprise_id*/;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_assess',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_assess INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_assess , please see table LOGGER_LOGS for detials'
            );
END p_migrate_case_assess;


PROCEDURE p_migrate_case_classification
/***********************************************************************
Name:        p_migrate_case_classification
Description: This procedure migrates case data to CASE_CLASSIFICATIONS table.
***********************************************************************/
AS

BEGIN

  INSERT INTO scase_classifications
    (case_id, seq_num, classification_id, enterprise_id)
    (SELECT (SELECT cm.case_id
               FROM scase_master cm
              WHERE cm.ud_text_12 = scm.mah_number),
            pkg_util.f_fetch_sequence ('CASE_CLASSIFICATIONS')/*S_CASE_CLASSIFICATIONS.Nextval*/,---need to change it to pkg_util.fetch_seq
            pkg_util.f_get_argus_code('LM_CASE_CLASSIFICATION',
                                      scm.valid_case_event) ,
            1
       FROM SRC_CASE_MAIN scm where scm.valid_case_event is not null
      );   ---need to add the case no here


  UPDATE dm_status
     SET status = 1,
         conv_end_time = SYSDATE
   WHERE table_name = 'CASE_CLASSIFICATIONS'
     AND enterprise_id = 1;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_classification',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_CLASSIFICATIONS'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_classification , please see table LOGGER_LOGS for detials'
            );
END p_migrate_case_classification;


PROCEDURE p_migrate_case_contact_log
/***********************************************************************
Name:        p_migrate_case_contact_log
Description: This procedure migrates case data to CASE_CONTACT_LOG table.
***********************************************************************/
AS

  l_sort_id       NUMBER(10);

BEGIN

  INSERT INTO scase_contact_log
    (case_id, seq_num, description ,sort_id , enterprise_id)
    (SELECT (SELECT cm.case_id
               FROM scase_master cm
              WHERE cm.ud_text_12 = scm.mah_number),
            pkg_util.f_fetch_sequence ('CASE_CONTACT_LOG')/*s_case_contact_log.Nextval*/,---need to change it to pkg_util.fetch_seq
            DECODE(scm.ENTERED_BY, NULL, NULL, 'Entered by '||scm.ENTERED_BY),
            1,
            1
       FROM SRC_CASE_MAIN scm
     );-- no need to get data from case_main can fetch it from case_master

  UPDATE dm_status
     SET status = 1,
         conv_end_time = SYSDATE
   WHERE table_name = 'CASE_CONTACT_LOG'
     AND enterprise_id = 1;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_contact_log',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_CONTACT_LOG'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_contact_log , please see table LOGGER_LOGS for detials'
            );
END p_migrate_case_contact_log;



PROCEDURE p_migrate_case_medwatch_data
/***********************************************************************
Name:        p_migrate_case_medwatch_data
Description: This procedure migrates case data to case_medwatch_data table.
***********************************************************************/
AS

  l_sort_id       NUMBER(10);

BEGIN

  INSERT INTO scase_medwatch_data
    (case_id,
     adverse_event,---doubt in adverse event
     product_problem,
     followup_type,
     facility_distributor,
     report_source,
     rs_other_text,
     otc,
     event_type,
     pla,
     enterprise_id)
    (SELECT (SELECT cm.case_id
               FROM scase_master cm
              WHERE cm.ud_text_12 = scm.mah_number),
            0,
            0,
            '',
            '',
            CASE WHEN pkg_util.f_get_argus_code('LM_REPORT_TYPE', scm.report_source) = 1 THEN 4 END,
            '',
            0,
            CASE WHEN (((SELECT COUNT(1) FROM src_case_event src WHERE src.mah_number = scm.mah_number AND UPPER(TRIM(src.outcome)) = 'FATAL') > 0)
              AND ((SELECT COUNT(1) FROM src_case_event src WHERE src.mah_number = scm.mah_number AND UPPER(TRIM(src.serious_nonserious)) = 'SERIOUS') > 0)) THEN
               1
               WHEN (((SELECT COUNT(1) FROM src_case_event src WHERE src.mah_number = scm.mah_number AND UPPER(TRIM(src.serious_nonserious)) = 'SERIOUS') > 0)
                 AND ((SELECT COUNT(1) FROM src_case_event src WHERE src.mah_number = scm.mah_number AND UPPER(TRIM(src.outcome)) <> 'FATAL') > 0)) THEN
                 2
                 WHEN ((SELECT COUNT(1) FROM src_case_event src WHERE src.mah_number = scm.mah_number AND UPPER(TRIM(src.serious_nonserious)) = 'NON-SERIOUS') > 0) THEN
                   8 END,
            '',
            1
       FROM SRC_CASE_MAIN scm
      );--need to add the case no here

  UPDATE dm_status
     SET status = 1,
         conv_end_time = SYSDATE
   WHERE table_name = 'CASE_MEDWATCH_DATA'
     AND enterprise_id = 1;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_medwatch_data',

                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_MEDWATCH_DATA'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_medwatch_data , please see table LOGGER_LOGS for detials'
            );
END p_migrate_case_medwatch_data;


PROCEDURE p_migrate_case_narrative
/***********************************************************************
  Name:        p_migrate_case_narrative
  Description: This procedure migrates data to CASE_NARRATIVE table.
  ***********************************************************************/
 AS

  CURSOR c_case_mas IS
    SELECT sm.case_id, sm.case_num, sm.ud_text_12 FROM scase_master sm;

  l_case_event           VARCHAR2(4000);
  l_comments             src_case_main.comments%type;
  l_notes_narrative      src_case_main.notes_narrative%type;
  l_meddra_version       VARCHAR2(4000);--src_case_event.meddra_version%type;
  l_serious_non_serious  VARCHAR2(4000);--src_case_event.serious_nonserious%type;
  l_event_dt_onset       VARCHAR2(4000);--src_case_event.event_date_time_to_onset%type;
  l_event_onset_lat      VARCHAR2(4000);--src_case_event.event_onset_latency%type;
  l_event_onset_lat_unit VARCHAR2(4000);--src_case_event.event_onset_latency_unit%type;
  l_outcome              VARCHAR2(4000);--src_case_event.outcome%type;

  l_listed_unlisted1 VARCHAR2(4000);--src_case_event.listed_unlisted_1%type;
  l_licence_no1      VARCHAR2(4000);--src_case_event.license_number_1%type;
  l_product1         VARCHAR2(4000);--src_case_event.product_name_1%type;
  l_dose_value1      src_case_event.dose_value_1%type;
  l_dose_unit1       VARCHAR2(4000);--src_case_event.dose_unit_1%type;
  l_route_of_admin1  VARCHAR2(4000);--src_case_event.route_of_admin_1%type;
  l_frequency1       VARCHAR2(4000);--src_case_event.frequency_1%type;
  l_start_dt_trmnt1  VARCHAR2(4000);--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt1   VARCHAR2(4000);--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val1   VARCHAR2(4000);--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit1  VARCHAR2(4000);--src_case_event.treatment_duration_unit_1%type;

  l_listed_unlisted2 VARCHAR2(4000);--src_case_event.listed_unlisted_1%type;
  l_licence_no2      VARCHAR2(4000);--src_case_event.license_number_1%type;
  l_product2         VARCHAR2(4000);--src_case_event.product_name_1%type;
  l_dose_value2      src_case_event.dose_value_1%type;
  l_dose_unit2       VARCHAR2(4000);--src_case_event.dose_unit_1%type;
  l_route_of_admin2  VARCHAR2(4000);--src_case_event.route_of_admin_1%type;
  l_frequency2       VARCHAR2(4000);--src_case_event.frequency_1%type;
  l_start_dt_trmnt2  VARCHAR2(4000);--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt2   VARCHAR2(4000);--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val2   VARCHAR2(4000);--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit2  VARCHAR2(4000);--src_case_event.treatment_duration_unit_1%type;

  l_listed_unlisted3 VARCHAR2(4000);--src_case_event.listed_unlisted_1%type;
  l_licence_no3      VARCHAR2(4000);--src_case_event.license_number_1%type;
  l_product3         VARCHAR2(4000);--src_case_event.product_name_1%type;
  l_dose_value3      src_case_event.dose_value_1%type;
  l_dose_unit3       VARCHAR2(4000);--src_case_event.dose_unit_1%type;
  l_route_of_admin3  VARCHAR2(4000);--src_case_event.route_of_admin_1%type;
  l_frequency3       VARCHAR2(4000);--src_case_event.frequency_1%type;
  l_start_dt_trmnt3  VARCHAR2(4000);--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt3   VARCHAR2(4000);--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val3   VARCHAR2(4000);--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit3  VARCHAR2(4000);--src_case_event.treatment_duration_unit_1%type;

  l_listed_unlisted4 VARCHAR2(4000);--src_case_event.listed_unlisted_1%type;
  l_licence_no4      VARCHAR2(4000);--src_case_event.license_number_1%type;
  l_product4         VARCHAR2(4000);--src_case_event.product_name_1%type;
  l_dose_value4      src_case_event.dose_value_1%type;
  l_dose_unit4       VARCHAR2(4000);--src_case_event.dose_unit_1%type;
  l_route_of_admin4  VARCHAR2(4000);--src_case_event.route_of_admin_1%type;
  l_frequency4       VARCHAR2(4000);--src_case_event.frequency_1%type;
  l_start_dt_trmnt4  VARCHAR2(4000);--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt4   VARCHAR2(4000);--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val4   VARCHAR2(4000);--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit4  VARCHAR2(4000);--src_case_event.treatment_duration_unit_1%type;

  l_listed_unlisted5 VARCHAR2(4000);
  l_listed_unlisted6 VARCHAR2(4000);
  l_listed_unlisted7 VARCHAR2(4000);

  l_licence_no5      VARCHAR2(4000);--src_case_event.license_number_1%type;
  l_product5         VARCHAR2(4000);--src_case_event.product_name_1%type;
  l_dose_value5      src_case_event.dose_value_1%type;
  l_dose_unit5       VARCHAR2(4000);--src_case_event.dose_unit_1%type;
  l_route_of_admin5  VARCHAR2(4000);--src_case_event.route_of_admin_1%type;
  l_frequency5       VARCHAR2(4000);--src_case_event.frequency_1%type;
  l_start_dt_trmnt5  VARCHAR2(4000);--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt5   VARCHAR2(4000);--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val5   VARCHAR2(4000);--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit5  VARCHAR2(4000);--src_case_event.treatment_duration_unit_1%type;

  l_licence_no6      VARCHAR2(4000);--src_case_event.license_number_1%type;
  l_product6         VARCHAR2(4000);--src_case_event.product_name_1%type;
  l_dose_value6      src_case_event.dose_value_1%type;
  l_dose_unit6       VARCHAR2(4000);--src_case_event.dose_unit_1%type;
  l_route_of_admin6  VARCHAR2(4000);--src_case_event.route_of_admin_1%type;
  l_frequency6       VARCHAR2(4000);--src_case_event.frequency_1%type;
  l_start_dt_trmnt6  VARCHAR2(4000);--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt6   VARCHAR2(4000);--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val6   VARCHAR2(4000);--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit6  VARCHAR2(4000);--src_case_event.treatment_duration_unit_1%type;

  l_licence_no7      VARCHAR2(4000);--src_case_event.license_number_1%type;
  l_product7         VARCHAR2(4000);--src_case_event.product_name_1%type;
  l_dose_value7      src_case_event.dose_value_1%type;
  l_dose_unit7       VARCHAR2(4000);--src_case_event.dose_unit_1%type;
  l_route_of_admin7  VARCHAR2(4000);--src_case_event.route_of_admin_1%type;
  l_frequency7       VARCHAR2(4000);--src_case_event.frequency_1%type;
  l_start_dt_trmnt7  VARCHAR2(4000);--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt7   VARCHAR2(4000);--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val7   VARCHAR2(4000);--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit7  VARCHAR2(4000);--src_case_event.treatment_duration_unit_1%type;

  l_rep_causaltiy1   VARCHAR2(4000);
  l_rep_causaltiy2   VARCHAR2(4000);
  l_rep_causaltiy3   VARCHAR2(4000);
  l_rep_causaltiy4   VARCHAR2(4000);
  l_rep_causaltiy5   VARCHAR2(4000);
  l_rep_causaltiy6   VARCHAR2(4000);
  l_rep_causaltiy7   VARCHAR2(4000);

  l_com_causaltiy1   VARCHAR2(4000);
  l_com_causaltiy2   VARCHAR2(4000);
  l_com_causaltiy3   VARCHAR2(4000);
  l_com_causaltiy4   VARCHAR2(4000);
  l_com_causaltiy5   VARCHAR2(4000);
  l_com_causaltiy6   VARCHAR2(4000);
  l_com_causaltiy7   VARCHAR2(4000);

  l_final_clob_narrative CLOB;
  l_final_temp           VARCHAR2(32767);

  l_rpt_src              VARCHAR2(4000);
  l_dt_of_not            DATE;
  l_con_org              VARCHAR2(4000);
  l_rep_name             VARCHAR2(4000);
  l_hcp_con              VARCHAR2(4000);
  l_valid_case_event     VARCHAR2(4000);
  l_exp_case_event       VARCHAR2(4000);
  l_src_of_lit           VARCHAR2(4000);
  l_aut_name             VARCHAR2(4000);
  l_journal              VARCHAR2(4000);
  l_pg_no                VARCHAR2(4000);
  l_sex                  VARCHAR2(4000);
  l_age_val              VARCHAR2(4000);
  l_age_unit             VARCHAR2(4000);
  l_age_grp              VARCHAR2(4000);
  l_bell_ref_no          VARCHAR2(4000);
  l_sev_of_reac          VARCHAR2(4000);
  l_pat_preg             VARCHAR2(4000);

BEGIN

  FOR rec1 in c_case_mas LOOP

    SELECT sm.comments, trim(sm.notes_narrative), sm.report_source, to_date(sm.date_of_notification,'dd-mm-rrrr'), sm.country_of_origin,
           sm.reporter_name, sm.hcp_confirmed, sm.valid_case_event, sm.expeditable_case_event,sm.source_of_literature,
           sm.author_name,sm.journal,sm.page_number, sm.sex,sm.age_value, sm.age_unit,sm.age_group,sm.bells_ref_number,
           sm.severity_of_reaction,sm.patient_pregnant
      INTO l_comments, l_notes_narrative,l_rpt_src,l_dt_of_not,l_con_org,l_rep_name,l_hcp_con,l_valid_case_event,
           l_exp_case_event,l_src_of_lit,l_aut_name,l_journal,l_pg_no,l_sex,l_age_val,l_age_unit,l_age_grp,
           l_bell_ref_no,l_sev_of_reac,l_pat_preg
      FROM src_case_main sm
     WHERE sm.mah_number = rec1.ud_text_12;

     l_final_temp := 'Report source:'||l_rpt_src||chr(10)||'Date of notification:'||to_char(l_dt_of_not,'dd-mon-rrrr')||chr(10)||'Country of origin:'||l_con_org||chr(10)||'Reporter name:'||l_rep_name;
     l_final_temp := l_final_temp||chr(10)||'HCP confirmed:'||l_hcp_con||chr(10)||'PSUR valid case/event:'||l_valid_case_event||chr(10)||'Expeditable case/event:'||l_exp_case_event||chr(10)||'Source of literature:'||l_src_of_lit;
     l_final_temp := l_final_temp||chr(10)||'Author name:'||l_aut_name||chr(10)||'Journal:'||l_journal||chr(10)||'Page Number:'||l_pg_no||chr(10)||'Sex:'||l_sex||chr(10)||'Age value:'||l_age_val||chr(10)||'Age unit:'||l_age_unit||chr(10)||'Age group:'||l_age_grp;
     l_final_temp := l_final_temp||chr(10)||'Bells Refernce Number:'||l_bell_ref_no||chr(10)||'Severity of reaction:'||l_sev_of_reac||chr(10)||'Patient pregnant:'||l_pat_preg;

    --DBMS_OUTPUT.put_line(rec1.ud_text_12);

  SELECT DISTINCT /*sce.listed_unlisted_1,*/ sce.license_number_1, sce.product_name_1, sce.dose_value_1, sce.dose_unit_1, sce.route_of_admin_1,
         sce.frequency_1,sce.start_date_treatment_1,sce.stop_date_treatment_1, sce.treatment_duration_value_1,sce.treatment_duration_unit_1,
         /*sce.listed_unlisted_2,*/sce.license_number_2,sce.product_name_2,sce.dose_value_2,sce.dose_unit_2,sce.route_of_admin_2,sce.frequency_2,sce.start_date_treatment_2,
         sce.stop_date_treatment_2,sce.treatment_duration_value_2,sce.treatment_duration_unit_2,/*sce.listed_unlisted_3,*/sce.license_number_3,sce.product_name_3,
         sce.dose_value_3,sce.dose_unit_3,sce.route_of_admin_3,sce.frequency_3,sce.start_date_treatment_3,sce.stop_date_treatment_3,sce.treatment_duration_value_3,sce.treatment_duration_unit_3,
         /*sce.listed_unlisted_4,*/sce.license_number_4,sce.product_name_4,sce.dose_value_4,sce.dose_unit_4,sce.route_of_admin_4,sce.frequency_4,sce.start_date_treatment_4,sce.stop_date_treatment_4,
         sce.treatment_duration_value_4,sce.treatment_duration_unit_4,sce.LICENSE_NUMBER_5, sce.PRODUCT_NAME_5, sce.DOSE_VALUE_5, sce.DOSE_UNIT_5, sce.ROUTE_OF_ADMIN_5, sce.FREQUENCY_5,
         sce.START_DATE_TREATMENT_5, sce.STOP_DATE_TREATMENT_5, sce.TREATMENT_DURATION_VALUE_5, sce.TREATMENT_DURATION_UNIT_5, sce.LICENSE_NUMBER_6, sce.PRODUCT_NAME_6, sce.DOSE_VALUE_6,
         sce.DOSE_UNIT_6, sce.ROUTE_OF_ADMIN_6, sce.FREQUENCY_6, sce.START_DATE_TREATMENT_6, sce.STOP_DATE_TREATMENT_6, sce.TREATMENT_DURATION_VALUE_6, sce.TREATMENT_DURATION_UNIT_6,
         sce.LICENSE_NUMBER_7, sce.PRODUCT_NAME_7, sce.DOSE_VALUE_7, sce.DOSE_UNIT_7, sce.ROUTE_OF_ADMIN_7, sce.FREQUENCY_7, sce.START_DATE_TREATMENT_7, sce.STOP_DATE_TREATMENT_7,
         sce.TREATMENT_DURATION_VALUE_7, sce.TREATMENT_DURATION_UNIT_7
    INTO /*l_listed_unlisted1,*/l_licence_no1,l_product1,l_dose_value1,l_dose_unit1,l_route_of_admin1,l_frequency1,
         l_start_dt_trmnt1,l_stop_dt_trmnt1,l_trmnt_dur_val1,l_trmnt_dur_unit1,/*l_listed_unlisted2,*/l_licence_no2,l_product2,
         l_dose_value2,l_dose_unit2,l_route_of_admin2,l_frequency2,l_start_dt_trmnt2,l_stop_dt_trmnt2,l_trmnt_dur_val2,l_trmnt_dur_unit2,
         /*l_listed_unlisted3,*/l_licence_no3,l_product3,l_dose_value3,l_dose_unit3,l_route_of_admin3,l_frequency3,
         l_start_dt_trmnt3,l_stop_dt_trmnt3,l_trmnt_dur_val3,l_trmnt_dur_unit3,/*l_listed_unlisted4,*/
         l_licence_no4,l_product4,l_dose_value4,l_dose_unit4,l_route_of_admin4,l_frequency4,l_start_dt_trmnt4  ,
         l_stop_dt_trmnt4,l_trmnt_dur_val4,l_trmnt_dur_unit4,l_licence_no5,l_product5,l_dose_value5,l_dose_unit5,l_route_of_admin5,l_frequency5,
         l_start_dt_trmnt5,l_stop_dt_trmnt5,l_trmnt_dur_val5,l_trmnt_dur_unit5,l_licence_no6,l_product6,l_dose_value6,l_dose_unit6,l_route_of_admin6,
         l_frequency6,l_start_dt_trmnt6,l_stop_dt_trmnt6,l_trmnt_dur_val6,l_trmnt_dur_unit6,l_licence_no7,l_product7,l_dose_value7,l_dose_unit7,
         l_route_of_admin7,l_frequency7,l_start_dt_trmnt7,l_stop_dt_trmnt7,l_trmnt_dur_val7,l_trmnt_dur_unit7
    FROM src_case_event sce
   WHERE sce.mah_number = rec1.ud_text_12;

    FOR y in (SELECT e.*
                FROM src_case_main sm, src_case_event e, scase_master cm
               WHERE cm.ud_text_12 = sm.mah_number
                 AND sm.mah_number = e.mah_number
                 AND e.mah_number = rec1.ud_text_12) LOOP

      IF (l_case_event IS NULL) THEN

        l_case_event := TRIM(y.reaction_description);
      ELSE
        IF (TRIM(y.reaction_description) IS NOT NULL) THEN
          l_case_event := l_case_event || ',' || TRIM(y.reaction_description);
        END IF;
      END IF;

      IF (l_meddra_version IS NULL) THEN

        l_meddra_version := TRIM(y.meddra_version);
      ELSE
        IF (TRIM(y.meddra_version) IS NOT NULL) THEN
          l_meddra_version := l_meddra_version || ',' || TRIM(y.meddra_version);
        END IF;
      END IF;

      --l_final_clob_narrative := 'Medrra Version: '|| l_meddra_version;

      IF (l_serious_non_serious IS NULL) THEN

        l_serious_non_serious := TRIM(y.serious_nonserious);
      ELSE
        IF (TRIM(y.serious_nonserious) IS NOT NULL) THEN
          l_serious_non_serious := l_serious_non_serious || ',' || TRIM(y.serious_nonserious);
        END IF;
      END IF;

      IF (l_event_dt_onset IS NULL) THEN

        l_event_dt_onset := TRIM(y.event_date_time_to_onset);
      ELSE
        IF (TRIM(y.event_date_time_to_onset) IS NOT NULL) THEN
          l_event_dt_onset := l_event_dt_onset || ',' || TRIM(y.event_date_time_to_onset);
        END IF;
      END IF;

      IF (l_event_onset_lat IS NULL) THEN

        l_event_onset_lat := TRIM(y.event_onset_latency);
      ELSE
        IF (TRIM(y.event_onset_latency) IS NOT NULL) THEN
          l_event_onset_lat := l_event_onset_lat || ',' || TRIM(y.event_onset_latency);
        END IF;
      END IF;

      IF (l_event_onset_lat_unit IS NULL) THEN

        l_event_onset_lat_unit := TRIM(y.event_onset_latency_unit);
      ELSE
        IF (TRIM(y.event_onset_latency_unit) IS NOT NULL) THEN
          l_event_onset_lat_unit := l_event_onset_lat_unit || ',' || TRIM(y.event_onset_latency_unit);
        END IF;
      END IF;

      IF (l_outcome IS NULL) THEN

        l_outcome := TRIM(y.outcome);
      ELSE
        IF (TRIM(y.outcome) IS NOT NULL) THEN
          l_outcome := l_outcome || ',' || TRIM(y.outcome);
        END IF;
      END IF;

   ------ start of code for listed/unlisted--------------------
      IF (l_listed_unlisted1 IS NULL) THEN

        l_listed_unlisted1 := TRIM(y.listed_unlisted_1);
      ELSE
        IF (TRIM(y.listed_unlisted_1) IS NOT NULL) THEN
          l_listed_unlisted1 := l_listed_unlisted1 || ',' || TRIM(y.listed_unlisted_1);
        END IF;
      END IF;

      IF (l_listed_unlisted2 IS NULL) THEN

        l_listed_unlisted2 := TRIM(y.listed_unlisted_2);
      ELSE
        IF (TRIM(y.listed_unlisted_2) IS NOT NULL) THEN
          l_listed_unlisted2 := l_listed_unlisted2 || ',' || TRIM(y.listed_unlisted_2);
        END IF;
      END IF;

      IF (l_listed_unlisted3 IS NULL) THEN

        l_listed_unlisted3 := TRIM(y.listed_unlisted_3);
      ELSE
        IF (TRIM(y.listed_unlisted_3) IS NOT NULL) THEN
          l_listed_unlisted3 := l_listed_unlisted3 || ',' || TRIM(y.listed_unlisted_3);
        END IF;
      END IF;

      IF (l_listed_unlisted4 IS NULL) THEN

        l_listed_unlisted4 := TRIM(y.listed_unlisted_4);
      ELSE
        IF (TRIM(y.listed_unlisted_4) IS NOT NULL) THEN
          l_listed_unlisted4 := l_listed_unlisted4 || ',' || TRIM(y.listed_unlisted_4);
        END IF;
      END IF;

      IF (l_listed_unlisted5 IS NULL) THEN

        l_listed_unlisted5 := TRIM(y.listed_unlisted_5);
      ELSE
        IF (TRIM(y.listed_unlisted_5) IS NOT NULL) THEN
          l_listed_unlisted5 := l_listed_unlisted5 || ',' || TRIM(y.listed_unlisted_5);
        END IF;
      END IF;

      IF (l_listed_unlisted6 IS NULL) THEN

        l_listed_unlisted6 := TRIM(y.listed_unlisted_6);
      ELSE
        IF (TRIM(y.listed_unlisted_6) IS NOT NULL) THEN
          l_listed_unlisted6 := l_listed_unlisted6 || ',' || TRIM(y.listed_unlisted_6);
        END IF;
      END IF;

      IF (l_listed_unlisted7 IS NULL) THEN

        l_listed_unlisted7 := TRIM(y.listed_unlisted_7);
      ELSE
        IF (TRIM(y.listed_unlisted_7) IS NOT NULL) THEN
          l_listed_unlisted7 := l_listed_unlisted7 || ',' || TRIM(y.listed_unlisted_7);
        END IF;
      END IF;

    ------ end of code for listed/unlisted--------------------

    -----------start of code for reported causality------------------------------
      IF (l_rep_causaltiy1 IS NULL) THEN

        l_rep_causaltiy1 := TRIM(y.reported_causality_1);
      ELSE
        IF (TRIM(y.reported_causality_1) IS NOT NULL) THEN
          l_rep_causaltiy1 := l_rep_causaltiy1 || ',' || TRIM(y.reported_causality_1);
        END IF;
      END IF;

      IF (l_rep_causaltiy2 IS NULL) THEN

        l_rep_causaltiy2 := TRIM(y.reported_causality_2);
      ELSE
        IF (TRIM(y.reported_causality_2) IS NOT NULL) THEN
          l_rep_causaltiy2 := l_rep_causaltiy2 || ',' || TRIM(y.reported_causality_2);
        END IF;
      END IF;

      IF (l_rep_causaltiy3 IS NULL) THEN

        l_rep_causaltiy3 := TRIM(y.reported_causality_3);
      ELSE
        IF (TRIM(y.reported_causality_3) IS NOT NULL) THEN
          l_rep_causaltiy3 := l_rep_causaltiy3 || ',' || TRIM(y.reported_causality_3);
        END IF;
      END IF;

      IF (l_rep_causaltiy4 IS NULL) THEN

        l_rep_causaltiy4 := TRIM(y.reported_causality_4);
      ELSE
        IF (TRIM(y.reported_causality_4) IS NOT NULL) THEN
          l_rep_causaltiy4 := l_rep_causaltiy4 || ',' || TRIM(y.reported_causality_4);
        END IF;
      END IF;

      IF (l_rep_causaltiy5 IS NULL) THEN

        l_rep_causaltiy5 := TRIM(y.reported_causality_5);
      ELSE
        IF (TRIM(y.reported_causality_5) IS NOT NULL) THEN
          l_rep_causaltiy5 := l_rep_causaltiy5 || ',' || TRIM(y.reported_causality_5);
        END IF;
      END IF;

      IF (l_rep_causaltiy6 IS NULL) THEN

        l_rep_causaltiy6 := TRIM(y.reported_causality_6);
      ELSE
        IF (TRIM(y.reported_causality_6) IS NOT NULL) THEN
          l_rep_causaltiy6 := l_rep_causaltiy6 || ',' || TRIM(y.reported_causality_6);
        END IF;
      END IF;

      IF (l_rep_causaltiy7 IS NULL) THEN

        l_rep_causaltiy7 := TRIM(y.reported_causality_7);
      ELSE
        IF (TRIM(y.reported_causality_7) IS NOT NULL) THEN
          l_rep_causaltiy7 := l_rep_causaltiy7 || ',' || TRIM(y.reported_causality_7);
        END IF;
      END IF;

    -----------end of code for reported causality--------------------------------

    -----------start of code for company causality------------------------------

      IF (l_com_causaltiy1 IS NULL) THEN

        l_com_causaltiy1 := TRIM(y.company_causality_1);
      ELSE
        IF (TRIM(y.company_causality_1) IS NOT NULL) THEN
          l_com_causaltiy1 := l_com_causaltiy1 || ',' || TRIM(y.company_causality_1);
        END IF;
      END IF;

      IF (l_com_causaltiy2 IS NULL) THEN

        l_com_causaltiy2 := TRIM(y.company_causality_2);
      ELSE
        IF (TRIM(y.company_causality_2) IS NOT NULL) THEN
          l_com_causaltiy2 := l_com_causaltiy2 || ',' || TRIM(y.company_causality_2);
        END IF;
      END IF;

      IF (l_com_causaltiy3 IS NULL) THEN

        l_com_causaltiy3 := TRIM(y.company_causality_3);
      ELSE
        IF (TRIM(y.company_causality_3) IS NOT NULL) THEN
          l_com_causaltiy3 := l_com_causaltiy3 || ',' || TRIM(y.company_causality_3);
        END IF;
      END IF;

      IF (l_com_causaltiy4 IS NULL) THEN

        l_com_causaltiy4 := TRIM(y.company_causality_4);
      ELSE
        IF (TRIM(y.company_causality_4) IS NOT NULL) THEN
          l_com_causaltiy4 := l_com_causaltiy4 || ',' || TRIM(y.company_causality_4);
        END IF;
      END IF;

      IF (l_com_causaltiy5 IS NULL) THEN

        l_com_causaltiy5 := TRIM(y.company_causality_5);
      ELSE
        IF (TRIM(y.company_causality_5) IS NOT NULL) THEN
          l_com_causaltiy5 := l_com_causaltiy5 || ',' || TRIM(y.company_causality_5);
        END IF;
      END IF;

      IF (l_com_causaltiy6 IS NULL) THEN

        l_com_causaltiy6 := TRIM(y.company_causality_6);
      ELSE
        IF (TRIM(y.company_causality_6) IS NOT NULL) THEN
          l_com_causaltiy6 := l_com_causaltiy6 || ',' || TRIM(y.company_causality_6);
        END IF;
      END IF;

      IF (l_com_causaltiy7 IS NULL) THEN

        l_com_causaltiy7 := TRIM(y.company_causality_7);
      ELSE
        IF (TRIM(y.company_causality_7) IS NOT NULL) THEN
          l_com_causaltiy7 := l_com_causaltiy7 || ',' || TRIM(y.company_causality_7);
        END IF;
      END IF;

   -----------end of code for company causality--------------------------------


    END LOOP;

   /* l_final_temp := l_final_temp||chr(10)||'MedDra Version:'|| l_meddra_version||chr(10)||'Serious/Non Serious:'||l_serious_non_serious||chr(10)||'Event Date/Time to onset:'||l_event_dt_onset||chr(10)||'Event Onset Latency:'||l_event_onset_lat;
    l_final_temp := l_final_temp||chr(10)||'Event Onset Latency Unit:'||l_event_onset_lat_unit||chr(10)||'Outcome:' ||l_outcome||chr(10)||'Listed/Unlisted1:'||l_listed_unlisted1;
    l_final_temp := l_final_temp||chr(10)||'Licence No1:'||l_licence_no1||chr(10)||'Product1:'||l_product1||chr(10)||'Dose value1:'||l_dose_value1||chr(10)||'Dose Unit1:'||l_dose_unit1;
    l_final_temp := l_final_temp||chr(10)||'Route of adminstration1:'||l_route_of_admin1||chr(10)||'Frequency1:'||l_frequency1||chr(10)||'Start date of treatment1:'|| l_start_dt_trmnt1||chr(10)||'Stop date of treatment1:'||l_stop_dt_trmnt1;
    l_final_temp := l_final_temp||chr(10)||'Treatment duration value1:' ||l_trmnt_dur_val1||chr(10)|| 'Treatment duration unit1: ' || l_trmnt_dur_unit1||chr(10)||'Listed/Unlisted2:'||l_listed_unlisted2||chr(10)||'Licence No2:'||l_licence_no2;
    l_final_temp := l_final_temp||chr(10)||'Product2:'||l_product2||chr(10)||'Dose value2:'||l_dose_value2||chr(10)||'Dose Unit2:'||l_dose_unit2||chr(10)||'Route of adminstration2:'||l_route_of_admin2;
    l_final_temp := l_final_temp||chr(10)||'Frequency2:'||l_frequency2||chr(10)||'Start date of treatment2:'||l_start_dt_trmnt2||chr(10)||'Stop date of treatment2:'||l_stop_dt_trmnt2||chr(10)||'Treatment duration value2:'||l_trmnt_dur_val2||chr(10)||'Treatment duration unit2:'||l_trmnt_dur_unit2;
    l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted3:'||l_listed_unlisted3||chr(10)||'Licence No3:'||l_licence_no3||chr(10)||'Product3:'||l_product3||chr(10)||'Dose value3:'||l_dose_value3||chr(10)||'Dose Unit3:'||l_dose_unit3;
    l_final_temp := l_final_temp||chr(10)||'Route of adminstration3:'||l_route_of_admin3||chr(10)||'Frequency3:'||l_frequency3||chr(10)||'Start date of treatment3:'||l_start_dt_trmnt3||chr(10)||'Stop date of treatment3:'||l_stop_dt_trmnt3;
    l_final_temp := l_final_temp||chr(10)||'Treatment duration value3:'||l_trmnt_dur_val3||chr(10)||'Treatment duration unit3:'||l_trmnt_dur_unit3||chr(10)||'Listed/Unlisted4:'||l_listed_unlisted4||chr(10)||'Licence No4:'||l_licence_no4;
    l_final_temp := l_final_temp||chr(10)||'Product4:'||l_product4||chr(10)||'Dose value4:'||l_dose_value4||chr(10)||'Dose Unit4:'||l_dose_unit4||chr(10)||'Route of adminstration4:'||l_route_of_admin4||chr(10)||'Frequency4:'||l_frequency4;
    l_final_temp := l_final_temp||chr(10)||'Start date of treatment4:'||l_start_dt_trmnt4||chr(10)||'Stop date of treatment4:'||l_stop_dt_trmnt4||chr(10)||'Treatment duration value4:'||l_trmnt_dur_val4||chr(10)||'Treatment duration unit4:'||l_trmnt_dur_unit4;*/


    l_final_temp := l_final_temp||chr(10)||'MedDra Version:'|| l_meddra_version||chr(10)||'Serious/Non Serious:'||l_serious_non_serious||chr(10)||'Event Date/Time to onset:'||l_event_dt_onset||chr(10)||'Event Onset Latency:'||l_event_onset_lat;
    l_final_temp := l_final_temp||chr(10)||'Event Onset Latency Unit:'||l_event_onset_lat_unit||chr(10)||'Outcome:' ||l_outcome||chr(10)||'Reported Cusality1:'||l_rep_causaltiy1||chr(10)||'Company Cusality1:'||l_com_causaltiy1;
    l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted1:'||l_listed_unlisted1||chr(10)||'Licence No1:'||l_licence_no1||chr(10)||'Product1:'||l_product1||chr(10)||'Dose value1:'||l_dose_value1||chr(10)||'Dose Unit1:'||l_dose_unit1;
    l_final_temp := l_final_temp||chr(10)||'Route of adminstration1:'||l_route_of_admin1||chr(10)||'Frequency1:'||l_frequency1||chr(10)||'Start date of treatment1:'||l_start_dt_trmnt1||chr(10)||'Stop date of treatment1:'||l_stop_dt_trmnt1;
    l_final_temp := l_final_temp||chr(10)||'Treatment duration value1:'||l_trmnt_dur_val1||chr(10)||'Treatment duration unit1:'|| l_trmnt_dur_unit1;

    IF ( l_product2 IS NOT NULL ) THEN

      l_final_temp := l_final_temp||chr(10)||'Reported Cusality2:'||l_rep_causaltiy2||chr(10)||'Company Cusality2:'||l_com_causaltiy2;
      l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted2:'||l_listed_unlisted2||chr(10)||'Licence No2:'||l_licence_no2||chr(10)||'Product2:'||l_product2;
      l_final_temp := l_final_temp||chr(10)||'Dose value2:'||l_dose_value2||chr(10)||'Dose Unit2:'||l_dose_unit2||chr(10)||'Route of adminstration2:'||l_route_of_admin2;
      l_final_temp := l_final_temp||chr(10)||'Frequency2:'||l_frequency2||chr(10)||'Start date of treatment2:'||l_start_dt_trmnt2||chr(10)||'Stop date of treatment2:'||l_stop_dt_trmnt2;
      l_final_temp := l_final_temp||chr(10)||'Treatment duration value2:'||l_trmnt_dur_val2||chr(10)||'Treatment duration unit2:'||l_trmnt_dur_unit2;
    END IF;

    IF ( l_product3 IS NOT NULL ) THEN

      l_final_temp := l_final_temp||chr(10)||'Reported Cusality3:'||l_rep_causaltiy3||chr(10)||'Company Cusality3:'||l_com_causaltiy3;
      l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted3:'||l_listed_unlisted3||chr(10)||'Licence No3:'||l_licence_no3||chr(10)||'Product3:'||l_product3;
      l_final_temp := l_final_temp||chr(10)||'Dose value3:'||l_dose_value3||chr(10)||'Dose Unit3:'||l_dose_unit3;
      l_final_temp := l_final_temp||chr(10)||'Route of adminstration3:'||l_route_of_admin3||chr(10)||'Frequency3:'||l_frequency3||chr(10)||'Start date of treatment3:'||l_start_dt_trmnt3;
      l_final_temp := l_final_temp||chr(10)||'Stop date of treatment3:'||l_stop_dt_trmnt3||chr(10)||'Treatment duration value3:'||l_trmnt_dur_val3||chr(10)||'Treatment duration unit3:'||l_trmnt_dur_unit3;

    END IF;

    IF ( l_product4 IS NOT NULL ) THEN

      l_final_temp := l_final_temp||chr(10)||'Reported Cusality4:'||l_rep_causaltiy4||chr(10)||'Company Cusality4:'||l_com_causaltiy4;
      l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted4:'||l_listed_unlisted4||chr(10)||'Licence No4:'||l_licence_no4||chr(10)||'Product4:'||l_product4;
      l_final_temp := l_final_temp||chr(10)||'Dose value4:'||l_dose_value4||chr(10)||'Dose Unit4:'||l_dose_unit4||chr(10)||'Route of adminstration4:'||l_route_of_admin4;
      l_final_temp := l_final_temp||chr(10)||'Frequency4:'||l_frequency4||chr(10)||'Start date of treatment4:'||l_start_dt_trmnt4||chr(10)||'Stop date of treatment4:'||l_stop_dt_trmnt4;
      l_final_temp := l_final_temp||chr(10)||'Treatment duration value4:'||l_trmnt_dur_val4||chr(10)||'Treatment duration unit4:'||l_trmnt_dur_unit4;

    END IF;

    IF ( l_product5 IS NOT NULL ) THEN

      l_final_temp := l_final_temp||chr(10)||'Reported Cusality5:'||l_rep_causaltiy5||chr(10)||'Company Cusality5:'||l_com_causaltiy5;
      l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted5:'||l_listed_unlisted5||chr(10)||'Licence No5:'||l_licence_no5||chr(10)||'Product5:'||l_product5;
      l_final_temp := l_final_temp||chr(10)||'Dose value5:'||l_dose_value5||chr(10)||'Dose Unit5:'||l_dose_unit5;
      l_final_temp := l_final_temp||chr(10)||'Route of adminstration5:'||l_route_of_admin5||chr(10)||'Frequency5:'||l_frequency5||chr(10)||'Start date of treatment5:'||l_start_dt_trmnt5||chr(10)||'Stop date of treatment5:'||l_stop_dt_trmnt5;
      l_final_temp := l_final_temp||chr(10)||'Treatment duration value5:'||l_trmnt_dur_val5||chr(10)||'Treatment duration unit5:'||l_trmnt_dur_unit5;

    END IF;

    IF ( l_product6 IS NOT NULL ) THEN

      l_final_temp := l_final_temp||chr(10)||'Reported Cusality6:'||l_rep_causaltiy6||chr(10)||'Company Cusality6:'||l_com_causaltiy6;
      l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted6:'||l_listed_unlisted6||chr(10)||'Licence No6:'||l_licence_no6||chr(10)||'Product6:'||l_product6;
      l_final_temp := l_final_temp||chr(10)||'Dose value6:'||l_dose_value6||chr(10)||'Dose Unit6:'||l_dose_unit6||chr(10)||'Route of adminstration6:'||l_route_of_admin6;
      l_final_temp := l_final_temp||chr(10)||'Frequency6:'||l_frequency6||chr(10)||'Start date of treatment6:'|| l_start_dt_trmnt6||chr(10)||'Stop date of treatment6:'||l_stop_dt_trmnt6;
      l_final_temp := l_final_temp||chr(10)||'Treatment duration value6:'||l_trmnt_dur_val6||chr(10)||'Treatment duration unit6:'||l_trmnt_dur_unit6;

    END IF;

    IF ( l_product7 IS NOT NULL ) THEN

      l_final_temp := l_final_temp||chr(10)||'Reported Cusality7:'||l_rep_causaltiy7||chr(10)||'Company Cusality7:'||l_com_causaltiy7;
      l_final_temp := l_final_temp||chr(10)||'Listed/Unlisted7:'||l_listed_unlisted7||chr(10)||'Licence No7:'||l_licence_no7||chr(10)||'Product7:'||l_product7;
      l_final_temp := l_final_temp||chr(10)||'Dose value7:'||l_dose_value7||chr(10)||'Dose Unit7:'||l_dose_unit7||chr(10)||'Route of adminstration7:'||l_route_of_admin7||chr(10)||'Frequency7:'||l_frequency7;
      l_final_temp := l_final_temp||chr(10)||'Start date of treatment7:'|| l_start_dt_trmnt7||chr(10)||'Stop date of treatment7:'||l_stop_dt_trmnt7||chr(10)||'Treatment duration value7:'||l_trmnt_dur_val7||chr(10)||'Treatment duration unit7:'|| l_trmnt_dur_unit7;

    END IF;

    insert into scase_narrative
      (CASE_ID, ABBREV_NARRATIVE, NARRATIVE, ENTERPRISE_ID)
    values
      (rec1.case_id,
       'Events : ' || l_case_event || chr(10) || 'Comments : ' ||
       l_comments || chr(10) || 'Narrative : ' || l_notes_narrative,
       l_final_temp,
       1);


  l_final_temp :=  NULL;
  l_case_event  := NULL;
  l_comments             := NULL;
  l_notes_narrative      := NULL;
  l_meddra_version       := NULL;--src_case_event.meddra_version%type;
  l_serious_non_serious  := NULL;--src_case_event.serious_nonserious%type;
  l_event_dt_onset       := NULL;--src_case_event.event_date_time_to_onset%type;
  l_event_onset_lat      := NULL;--src_case_event.event_onset_latency%type;
  l_event_onset_lat_unit := NULL;--src_case_event.event_onset_latency_unit%type;
  l_outcome              := NULL;--src_case_event.outcome%type;

  l_listed_unlisted1 := NULL;--src_case_event.listed_unlisted_1%type;
  l_licence_no1      := NULL;--src_case_event.license_number_1%type;
  l_product1         := NULL;--src_case_event.product_name_1%type;
  l_dose_value1      := NULL;
  l_dose_unit1       := NULL;--src_case_event.dose_unit_1%type;
  l_route_of_admin1  := NULL;--src_case_event.route_of_admin_1%type;
  l_frequency1       := NULL;--src_case_event.frequency_1%type;
  l_start_dt_trmnt1  := NULL;--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt1   := NULL;--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val1   := NULL;--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit1  := NULL;--src_case_event.treatment_duration_unit_1%type;

  l_listed_unlisted2 := NULL;--src_case_event.listed_unlisted_1%type;
  l_licence_no2      := NULL;--src_case_event.license_number_1%type;
  l_product2         := NULL;--src_case_event.product_name_1%type;
  l_dose_value2      := NULL;
  l_dose_unit2       := NULL;--src_case_event.dose_unit_1%type;
  l_route_of_admin2  := NULL;--src_case_event.route_of_admin_1%type;
  l_frequency2       := NULL;--src_case_event.frequency_1%type;
  l_start_dt_trmnt2  := NULL;--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt2   := NULL;--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val2   := NULL;--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit2  := NULL;--src_case_event.treatment_duration_unit_1%type;

  l_listed_unlisted3 := NULL;--src_case_event.listed_unlisted_1%type;
  l_licence_no3      := NULL;--src_case_event.license_number_1%type;
  l_product3         := NULL;--src_case_event.product_name_1%type;
  l_dose_value3      := NULL;
  l_dose_unit3       := NULL;--src_case_event.dose_unit_1%type;
  l_route_of_admin3  := NULL;--src_case_event.route_of_admin_1%type;
  l_frequency3       := NULL;--src_case_event.frequency_1%type;
  l_start_dt_trmnt3  := NULL;--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt3   := NULL;--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val3   := NULL;--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit3  := NULL;--src_case_event.treatment_duration_unit_1%type;

  l_listed_unlisted4 := NULL;--src_case_event.listed_unlisted_1%type;
  l_licence_no4      := NULL;--src_case_event.license_number_1%type;
  l_product4         := NULL;--src_case_event.product_name_1%type;
  l_dose_value4      := NULL;
  l_dose_unit4       := NULL;--src_case_event.dose_unit_1%type;
  l_route_of_admin4  := NULL;--src_case_event.route_of_admin_1%type;
  l_frequency4       := NULL;--src_case_event.frequency_1%type;
  l_start_dt_trmnt4  := NULL;--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt4   := NULL;--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val4   := NULL;--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit4  := NULL;

  l_licence_no5      := NULL;--src_case_event.license_number_1%type;
  l_product5         := NULL;--src_case_event.product_name_1%type;
  l_dose_value5      := NULL;
  l_dose_unit5       := NULL;--src_case_event.dose_unit_1%type;
  l_route_of_admin5  := NULL;--src_case_event.route_of_admin_1%type;
  l_frequency5       := NULL;--src_case_event.frequency_1%type;
  l_start_dt_trmnt5  := NULL;--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt5   := NULL;--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val5   := NULL;--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit5  := NULL;

  l_licence_no6      := NULL;--src_case_event.license_number_1%type;
  l_product6         := NULL;--src_case_event.product_name_1%type;
  l_dose_value6      := NULL;
  l_dose_unit6       := NULL;--src_case_event.dose_unit_1%type;
  l_route_of_admin6  := NULL;--src_case_event.route_of_admin_1%type;
  l_frequency6       := NULL;--src_case_event.frequency_1%type;
  l_start_dt_trmnt6  := NULL;--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt6   := NULL;--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val6   := NULL;--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit6  := NULL;

  l_licence_no7      := NULL;--src_case_event.license_number_1%type;
  l_product7         := NULL;--src_case_event.product_name_1%type;
  l_dose_value7      := NULL;
  l_dose_unit7       := NULL;--src_case_event.dose_unit_1%type;
  l_route_of_admin7  := NULL;--src_case_event.route_of_admin_1%type;
  l_frequency7       := NULL;--src_case_event.frequency_1%type;
  l_start_dt_trmnt7  := NULL;--src_case_event.start_date_treatment_1%type;
  l_stop_dt_trmnt7   := NULL;--src_case_event.stop_date_treatment_1%type;
  l_trmnt_dur_val7   := NULL;--src_case_event.treatment_duration_value_1%type;
  l_trmnt_dur_unit7  := NULL;

  l_rpt_src              := NULL;
  l_dt_of_not            := NULL;
  l_con_org              := NULL;
  l_rep_name             := NULL;
  l_hcp_con              := NULL;
  l_valid_case_event     := NULL;
  l_exp_case_event       := NULL;
  l_src_of_lit           := NULL;
  l_aut_name             := NULL;
  l_journal              := NULL;
  l_pg_no                := NULL;
  l_sex                  := NULL;
  l_age_val              := NULL;
  l_age_unit             := NULL;
  l_age_grp              := NULL;
  l_bell_ref_no          := NULL;
  l_sev_of_reac          := NULL;
  l_pat_preg             := NULL;
  l_listed_unlisted5     := NULL;
  l_listed_unlisted6     := NULL;
  l_listed_unlisted7     := NULL;

  l_rep_causaltiy1       := NULL;
  l_rep_causaltiy2       := NULL;
  l_rep_causaltiy3       := NULL;
  l_rep_causaltiy4       := NULL;
  l_rep_causaltiy5       := NULL;
  l_rep_causaltiy6       := NULL;
  l_rep_causaltiy7       := NULL;

  l_com_causaltiy1       := NULL;
  l_com_causaltiy2       := NULL;
  l_com_causaltiy3       := NULL;
  l_com_causaltiy4       := NULL;
  l_com_causaltiy5       := NULL;
  l_com_causaltiy6       := NULL;
  l_com_causaltiy7   	   := NULL;

  COMMIT;

  END LOOP;

  UPDATE dm_status
     SET status = 1, conv_end_time = SYSDATE
   WHERE table_name = 'CASE_NARRATIVE'
     AND enterprise_id = 1/*pkg_util.g_enterprise_id*/;

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    logger.f_error('p_migrate_case_narrative',
                   'pkg_main',
                   SQLCODE || ' ' || SQLERRM,
                   'CASE_NARRATIVE INSERT');
    COMMIT;
    raise_application_error(-20000,
                            'Error In procedure p_migrate_case_narrative , please see table LOGGER_LOGS for detials');
END p_migrate_case_narrative;


PROCEDURE p_migrate_case_pat_info
/***********************************************************************
Name:        p_migrate_case_pat_info
Description: This procedure migrates case data to CASE_PAT_INFO table.
***********************************************************************/
AS

BEGIN

  INSERT INTO scase_pat_info
    (case_id,
     --pat_lastname,
     pat_dob_res,
     pat_age,
     age_unit_id,
     age_group_id,
     gender_id,
     pat_stat_preg,
     confidential,
     enterprise_id)
    (SELECT (SELECT cm.case_id
               FROM scase_master cm
              WHERE cm.ud_text_12 = scm.mah_number),
            --scm.reporter_name,
            0,
            CASE
              WHEN (UPPER(scm.age_value) = 'UNKNOWN' OR UPPER(scm.age_value) = 'U') THEN
               ''
              ELSE
               scm.age_value
            END,
            pkg_util.f_get_argus_code('LM_AGE_UNITS', scm.age_unit),
            pkg_util.f_get_argus_code('LM_AGE_GROUPS', scm.age_group),
            pkg_util.f_get_argus_code('LM_GENDER', scm.sex),
            CASE
              WHEN UPPER(TRIM(SCM.patient_pregnant)) = 'N/A' THEN
               3
              WHEN UPPER(TRIM(SCM.patient_pregnant)) = 'UNKNOWN' THEN
               2
              WHEN UPPER(TRIM(SCM.patient_pregnant)) = 'YES' THEN
               1
              WHEN UPPER(TRIM(SCM.patient_pregnant)) = 'NO' THEN
               0
            END,
            1,
            1
       FROM SRC_CASE_MAIN scm
      ); --need to add the case no here


  UPDATE dm_status
     SET status = 1,
         conv_end_time = SYSDATE
   WHERE table_name = 'CASE_PAT_INFO'
     AND enterprise_id = 1;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_pat_info',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_PAT_INFO'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_pat_info , please see table LOGGER_LOGS for detials'
            );
END p_migrate_case_pat_info;


PROCEDURE p_migrate_case_pregnancy
   /***********************************************************************
     Name:        p_migrate_case_pregnancy
     Description: This procedure migrates data to case_pregnancy table.
     ***********************************************************************/
AS
BEGIN

     INSERT INTO scase_pregnancy
       (case_id, parent, enterprise_id)
       (SELECT (SELECT cm.case_id
                  FROM scase_master cm
                 WHERE cm.ud_text_12  = scm.mah_number),
               0,
               1
          FROM src_case_main scm
         WHERE UPPER(scm.sex) <> 'M'
           );--need to add the case no here

     UPDATE dm_status
        SET status = 1, conv_end_time = SYSDATE
      WHERE table_name = 'CASE_PREGNANCY'
        AND enterprise_id = 1;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_pregnancy',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'case_pregnancy INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_pregnancy , please see table LOGGER_LOGS for detials'
            );
   END p_migrate_case_pregnancy;


  PROCEDURE p_main_migrate (
      pi_enterprise_abbrv   IN   VARCHAR2,
      pi_prod_cd            IN   VARCHAR2,
      pi_migration_user     IN   VARCHAR2,
      pi_migration_site     IN   VARCHAR2,
      pi_database           IN   VARCHAR2
   )

AS
      /***********************************************************************
      Name:        p_main_migrate
      Description: This procedure migrates case data from Source to Staging.
      ***********************************************************************/

      v_status      NUMBER;
      v_max         NUMBER;
      v_nextval     NUMBER;
      v_increment   NUMBER;
      v_count       NUMBER;
BEGIN

      pkg_util.p_initialize (pi_enterprise_abbrv,
                             pi_prod_cd,
                             pi_migration_user,
                             pi_migration_site
                            );

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_MASTER'
         AND enterprise_id = 1;

      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_MASTER'
     AND enterprise_id = 1;

         p_migrate_case_master;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT'
     AND enterprise_id = 1;

      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT'
     AND enterprise_id = 1;

         p_migrate_case_event;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_DEATH'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_DEATH'
     AND enterprise_id = 1;


         p_migrate_case_death;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_REPORTERS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_REPORTERS'
     AND enterprise_id = 1;


         p_migrate_case_reporters;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_ROUTING'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_ROUTING'
     AND enterprise_id = 1;


         p_migrate_case_routing;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_STUDY'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_STUDY'
     AND enterprise_id = 1;


         p_migrate_case_study;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_ASSESS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_ASSESS'
     AND enterprise_id = 1;


         p_migrate_case_assess;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_LITERATURE'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_LITERATURE'
     AND enterprise_id = 1;


         p_migrate_case_literature;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PREGNANCY'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PREGNANCY'
     AND enterprise_id = 1;


         p_migrate_case_pregnancy;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_NARRATIVE'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_NARRATIVE'
     AND enterprise_id = 1;


         p_migrate_case_narrative;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PAT_INFO'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PAT_INFO'
     AND enterprise_id = 1;


         p_migrate_case_pat_info;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PRODUCT'
     AND enterprise_id = 1;


           IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PRODUCT'
     AND enterprise_id = 1;


         p_migrate_case_product;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT_ASSESS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT_ASSESS'
     AND enterprise_id = 1;


         p_migrate_case_event_assess;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_DOSE_REGIMENS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_DOSE_REGIMENS'
     AND enterprise_id = 1;


         p_migrate_case_dose_regimens;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PROD_INGREDIENT'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PROD_INGREDIENT'
     AND enterprise_id = 1;


         p_migrate_case_prod_ingredient;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_PROD_DRUGS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_PROD_DRUGS'
     AND enterprise_id = 1;


         p_migrate_case_prod_drugs;
      END IF;


      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_MEDWATCH_DATA'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_MEDWATCH_DATA'
     AND enterprise_id = 1;


         p_migrate_case_medwatch_data;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT_DETAIL'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT_DETAIL'
     AND enterprise_id = 1;


         p_migrate_case_event_detail;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
     AND enterprise_id = 1;


         p_migrate_case_ep_rptblty;
      END IF;


    SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_ACCESS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_ACCESS'
     AND enterprise_id = 1;


         p_migrate_case_access;
      END IF;


    SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_CLASSIFICATIONS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_CLASSIFICATIONS'
     AND enterprise_id = 1;


         p_migrate_case_classification;
      END IF;


    SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_CONTACT_LOG'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_CONTACT_LOG'
     AND enterprise_id = 1;


         p_migrate_case_contact_log;
      END IF;

    SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_JUSTIFICATIONS'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_JUSTIFICATIONS'
     AND enterprise_id = 1;


         p_migrate_case_justifications;
      END IF;

      SELECT status
        INTO v_status
        FROM dm_status
       WHERE table_name = 'CASE_REFERENCE'
     AND enterprise_id = 1;


      IF v_status = 0
      THEN
         UPDATE dm_status
            SET conv_start_time = SYSDATE
          WHERE table_name = 'CASE_REFERENCE'
     AND enterprise_id = 1;


         p_migrate_case_reference;
      END IF;


      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_main_migrate',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'p_main_migrate'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_main_migrate , please see table LOGGER_LOGS for detials'
            );
END p_main_migrate;

   PROCEDURE p_push_argus_app
   AS
      /****************************************************************************
      Name:        p_push_argus_app
      Description: This procedure migrates case data from Staging to Argus schema
      *****************************************************************************/
      v_count            NUMBER;
      v_check            NUMBER;
      l_sort_id          NUMBER;
      l_first_sus_prod   NUMBER;
      l_case_id          NUMBER;
      l_sort_id_2        NUMBER;
      l_sort_id_1        NUMBER;




   BEGIN

      SELECT COUNT (1)
        INTO v_count
        FROM dm_status
       WHERE status = 1;

      IF (v_count <> 0)
      THEN
         SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_MASTER'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_master
                    (case_id, case_num, rev, workflow_seq_num, last_workflow_seq_num, create_time, init_rept_date, user_id,
           last_update_time, last_update_user_id, requires_followup, followup_date, owner_id, state_id, country_id, lang_id, priority,
                   site_id,  seriousness, rpt_type_id, last_state_id, assessment_needed, priority_override, sid, safety_date, normal_time, max_time,
          report_scheduling, priority_assessment, close_user_id, close_date, close_notes, date_locked, deleted, due_soon, global_num,
          priority_date_assessed, lam_assess_done, e2b_ww_number, worklist_owner_id, susar, last_update_event, initial_justification, force_soon,
          lock_status_id, medically_confirm, UD_TEXT_1, enterprise_id)
               (SELECT case_id, case_num, rev, workflow_seq_num, last_workflow_seq_num, create_time, init_rept_date, user_id,
           last_update_time, last_update_user_id, requires_followup, followup_date, owner_id, state_id, country_id, lang_id, priority,
                   site_id,  seriousness, rpt_type_id, last_state_id, assessment_needed, priority_override, sid, safety_date, normal_time, max_time,
          report_scheduling, priority_assessment, close_user_id, close_date, close_notes, date_locked, deleted, due_soon, global_num,
          priority_date_assessed, lam_assess_done, e2b_ww_number, worklist_owner_id, susar, last_update_event, initial_justification, force_soon,
          lock_status_id, medically_confirm, UD_TEXT_1, enterprise_id
                 FROM scase_master);


            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_MASTER';

            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event
               (SELECT *
                  FROM scase_event);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_DEATH'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_death
               (SELECT *
                  FROM scase_death);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_DEATH';

            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_REPORTERS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_reporters
               (SELECT *
                  FROM scase_reporters);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_REPORTERS';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_ROUTING'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_routing
               (SELECT *
                  FROM SCASE_ROUTING);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_ROUTING';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_STUDY'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_study
               (SELECT *
                  FROM scase_study);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_STUDY';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_ASSESS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_assess
               (SELECT *
                  FROM scase_assess);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_ASSESS';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_LITERATURE'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_literature
               (SELECT *
                  FROM scase_literature);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_LITERATURE';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PREGNANCY'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_pregnancy
               (SELECT *
                  FROM scase_pregnancy);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PREGNANCY';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_NARRATIVE'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_narrative
               (SELECT *
                  FROM scase_narrative);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_NARRATIVE';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PAT_INFO'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_pat_info
               (SELECT *
                  FROM scase_pat_info);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PAT_INFO';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PRODUCT'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_product
               (SELECT *
                  FROM scase_product);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PRODUCT';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT_ASSESS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event_assess
               (SELECT *
                  FROM scase_event_assess);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT_ASSESS';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PROD_DRUGS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_prod_drugs
               (SELECT *
                  FROM scase_prod_drugs);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PROD_DRUGS';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_DOSE_REGIMENS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_dose_regimens
               (SELECT *
                  FROM scase_dose_regimens);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_DOSE_REGIMENS';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_PROD_INGREDIENT'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_prod_ingredient
               (SELECT *
                  FROM scase_prod_ingredient);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_PROD_INGREDIENT';


            COMMIT;
         END IF;


-------------------------------------------------------
     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_MEDWATCH_DATA'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_medwatch_data
               (SELECT *
                  FROM scase_medwatch_data);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_MEDWATCH_DATA';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT_DETAIL'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event_detail
               (SELECT *
                  FROM scase_event_detail);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT_DETAIL';


            COMMIT;
         END IF;

     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_event_product_rptblty
                  (SELECT *
                  FROM scase_event_product_rptblty);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_EVENT_PRODUCT_RPTBLTY';


            COMMIT;
         END IF;


     SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_ACCESS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_access
               (SELECT *
                  FROM scase_access);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_ACCESS';


            COMMIT;
         END IF;


       SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_CLASSIFICATIONS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_classifications
               (SELECT *
                  FROM scase_classifications);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_CLASSIFICATIONS';


            COMMIT;
         END IF;


       SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_CONTACT_LOG'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.case_contact_log
               (SELECT *
                  FROM scase_contact_log);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_CONTACT_LOG';


            COMMIT;
         END IF;

        SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_JUSTIFICATIONS'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.Case_Justifications
               (SELECT *
                  FROM SCASE_JUSTIFICATIONS);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_JUSTIFICATIONS';


            COMMIT;
         END IF;

        SELECT COUNT (1)
           INTO v_check
           FROM dm_status
          WHERE table_name = 'CASE_REFERENCE'
            AND argus_app_count IS NULL
            AND status = 1;

         IF v_check = 1
         THEN
            INSERT INTO argus_app.Case_Reference
               (SELECT *
                  FROM SCASE_REFERENCE);

            v_count := SQL%ROWCOUNT;

            UPDATE dm_status
               SET argus_app_count = v_count
             WHERE table_name = 'CASE_REFERENCE';


            COMMIT;
         END IF;


         UPDATE dm_status ds
            SET staging_count =
                   (SELECT TO_NUMBER
                              (EXTRACTVALUE
                                  (XMLTYPE
                                      (DBMS_XMLGEN.getxml
                                                 (   'select count(*) c from '
                                                  || ut.table_name
                                                 )
                                      ),
                                   '/ROWSET/ROW/C'
                                  )
                              ) count11
                      FROM user_tables ut
                     WHERE ut.table_name = 'S' || ds.table_name)
          WHERE ds.status = 1;


         COMMIT;
      END IF;


            UPDATE argus_app.case_event
               SET ud_number_11 = null;

            UPDATE argus_app.case_product
              SET ud_number_11 = NULL;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_push_argus_app',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'p_push_argus_app'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_push_argus_app , please see table LOGGER_LOGS for detials'
            );
   END p_push_argus_app;

  PROCEDURE p_migrate_case_justifications
/***********************************************************************
    Name:        p_migrate_case_justifications
    Description: This procedure migrates data to CASE_JUSTIFICATIONS table.
    ***********************************************************************/

  AS
  BEGIN

    UPDATE scase_master scm
       SET scm.state_id = 1
     WHERE scm.ud_text_12 in ( SELECT sce.mah_number FROM src_case_event sce WHERE UPPER(TRIM(sce.reaction_description)) like UPPER('%No Reaction D%')
                                 UNION
                               SELECT sce.mah_number FROM src_case_event sce WHERE UPPER(TRIM(sce.reaction_description)) like  UPPER('%Study%')
                             );
    COMMIT;

     INSERT INTO SCASE_JUSTIFICATIONS
       (case_id,
        field_id,
        PRIMARY_SEQ_NUM,
        ALT_SEQ_NUM,
        J_TEXT,
        UPDATED_TIME,
        ENTERPRISE_ID)
       (SELECT scm.case_id,
               2110018,
               1,
               1,
               'This legacy report did not meet the 4 minimum criteria for databasing a case (No reaction description) and was databased in error hence qualifies for deletion.',
               sysdate,
               pkg_util.g_enterprise_id
          FROM scase_master scm
         WHERE scm.state_id = 1);

      UPDATE dm_status
         SET status = 1,
             conv_end_time = SYSDATE
       WHERE table_name = 'CASE_JUSTIFICATIONS'
         AND enterprise_id = pkg_util.g_enterprise_id;

  COMMIT;

  EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_justifications',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE Justificaion INSERT'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_justifications , please see table LOGGER_LOGS for detials'
            );

END p_migrate_case_justifications;

  PROCEDURE p_migrate_case_reference
    /***********************************************************************
    Name:        p_migrate_case_justifications
    Description: This procedure migrates data to CASE_JUSTIFICATIONS table.
    ***********************************************************************/

  AS
  BEGIN

    INSERT INTO scase_reference
      (CASE_ID, SEQ_NUM, REF_NO, REF_TYPE_ID, SORT_ID, enterprise_id)
      (SELECT scm.case_id,
              pkg_util.f_fetch_sequence('CASE_REFERENCE'),
              scm.ud_text_12,
              22,
              1,
              1
         FROM scase_master scm);

   UPDATE dm_status
      SET status = 1,
          conv_end_time = SYSDATE
    WHERE table_name = 'CASE_REFERENCE'
      AND enterprise_id = 1;

      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_migrate_case_reference',
                         'pkg_main',
                         SQLCODE || ' ' || SQLERRM,
                         'CASE_REFERENCE'
                        );
         COMMIT;
         raise_application_error
            (-20000,
             'Error In procedure p_migrate_case_reference , please see table LOGGER_LOGS for detials'
            );

  END p_migrate_case_reference;


END pkg_main;
/