CREATE OR REPLACE PACKAGE BODY pkg_util
AS
/*
Package Name: PKG_UTIL
Description: This package body will contain the supporting code for the other main migration packages

*/

 PROCEDURE p_initialize (
      pi_enterprise_abbrv   VARCHAR2,
      pi_prod_cd            VARCHAR2,
      pi_migration_user     VARCHAR2,
      pi_migration_site     VARCHAR2
   )
   AS
      l_first_time   NUMBER;
      l_count        NUMBER;
   BEGIN
      SELECT TO_DATE (SYSDATE, 'DD-MON-RRRR HH24:MI:SS')
        INTO g_migration_date
        FROM DUAL;

      pkg_rls.set_context ('admin', 0, 'ARGUS_SAFETY', '');

      SELECT enterprise_id
        INTO g_enterprise_id
        FROM cfg_enterprise
       WHERE TRIM (UPPER (enterprise_abbrv)) =
                                            TRIM (UPPER (pi_enterprise_abbrv));

      g_enterprise_abbrev := TRIM (UPPER (pi_enterprise_abbrv));
      g_enterprise_abbrev := pi_enterprise_abbrv;
      g_migration_user_fullname := TRIM (UPPER (pi_migration_user));
      pkg_rls.set_context ('admin', g_enterprise_id, 'ARGUS_SAFETY', '');


      SELECT user_id
        INTO g_migration_user
        FROM argus_app.cfg_users
       WHERE TRIM (UPPER (user_fullname)) = TRIM (UPPER (pi_migration_user));

      SELECT site_id
        INTO g_migration_site_id
        FROM argus_app.lm_sites
       WHERE TRIM (UPPER (site_desc)) = TRIM (UPPER (pi_migration_site));

      SELECT VALUE
        INTO g_argus_meddra_dict_id
        FROM cmn_profile cp
       WHERE cp.KEY = 'AUTOE_P_E_TERM_DIC';

      SELECT VALUE
        INTO g_argus_who_dict_id
        FROM cmn_profile cp
       WHERE cp.KEY = 'AUTOE_P_SUS_D_DIC';

      SELECT TRIM (UPPER (pi_prod_cd))
        INTO g_prod_cd
        FROM DUAL;



     /* SELECT COUNT (*)
        INTO l_first_time
        FROM dm_status
       WHERE enterprise_id = g_enterprise_id;


      IF (l_first_time = 0)
      THEN

         INSERT INTO dm_status
                     (table_name, status, staging_count, argus_app_count,
                      conv_start_time, conv_end_time, enterprise_id)
            SELECT table_name, 0, NULL, NULL, NULL, NULL, g_enterprise_id
              FROM dm_status
             WHERE enterprise_id = -1;
      END IF;*/

      p_insert_dm_mapping;
      p_update_dm_mapping;

   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.f_error ('p_initialize',
                         'pkg_util',
                         SQLERRM,
                         'Migration Initialize'
                        );
         COMMIT;
   END p_initialize;

   FUNCTION f_get_argus_code (
      pi_target_table_name   VARCHAR2,
      pi_source_value    VARCHAR2
   )
      RETURN NUMBER
   AS
      l_argus_code   NUMBER;
    v_count        NUMBER;
   BEGIN
    SELECT COUNT(*)
        INTO v_count
        FROM dm_mapping
       WHERE TRIM (UPPER (target_table_name)) =
                                            TRIM (UPPER (pi_target_table_name))
         AND TRIM (UPPER (source_col_value)) = TRIM (UPPER (pi_source_value))
         AND enterprise_id = g_enterprise_id
         AND ROWNUM = 1;

  IF v_count >0 THEN
      SELECT target_lm_code
        INTO l_argus_code
        FROM dm_mapping
       WHERE TRIM (UPPER (target_table_name)) =
                                            TRIM (UPPER (pi_target_table_name))
         AND TRIM (UPPER (source_col_value)) = TRIM (UPPER (pi_source_value))
         AND enterprise_id = g_enterprise_id
         AND ROWNUM = 1;

      RETURN l_argus_code;

  ELSE RETURN NULL;
  END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         logger.f_error ('f_get_argus_code',
                         'pkg_util',
                         SQLERRM,
                            'Retrieving Argus Code for '
                         || pi_target_table_name
                         || ','
                         || pi_source_value
                        );
         RETURN NULL;
   END f_get_argus_code;


   PROCEDURE p_insert_dm_mapping
   AS
   BEGIN
      DELETE FROM dm_mapping
            WHERE enterprise_id = pkg_util.g_enterprise_id;

      INSERT INTO dm_mapping
                  (target_table_name, target_col_name, target_col_value,
                   col_to_be_fetched, source_col_value, prod_cd, enterprise_id)
         SELECT target_table_name, target_col_name, target_value,
                col_to_be_fetched, source_value, g_prod_cd, g_enterprise_id
           FROM dm_mapping_raw dmr, dm_mapping_reference dmref
          WHERE UPPER (TRIM (dmr.code_list)) = UPPER (TRIM (dmref.code_list))
            AND UPPER (TRIM (dmr.enterprise_abbrev)) =
                                            UPPER (TRIM (g_enterprise_abbrev));

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.f_error ('p_insert_dm_mapping',
                         'pkg_util',
                         SQLERRM,
                         'Error during insertion of DM_MAPPING table'
                        );
   END p_insert_dm_mapping;


   PROCEDURE p_update_dm_mapping
   AS
      CURSOR c_tables
      IS
         SELECT DISTINCT target_table_name, target_col_name, col_to_be_fetched
                    FROM dm_mapping
                   WHERE enterprise_id = g_enterprise_id;

      l_query_text   VARCHAR2 (4000 CHAR);
   BEGIN
      FOR tbl IN c_tables
      LOOP
         l_query_text :=
               'UPDATE DM_MAPPING DM '
            || 'SET DM.TARGET_LM_CODE = (SELECT '
            || tbl.col_to_be_fetched
            || ' FROM '
            || tbl.target_table_name
            || ' WHERE UPPER(TRIM('
            || tbl.target_col_name
            || ')) = UPPER(TRIM(DM.TARGET_COL_VALUE))and rownum = 1)  WHERE DM.TARGET_TABLE_NAME = '''
            || tbl.target_table_name
            || ''' AND dm.TARGET_COL_NAME = '''
            || tbl.target_col_name
            || ''' AND dm.COL_TO_BE_FETCHED = '''
            || tbl.col_to_be_fetched
            || ''' AND dm.enterprise_id = '
            || pkg_util.g_enterprise_id;

         EXECUTE IMMEDIATE l_query_text;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.f_error ('p_update_dm_mapping',
                         'pkg_util',
                         SQLERRM,
                         'Error during updation of DM_MAPPING table'
                        );
   END p_update_dm_mapping;


   FUNCTION f_fetch_sequence (
      pi_table_name      VARCHAR2,
      pi_display_order   NUMBER := NULL
   )
      RETURN NUMBER
   AS
      l_seq        NUMBER (20);
      l_seq_name   VARCHAR2 (50);
   BEGIN
      IF pi_display_order = 1
      THEN
         RETURN 1;
      ELSE
         SELECT sequence_name
           INTO l_seq_name
           FROM argus_app.cmn_table_col_seq_relations
          WHERE UPPER (table_name) = pi_table_name;

         EXECUTE IMMEDIATE 'SELECT ' || l_seq_name || '.NEXTVAL FROM DUAL'
                      INTO l_seq;

         RETURN l_seq;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.f_error ('f_fetch_sequence',
                         'pkg_util',
                         SQLERRM,
                         'Retriving sequence name for table ' || pi_table_name
                        );
         RETURN NULL;
   END f_fetch_sequence;


   PROCEDURE p_get_current_meddra_hierarchy (
      pi_source     IN       VARCHAR2,
      pi_llt_name   IN       VARCHAR2,
      po_terms      OUT      pkg_util.gt_meddra_terms
   )
   AS
   BEGIN
      SELECT mmh.soc_code, mmh.soc_name, mmh.hlgt_code,
             mmh.hlgt_name, mmh.hlt_code, mmh.hlt_name,
             mmh.pt_code, mmh.pt_name, mptl.llt_code,
             mptl.llt_name_english
        INTO po_terms.soccode, po_terms.socname, po_terms.hlgtcode,
             po_terms.hlgtname, po_terms.hltcode, po_terms.hltname,
             po_terms.ptcode, po_terms.ptname, po_terms.lltcode,
             po_terms.lltname
        FROM MEDDRA_USER_20_0.meddra_md_hierarchy mmh, MEDDRA_USER_20_0.meddra_pref_term_llt mptl
       WHERE mmh.pt_code = mptl.pt_code
         AND UPPER (mptl.llt_name_english) = UPPER (TRIM (pi_llt_name))
         AND mmh.primary_soc_fg = 'Y';
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         IF TRIM (pi_llt_name) IS NOT NULL
         THEN
            logger.f_error ( 'p_get_current_meddra_hierarchy',
                              'pkg_util',
                              SQLERRM,
                              'Meddra LLT '
                              || pi_llt_name
                              || ' not found in dictionary for source '
                              || pi_source
                           );
         END IF;

         po_terms.codestatus := 0;
      WHEN OTHERS
      THEN
         logger.f_error ('p_get_current_meddra_hierarchy',
                         'pkg_util',
                         SQLERRM,
                            'Error during getting meddra terms for '
                         || pi_llt_name
                         || ' for source '
                         || pi_source
                        );
         po_terms.codestatus := 0;
   END p_get_current_meddra_hierarchy;


   FUNCTION isserious (piserious_nonserious VARCHAR2)
      RETURN NUMBER AS
   BEGIN
      IF ((substr(upper(trim(piserious_nonserious)),1,3)='SER'))
      THEN
         RETURN 1;
      ELSE
         RETURN 0;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN null;
   END isserious;


    FUNCTION isfatal (pi_outcome VARCHAR2)
      RETURN NUMBER AS
   BEGIN
      IF ((substr(upper(trim(pi_outcome)),1,3)='FAT'))
      THEN
         RETURN 1;
      ELSE
         RETURN null;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END isfatal;

   FUNCTION f_get_meddra_term (
      pi_source     IN   VARCHAR2,
      pi_llt_term   IN   VARCHAR2,
      pi_in_term    IN   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      l_meddra_terms   pkg_util.gt_meddra_terms;
      pi_out_term      VARCHAR2 (200);
   BEGIN
      pkg_util.p_get_current_meddra_hierarchy (pi_source,
                                               pi_llt_term,
                                               l_meddra_terms
                                              );

      IF UPPER (pi_in_term) = 'SOCCODE'
      THEN
         pi_out_term := l_meddra_terms.soccode;
      ELSIF UPPER (pi_in_term) = 'HLTCODE'
      THEN
         pi_out_term := l_meddra_terms.hltcode;
      ELSIF UPPER (pi_in_term) = 'HLGTCODE'
      THEN
         pi_out_term := l_meddra_terms.hlgtcode;
      ELSIF UPPER (pi_in_term) = 'PTCODE'
      THEN
         pi_out_term := l_meddra_terms.ptcode;
      ELSIF UPPER (pi_in_term) = 'LLTCODE'
      THEN
         pi_out_term := l_meddra_terms.lltcode;
      ELSIF UPPER (pi_in_term) = 'SOCNAME'
      THEN
         pi_out_term := l_meddra_terms.socname;
      ELSIF UPPER (pi_in_term) = 'HLTNAME'
      THEN
         pi_out_term := l_meddra_terms.hltname;
      ELSIF UPPER (pi_in_term) = 'HLGTNAME'
      THEN
         pi_out_term := l_meddra_terms.hlgtname;
      ELSIF UPPER (pi_in_term) = 'PTNAME'
      THEN
         pi_out_term := l_meddra_terms.ptname;
      ELSIF UPPER (pi_in_term) = 'LLTNAME'
      THEN
         pi_out_term := l_meddra_terms.lltname;
      END IF;

      RETURN pi_out_term;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         IF TRIM (pi_llt_term) IS NOT NULL
         THEN
            logger.f_error ( 'f_get_meddra_term',
                             'pkg_util',
                             SQLERRM,
                             'Meddra LLT '
                             || pi_llt_term
                             || ' not found in dictionary for source '
                             || pi_source
                           );
         END IF;

         pi_out_term := NULL;
         RETURN pi_out_term;
      WHEN OTHERS
      THEN
         logger.f_error ('f_get_meddra_term',
                         'pkg_util',
                         SQLERRM,
                            'Error during getting meddra terms for '
                         || pi_llt_term
                         || ' for source '
                         || pi_source
                        );
         pi_out_term := NULL;
         RETURN pi_out_term;
   END f_get_meddra_term;

   FUNCTION isdate (pidate VARCHAR2, piformat VARCHAR2)
      RETURN NUMBER
   AS
      vdummy   DATE;
   BEGIN
      IF (pidate IS NULL)
      THEN
         RETURN 0;
      ELSE
         vdummy := TO_DATE (pidate, piformat);
         RETURN 1;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END isdate;

   FUNCTION f_get_argus_value ( pi_target_table_name   VARCHAR2,
                                pi_source_value    VARCHAR2
                              )
      RETURN VARCHAR2

   AS

   v_count                    NUMBER(10);
   l_argus_value              dm_mapping.target_col_value%type;

   BEGIN

     SELECT COUNT(*)
        INTO v_count
        FROM dm_mapping
       WHERE TRIM (UPPER (target_table_name)) =
                                            TRIM (UPPER (pi_target_table_name))
         AND TRIM (UPPER (source_col_value)) = TRIM (UPPER (pi_source_value))
         AND enterprise_id = 1
         AND ROWNUM = 1;

     IF ( v_count > 0) THEN

       SELECT target_col_value
         INTO l_argus_value
         FROM dm_mapping
        WHERE TRIM (UPPER (target_table_name)) =
                                             TRIM (UPPER (pi_target_table_name))
          AND TRIM (UPPER (source_col_value)) = TRIM (UPPER (pi_source_value))
          AND enterprise_id = 1
          AND ROWNUM = 1;

       RETURN l_argus_value;

     ELSE
       RETURN NULL;
     END IF;

     EXCEPTION
        WHEN OTHERS
        THEN
           logger.f_error ('f_get_argus_code',
                           'pkg_util',
                           SQLERRM,
                              'Retrieving Argus Code for '
                           || pi_target_table_name
                           || ','
                           || pi_source_value
                          );
           RETURN NULL;
   END f_get_argus_value;

END pkg_util;
/