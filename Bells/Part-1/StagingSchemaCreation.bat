echo off
REM #############################################################################################
REM ## File      :  CreateStagingSchema.bat .. Batch File                                      ##
REM ## Purpose   :  create staging schema for migration                                        ##
REM #############################################################################################
echo off
cls
sqlplus /nolog @.\CreateStagingSchema.sql